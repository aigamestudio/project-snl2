﻿using UnityEngine;

public class GameplayAISpeedController : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;
	PhaseUseCardActionBehaviour _useCardAction;
	PanelBattleAIController _battleAIController;

	bool canRush = false;

	void Awake () {
		_gameplayManager = GetComponent<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_battleAIController = _uiGameplayScreen.GetComponentInChildren<PanelBattleAIController>(true);
	}

	void Update() {
		_uiGameplayScreen.ShowRushPanel(canRush);
		CheckCanRush();
		HandleRush();
	}

	private void HandleRush() {
		if(canRush) {
			if(Input.GetMouseButton(0)) {
				GameplaySpeedController.RushGameSpeed();
			}
			else {
				GameplaySpeedController.RestoreGameSpeed();
			}
		}
	}

	private void CheckCanRush() {
		//if(GameplayManagerBehaviour.isCampaign) { // buat multiplayer internet uncomment
			CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
			if(GameplayManagerBehaviour.playerIsBot[currentPlayer.playerId]) {
				if(_gameplayManager.GetCurrentPhase() == GameplayManagerBehaviour.GameplayPhase.CheckBattle_Battle) {
					canRush = _battleAIController.CheckBattleBothUsingAI();
					if(!canRush) GameplaySpeedController.MinGameSpeed();
				}
				else if(_gameplayManager.GetCurrentPhase() == GameplayManagerBehaviour.GameplayPhase.UseCard_Action) {
					bool isExclusive = _useCardAction._cardUsed.GetExclusiveCharacter() && _useCardAction._cardUsed.GetExclusiveCharacter().GetCharacterType() == currentPlayer.GetCharacterType();
                    canRush = !(isExclusive && _useCardAction.isUseSpecial);
					if(canRush) {
						for(int i = 0; i < _useCardAction.listTarget.Count; i++) {
							if(!GameplayManagerBehaviour.playerIsBot[_useCardAction.listTarget[i].playerId]) { canRush = false; break; }
						}
					}
					if(!canRush) GameplaySpeedController.MinGameSpeed();
				}
				else {
					canRush = true;
				}
			}
			else {
				canRush = false;
				bool minGameSpeedPhase = _gameplayManager.GetCurrentPhase() == GameplayManagerBehaviour.GameplayPhase.CheckBattle_Battle || _gameplayManager.GetCurrentPhase() == GameplayManagerBehaviour.GameplayPhase.UseCard_Action;
                if(minGameSpeedPhase) GameplaySpeedController.MinGameSpeed();
				else GameplaySpeedController.RestoreGameSpeed();
			}

			if(_gameplayManager.GetCurrentPhase() == GameplayManagerBehaviour.GameplayPhase.Win) {
				canRush = false;
				GameplaySpeedController.MinGameSpeed();
			}
		/*} else {
			canRush = false;
			switch(_gameplayManager.GetCurrentPhase()) {
				case GameplayManagerBehaviour.GameplayPhase.CheckBattle_Battle: GameplaySpeedController.MinGameSpeed(); break;
				case GameplayManagerBehaviour.GameplayPhase.Win: GameplaySpeedController.MinGameSpeed(); break;
				default: GameplaySpeedController.RestoreGameSpeed(); break;
			}
		}*/
	}
}
