﻿/**
*	UIManager buat menuscreen
*	Musti taro canvas
*	Childnya musti kumpulan game object/panel
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIMenuScreenBehaviour : MonoBehaviour {
	public enum CurrentScreen { MainMenuScreen, SettingScreen, CampaignScreen, MultiplayerScreen }
	[SerializeField] CurrentScreen _currentScreen = CurrentScreen.MainMenuScreen;
	GameObject[] _screens;

    [SerializeField] int cheatCodesCountNeed = 17;

	[SerializeField] GameObject _transition;
	[SerializeField] float transitionTime = 1.0f;

	void Awake () {
		_screens = new GameObject[System.Enum.GetValues(typeof(CurrentScreen)).Length];
		for(int i = 0; i < _screens.Length; i++) {
			_screens[i] = transform.GetChild(i).gameObject;
		}
	}

	void OnEnable() {
		ChangeScreen((int)_currentScreen);
	}

	void Update() {
		if(_currentScreen == CurrentScreen.MainMenuScreen) {
			if(Input.GetKey(KeyCode.Escape)) {
				Application.Quit();
			}
		} else {
			if(Input.GetKey(KeyCode.Escape)) {
				DeactiveScreen((int)_currentScreen);
				ChangeScreen(0);
			}
		}
	}

	private void DeactivateAllScreen() {
		for(int i=0; i<_screens.Length; i++) {
			_screens[i].SetActive(false);
		}
	}

	public void ChangeScreen(int newScreen) {
		_currentScreen = (CurrentScreen)newScreen;
		_screens[(int)_currentScreen].SetActive(true);
	}

	public void DeactiveScreen(int screen) {
		_screens[screen].SetActive(false);
	}

	public void ChangeScene(string sceneName) {
		GameObject transition = Instantiate(_transition);
		transition.transform.SetParent(transform, false);
		StartCoroutine(WaitForChangeScene(sceneName));
	}

	public void OpenWWW(string url) {
		Application.OpenURL(url);
	}

    int cheatCodeCount = 0;
    public void UseCheatCodes()
    {
        cheatCodeCount++;
        if(cheatCodeCount >= cheatCodesCountNeed)
        {
            UINotificationBehaviour.instance.ShowErrorNotification("Cheat activated!\nAll maps and characters has been unlocked!");
            for(int i = 0; i< GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP; i++)
            {
                ProfileData.map_unlocked[i] = true;
            }

            for(int i=0; i< GameplayManagerBehaviour.AVAILABLE_CHARACTER; i++)
            {
                ProfileData.character_unlocked[i] = true;
                ProfileData.character_level[i] = GameplayManagerBehaviour.MAX_CHARACTER_LEVEL - 1;
                ProfileData.character_campaign_lastMapUnlocked[i] = GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP - 1;
            }

            ProfileData.SaveCharacterData();
            ProfileData.SaveMapData();
        }
    }

    public void ShowAchievement()
    {
        CloudOnce.Cloud.Achievements.ShowOverlay();
    }

	IEnumerator WaitForChangeScene(string nextSceneName) {
		yield return new WaitForSeconds(transitionTime);
		SceneManager.LoadScene(nextSceneName);
	}
}
