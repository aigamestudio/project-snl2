﻿using UnityEngine;

public class PanelBehaviour : MonoBehaviour {
	public string panelName = "";
	public static bool userCanInput = true;
	
	CharacterManagerBehaviour _characterManager;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
	}
	
	void Update () {
		HandleEnableInput();
	}

	private void HandleEnableInput() {
        userCanInput = !GameplayManagerBehaviour.playerIsBot[_characterManager.GetCurrentPlayer()];
	}
}
