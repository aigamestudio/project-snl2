﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;

public class UIChooseCharacterScreenBehaviour : MonoBehaviour {
	ChooseCharacterController _chooseCharacterController;
	ChooseCharacterManagerBehaviour _chooseCharacterManager;

	SwipeToChangeObjectBehaviour _swipeToChangeObject;
	Text _nameText;
	Text _descText;

	GameObject[] _playerHUD;
	Button _readyButton;

	Button _upgradeButton;
	Button _upgradePanelButton;
	Text _upgradePanelCurrentMoneyText;
	Text _upgradePanelNextLevelText;
	Text _upgradePanelPrevLevelText;

	[SerializeField] string lockedStr = "Terkunci";
	[SerializeField] string[] lockedDescStr;

	[SerializeField] string upgradeNotAvailableStr = "Item is not available";

    int currentUserChoose = 0;

    //[SerializeField] int rewardWatchAdsCoin = 10000;
    [SerializeField] GameObject _transition;
	[SerializeField] float transitionTime = 1.0f;


    void Awake () {
		_chooseCharacterManager = GameObject.FindObjectOfType<ChooseCharacterManagerBehaviour>();
		_chooseCharacterController = GameObject.FindObjectOfType<ChooseCharacterController>();
		_swipeToChangeObject = GameObject.FindObjectOfType<SwipeToChangeObjectBehaviour>();
		_nameText = transform.Find("Name").GetComponent<Text>();
		_descText = transform.Find("Desc Box").Find("Desc").GetComponent<Text>();
		_readyButton = transform.Find("Ready").GetComponent<Button>();
		_upgradeButton = transform.Find("Upgrade").GetComponent<Button>();
		_upgradePanelButton = transform.Find("Upgrade Panel").Find("BackPlate").Find("OK Button").GetComponent<Button>();
		_upgradePanelCurrentMoneyText = transform.Find("Upgrade Panel").Find("BackPlate").Find("Current Money Text").GetChild(0).GetComponent<Text>();
		_upgradePanelNextLevelText = transform.Find("Upgrade Panel").Find("BackPlate").Find("Next Level Text").GetComponent<Text>();
		_upgradePanelPrevLevelText = transform.Find("Upgrade Panel").Find("BackPlate").Find("Previous Level Text").GetComponent<Text>();

		_playerHUD = new GameObject[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i=0; i<_playerHUD.Length; i++) {
			_playerHUD[i] = transform.Find("Player " + (i + 1).ToString()).gameObject;
		}
	}

	void OnEnable() {
		Transform characterList = _swipeToChangeObject.transform;
		foreach(Transform characterObject in characterList) {
			CharacterBehaviour character = characterObject.GetComponent<CharacterBehaviour>();
			character.ChangeCharacterLevel(ProfileData.character_level[character.GetCharacterType()]);
		}
	}
	
	void Update () {
		HandleText();
		HandleHUD();
		HandleReadyButton();
		HandleUpgradeCharacterLevel();
        HandlePlayerChooseNotification();

		if(Input.GetKey(KeyCode.Escape)) {
			BackButton();
		}
	}

	private void HandleText() {
		int currentSlide = _swipeToChangeObject.GetCurrentSlide();
		CharacterBehaviour currentPlayer = _swipeToChangeObject.transform.GetChild(currentSlide).GetComponent<CharacterBehaviour>();

		_nameText.text = currentPlayer.GetCharacterName();
		_descText.text = "Stamina : " + currentPlayer.GetMaxHP() + "\nSpecial : " + currentPlayer.GetSpecialDesc() + "\n\n" + currentPlayer.GetStoryDesc();
	}

	private void HandleHUD() {
		for (int i=0; i<_playerHUD.Length; i++) {
			bool isReady = _chooseCharacterManager.GetIsReady(i);
			int characterType = _chooseCharacterManager.GetCharacterType(i);
			_playerHUD[i].GetComponent<Animator>().SetBool("isReady", isReady);
			_playerHUD[i].GetComponent<Animator>().SetInteger("characterType", characterType);

			Text healthText = _playerHUD[i].transform.Find("Health").GetComponent<Text>();
			Text pointText = _playerHUD[i].transform.Find("Point").GetComponent<Text>();
			if(isReady) {
				CharacterBehaviour player = _swipeToChangeObject.transform.GetChild(characterType).GetComponent<CharacterBehaviour>();
				healthText.text = player.currentHP.ToString();
				pointText.text = player.currentPoint.ToString();
			} else {
				healthText.text = "-";
				pointText.text = "-";
			}

			Transform cardsCount = _playerHUD[i].transform.Find("CardsCount");
			cardsCount.gameObject.SetActive(false);
			
			Text nameText = _playerHUD[i].transform.Find("Name").GetComponent<Text>();
			nameText.text = (!string.IsNullOrEmpty(_chooseCharacterManager.GetPlayerName(i))) ? _chooseCharacterManager.GetPlayerName(i) : "-";
		}
	}

	private void HandleReadyButton() {
        bool isCharacterUnlocked = ProfileData.character_unlocked[_swipeToChangeObject.GetCurrentSlide()];
		if (!isCharacterUnlocked) {
			_readyButton.transform.GetChild(0).GetComponent<Text>().text = lockedStr;
			_readyButton.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = lockedDescStr[_swipeToChangeObject.GetCurrentSlide()];
			_readyButton.interactable = false;
			return;
		}

        //int playerID = _chooseCharacterManager.playerId; 
        _readyButton.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "";
        if (!_chooseCharacterManager.GetIsReady(currentUserChoose)) { // ganti jadi sesuai user choose
            if (currentUserChoose + 1 < ChooseCharacterManagerBehaviour.userCount) _readyButton.transform.GetChild(0).GetComponent<Text>().text = "Next Player";
            else _readyButton.transform.GetChild(0).GetComponent<Text>().text = "Ready!";
			_readyButton.interactable = true;
		} else {
			//if (!_chooseCharacterManager.GetCanPlay()) {
				//_readyButton.transform.GetChild(0).GetComponent<Text>().text = "Waiting for other players ...";
				//_readyButton.interactable = false;
			//} else {
				_readyButton.transform.GetChild(0).GetComponent<Text>().text = "Start!";
				_readyButton.interactable = true;
			//}
		}
	}

	private void HandleUpgradeCharacterLevel() {
		CharacterBehaviour currentCharacter = _swipeToChangeObject.transform.GetChild(_swipeToChangeObject.GetCurrentSlide()).GetComponent<CharacterBehaviour>();
		bool canUpgrade = currentCharacter.GetCurrentLevel() + 1 < GameplayManagerBehaviour.MAX_CHARACTER_LEVEL;
        _upgradeButton.gameObject.SetActive(GameplayManagerBehaviour.isCampaign);
        _upgradeButton.interactable = canUpgrade && !_chooseCharacterManager.GetCanPlay();
		if(canUpgrade) {
			_upgradePanelCurrentMoneyText.text = "Rp " + ProfileData.profile_coin + ",-";
			CharacterBehaviour.CharacterLevel currentLevel = currentCharacter.GetCharacterLevels()[currentCharacter.GetCurrentLevel()];
			CharacterBehaviour.CharacterLevel nextLevel = currentCharacter.GetCharacterLevels()[currentCharacter.GetCurrentLevel() + 1];

			int currentCharacterMaxHP = currentLevel.GetMaxHP();
			string currentCharacterSpecialDesc = currentLevel.GetSpecialDesc();
			_upgradePanelPrevLevelText.text = "From " + currentLevel.GetLevelItemName() + "\n\nStamina : " + currentCharacterMaxHP + "\n\nSpecial : \n" + currentCharacterSpecialDesc;

			int currentCharacterNextMaxHP = nextLevel.GetMaxHP();
			string currentCharacterNextSpecialDesc = nextLevel.GetSpecialDesc();
			_upgradePanelNextLevelText.text = "Upgrade to " + nextLevel.GetLevelItemName() + "\n\nStamina : " + currentCharacterNextMaxHP + "\n\nSpecial : \n" + currentCharacterNextSpecialDesc;

			int nextLevelMapIdRequirement = nextLevel.GetMapIdRequirement();
			bool isUpgradeAvailable = ProfileData.character_campaign_lastMapUnlocked[currentCharacter.GetCharacterType()] > nextLevelMapIdRequirement;
			bool isCoinAvailable = ProfileData.profile_coin >= nextLevel.GetCoinRequirement();
			_upgradePanelButton.transform.GetChild(0).GetComponent<Text>().text = (isUpgradeAvailable) ? "<size='8'>Upgrade " + currentCharacter.GetCharacterName() + " for\n</size>Rp " + nextLevel.GetCoinRequirement() + ",-" : upgradeNotAvailableStr;
			_upgradePanelButton.interactable = isUpgradeAvailable && isCoinAvailable;
		}
	}

    [HideInInspector] bool isDoneShowingNotification = false;
    private void HandlePlayerChooseNotification()
    {
        if (!GameplayManagerBehaviour.isCampaign)
        {
            if(!isDoneShowingNotification)
            {
                UINotificationBehaviour.instance.ShowErrorNotification("Choose character for " + _chooseCharacterManager.GetPlayerName(currentUserChoose));
                isDoneShowingNotification = true;
            }
        }
    }

	public void UpgradeButton() {
		CharacterBehaviour currentCharacter = _swipeToChangeObject.transform.GetChild(_swipeToChangeObject.GetCurrentSlide()).GetComponent<CharacterBehaviour>();
        currentCharacter.ChangeCharacterLevel(currentCharacter.GetCurrentLevel() + 1);

		ProfileData.character_level[currentCharacter.GetCharacterType()] = currentCharacter.GetCurrentLevel();
		ProfileData.profile_coin -= currentCharacter.GetCharacterLevels()[currentCharacter.GetCurrentLevel()].GetCoinRequirement();
		ProfileData.SaveCharacterData();
		ProfileData.SaveProfileData();
	}

	public void ReadyButton() {
		if(!_chooseCharacterManager.GetCanPlay()) { 
			CharacterBehaviour currentCharacter = _swipeToChangeObject.transform.GetChild(_swipeToChangeObject.GetCurrentSlide()).GetComponent<CharacterBehaviour>();
			//int playerID = _chooseCharacterManager.playerId;
			int characterType = currentCharacter.GetCharacterType();
			int characterLevel = currentCharacter.GetCurrentLevel();
			_chooseCharacterManager.SetIsReady(currentUserChoose, true); // current user choose
			_chooseCharacterManager.SetCharacterType(currentUserChoose, characterType, characterLevel);
			_swipeToChangeObject.enabled = (currentUserChoose + 1 < ChooseCharacterManagerBehaviour.userCount);
            isDoneShowingNotification = !(currentUserChoose + 1 < ChooseCharacterManagerBehaviour.userCount);
            currentUserChoose = Mathf.Min(currentUserChoose + 1, ChooseCharacterManagerBehaviour.userCount - 1);
		} else {
			_chooseCharacterManager.LoadingScene();
            LoadingScene();
		}
	}

	public void WatchAdsButton() {
		//var options = new ShowOptions { resultCallback = HandleShowResult };
		//Advertisement.Show("rewardedVideo", options);
	}

	public void LoadingScene() {
		GameObject transition = Instantiate(_transition);
		transition.transform.SetParent(transform, false);
		StartCoroutine(ChangeScene("loading"));
	}

	public void BackButton() {
		GameObject transition = Instantiate(_transition);
		transition.transform.SetParent(transform, false);
		StartCoroutine(ChangeScene("menuscreen"));
	}

	IEnumerator ChangeScene(string newScene) {
		yield return new WaitForSeconds(transitionTime);
		SceneManager.LoadScene(newScene);
	}
}
