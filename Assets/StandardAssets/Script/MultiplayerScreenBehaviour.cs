﻿using UnityEngine;
using System.Collections;

public class MultiplayerScreenBehaviour : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<Animator>().SetBool("loadIsExist", true);
    }

    private string[] GenerateNameForMultiplayer(int playerCount)
    {
        string[] playersName = new string[GameplayManagerBehaviour.MAX_PLAYER];
        for (int i = 0; i < playersName.Length; i++)
        {
            if (i < playerCount) playersName[i] = "Player "+(i+1);
            else playersName[i] = "CPU";
        }

        return playersName;
    }

    private bool[] GenerateIsBotForMultiplayer(int playerCount)
    {
        bool[] playerIsBot = new bool[GameplayManagerBehaviour.MAX_PLAYER];
        for (int i = 0; i < playerIsBot.Length; i++)
        {
            if (i < playerCount) playerIsBot[i] = false;
            else playerIsBot[i] = true;
        }

        return playerIsBot;
    }

    public void SetPlayerButton(int playerCount) { ChooseCharacterManagerBehaviour.userCount = playerCount; }
        //LoadingScreenBehaviour.SetPlayerIdTurn(0); // ga kepake seharusnya
        //LoadingScreenBehaviour.SetPlayerName(GenerateNameForMultiplayer(playerCount));
        //LoadingScreenBehaviour.SetPlayerIsBot(GenerateIsBotForMultiplayer(playerCount)); // set player count doang ntar di choosechar baru??
        //LoadingScreenBehaviour.SetInitialCharacter(GenerateInitialPrefabForCampaign()); di choose char
        // initial character level di choose char pake level yang ada di playerpref aja

}
