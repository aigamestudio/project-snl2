﻿using UnityEngine;

public class MapBehaviour : MonoBehaviour {
	[SerializeField] int mapType = 0;
	[SerializeField] string mapName = "";
	[SerializeField] bool mapHasStoryline = true;
	[SerializeField] bool mapIsDark = false;
	[SerializeField] CharacterBehaviour _bossCharacter;
	[SerializeField] int botLevel = 3;
	[SerializeField] int rewardCoin = 10000;

	MapTileBehaviour[] _mapTiles;

	void Awake() {
		AssignMapTiles();
	}

	void OnEnable() {
		_bossCharacter.ChangeCharacterLevel(0);
	}

	private void AssignMapTiles() {
		if(transform.Find("Tiles")) {
			Transform tiles = transform.Find("Tiles");

			_mapTiles = new MapTileBehaviour[tiles.childCount];
			for(int i = 0; i < _mapTiles.Length; i++) {
				_mapTiles[i] = tiles.GetChild(i).GetComponent<MapTileBehaviour>();
				_mapTiles[i].tileId = i;
			}
		}
	}

	public MapTileBehaviour GetLastCheckpointTile(MapTileBehaviour mapTile) {
		int mapTilePos = Mathf.Max(0, mapTile.tileId - 1); ;
		for(int i=mapTilePos; i>=0; i--) {
			if (_mapTiles[i].GetTileType() == MapTileBehaviour.TileType.Checkpoint) {
				return _mapTiles[i];
			}
		}

		return _mapTiles[GameplayManagerBehaviour.START_POSITION];
	}

	public int GetMapType() { return mapType; }
	public bool GetMapHasStoryline() { return mapHasStoryline; }
	public bool GetMapIsDark() { return mapIsDark; }
	public int GetMapTileCount() { return _mapTiles.Length; }
	public string GetMapName() { return mapName; }
	public MapTileBehaviour GetMapTile(int tileId) { return _mapTiles[tileId]; }
	public CharacterBehaviour GetBossCharacter() { return _bossCharacter; }
	public int GetBotLevel() { return botLevel; }
	public int GetMapRewardCoin() { return rewardCoin; }
}
