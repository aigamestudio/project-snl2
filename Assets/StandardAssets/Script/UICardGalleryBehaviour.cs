﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class UICardGalleryBehaviour : MonoBehaviour {
	Transform _cardDetail;
	GameObject _cardDetail_cardUI;
	Text _cardDetail_cardDescText;

	[SerializeField] CardBehaviour[] cardList;

	void Awake() {
		_cardDetail = transform.Find("CardDetail");
		_cardDetail_cardUI = _cardDetail.Find("Mask").Find("Scroll").Find("Card UI").gameObject;
		_cardDetail_cardDescText = _cardDetail.Find("Mask").Find("Scroll").Find("Card Desc").GetComponent<Text>();
	}

	public void ShowCardDetail(int cardId) {
		CardBehaviour _cardUsed = cardList[cardId];

		_cardDetail_cardUI.GetComponent<Animator>().SetTrigger("hide");
		_cardDetail_cardUI.GetComponent<Animator>().SetInteger("cardType", _cardUsed.GetCardType());
		_cardDetail_cardUI.GetComponent<Animator>().SetBool("isVisible", PanelBehaviour.userCanInput);
		
		if(_cardUsed.GetHasSpecial()) {
			_cardDetail_cardDescText.text = _cardUsed.GetCardDesc() + "\n\n" + _cardUsed.GetCardSpecialDesc();
		}
		else {
			_cardDetail_cardDescText.text = _cardUsed.GetCardDesc();
		}
	}

	public void BackToMenu() {
		SceneManager.LoadScene("menuscreen");
	}
}
