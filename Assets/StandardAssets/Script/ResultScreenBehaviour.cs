﻿using UnityEngine;

public class ResultScreenBehaviour : MonoBehaviour {
	public static int playerOrder = 0;
	public static int gamePerfectCount = 0;
	public static int mapRewardCoin = 10000;
	static int currentRepeatGame = 0;

	[SerializeField] int perfectRollDiceMultiplier = 100;
	[SerializeField] int watchAdsRepeatGame = 3;
	[SerializeField] int paidAudienceAdsCoin = 25000;
	[SerializeField] int rewardWatchAdsCoin = 10000;

	int rewardCoin = 0;
	int perfectRollDiceRewardCoin = 0;
	int totalCoin = 0;

	CharacterBehaviour.CharacterLevel newCharacterLevel;
	string newCharacterUnlocked = "";

	bool adsAvailable = false;

	void OnEnable() {
		currentRepeatGame++;
		adsAvailable = currentRepeatGame % watchAdsRepeatGame == 1;

        HandleMapData();
        HandleProfileData();
		HandleCharacterData();
	}

	private void HandleProfileData() {
		ProfileData.profile_campaign_playerId = GameplayManagerBehaviour.playerIdTurn;
		ProfileData.profile_campaign_lastMap = (playerOrder == 0) ? ProfileData.profile_campaign_lastMap + 1 : ProfileData.profile_campaign_lastMap;

		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			ProfileData.profile_campaign_lastCharacters[i] = GameplayManagerBehaviour.initialPrefab[i].GetCharacterType();
		}

		rewardCoin = (playerOrder == 0) ? mapRewardCoin : 0;
		perfectRollDiceRewardCoin = gamePerfectCount * perfectRollDiceMultiplier;
		totalCoin = rewardCoin + perfectRollDiceRewardCoin;
		ProfileData.profile_coin += totalCoin;

		ProfileData.SaveProfileData();
	}

	private void HandleCharacterData() {
		CharacterBehaviour character = GameplayManagerBehaviour.initialPrefab[GameplayManagerBehaviour.playerIdTurn];
		int currentCharacterCampaignLastMapUnlocked = ProfileData.character_campaign_lastMapUnlocked[character.GetCharacterType()];
		if(currentCharacterCampaignLastMapUnlocked < ProfileData.profile_campaign_lastMap) {
			currentCharacterCampaignLastMapUnlocked = ProfileData.profile_campaign_lastMap;
			ProfileData.character_campaign_lastMapUnlocked[character.GetCharacterType()] = currentCharacterCampaignLastMapUnlocked;
			for(int i=0; i<character.GetCharacterLevels().Length; i++) {
				if(character.GetCharacterLevels()[i].GetMapIdRequirement() == currentCharacterCampaignLastMapUnlocked) {
					newCharacterLevel = character.GetCharacterLevels()[i];
                    break;
				}
			}
		}

		if(!ProfileData.character_unlocked[4]) {
			for(int i = 0; i < GameplayManagerBehaviour.AVAILABLE_CHARACTER_DEFAULT; i++) {
				if(ProfileData.character_campaign_lastMapUnlocked[i] < GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP) break;
				ProfileData.character_unlocked[4] = true; // cek unlock uwa-uwa
				newCharacterUnlocked = "Uwa-uwa";
			}
		}

		if(!ProfileData.character_unlocked[5]) {
			bool hasUnlocked = ProfileData.character_campaign_lastMapUnlocked[4] >= GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP;
			ProfileData.character_unlocked[5] = hasUnlocked; // cek unlock kaloong
            newCharacterUnlocked = (hasUnlocked) ? "Kaloong" : "";
		}

		ProfileData.SaveCharacterData();
	}

    private void HandleMapData()
    {
        if (playerOrder == 0) ProfileData.map_unlocked[ProfileData.profile_campaign_lastMap] = true;
        ProfileData.SaveMapData();
    }

	public void GiveRewardWatchAds(bool isPaidAudience) {
		ProfileData.profile_coin += (isPaidAudience) ? paidAudienceAdsCoin : rewardWatchAdsCoin;
		ProfileData.SaveProfileData();
	}

	public void NextStage() {
		GetComponent<CampaignScreenBehaviour>().ContinueButton();
	}

	public int GetRewardCoin() { return rewardCoin; }
	public int GetPerfectRollDiceRewardCoin() { return perfectRollDiceRewardCoin; }
	public int GetTotalCoin() { return totalCoin; }
	public CharacterBehaviour.CharacterLevel GetNewCharacterLevel() { return newCharacterLevel; }
	public string GetNewCharacterUnlocked() { return newCharacterUnlocked; }
	public bool GetAdsAvailable() { return adsAvailable; }
}
