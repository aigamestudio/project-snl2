﻿using UnityEngine;
using System.Collections.Generic;

public class CardDeckBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;

	Transform _availableDeck;
	Transform _graveDeck;

	[SerializeField] CardBehaviour[] _availableCardTypePrefab = new CardBehaviour[GameplayManagerBehaviour.AVAILABLE_CARD_TYPE];
	[SerializeField] CardBehaviour[] _playerSpecialCard = new CardBehaviour[GameplayManagerBehaviour.AVAILABLE_CHARACTER];
	[SerializeField] int minSpecialCardOrder = 3;
	[SerializeField] int maxSpecialCardOrder = 7;
	[SerializeField] List<Transform> customOrderList = new List<Transform>();

	void Awake() {
		_characterManager = FindObjectOfType<CharacterManagerBehaviour>();
		_availableDeck = transform.Find("Available Deck");
		_graveDeck = transform.Find("Grave Deck");
	}

	void Update() {
		HandleCustomOrderList();
		ShuffleAvailableDeck();
	}

	private void ShuffleAvailableDeck() {
		if(_availableDeck.childCount == 0) {
			int totalCard = _graveDeck.childCount;
			for(int i = 0; i < totalCard; i++) {
				int randomId = Random.Range(0, _graveDeck.childCount);
				SetCardOnAvailableDeck(randomId);
			}

			int[] random = new int[GameplayManagerBehaviour.MAX_PLAYER];
			for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
				CardBehaviour specialCard = _playerSpecialCard[_characterManager.GetCharacter(i).GetCharacterType()];
				random[i] = GameplayManagerBehaviour.MAX_PLAYER * Random.Range(minSpecialCardOrder, maxSpecialCardOrder + 1) + i;
                specialCard.transform.SetSiblingIndex(random[i]);
			}
			for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
				CardBehaviour specialCard = _playerSpecialCard[_characterManager.GetCharacter(i).GetCharacterType()];
				specialCard.transform.SetSiblingIndex(random[i]);
			}
		}
	}

	private void HandleCustomOrderList() {
		for(int i = customOrderList.Count - 1; i >= 0; i--) {
			customOrderList[i].SetParent(_availableDeck);
			customOrderList[i].SetAsFirstSibling();
			customOrderList.RemoveAt(i);
		}
	}

	public int ViewLastCardUsedType() {
		if(_graveDeck.childCount > 0) {
			return _graveDeck.GetChild(_graveDeck.childCount - 1).GetComponent<CardBehaviour>().GetCardType();
		}

		return -1;
	}

	public int CountCardTypeOnAvailableDeck(int cardType) {
		int result = 0;
		for(int i=0; i<_availableDeck.childCount; i++) {
			if(_availableDeck.GetChild(i).GetComponent<CardBehaviour>().GetCardType() == cardType) result++;
		}

		return result;
	}

	public CardBehaviour[] GetAvailableCardTypePrefab() { return _availableCardTypePrefab; }
	public List<Transform> GetCustomOrderList() { return customOrderList; }
	public Transform GetAvailableDeck() { return _availableDeck; }
	public Transform GetGraveDeck() { return _graveDeck; }
	public void SetCardOnAvailableDeck(int graveDeckCardId) { _graveDeck.GetChild(graveDeckCardId).SetParent(_availableDeck); } 
}
