﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class UIStorylineScreenBehaviour : MonoBehaviour {
	GameObject _tutorialConfirmation;
	Text _storyText;
	Animator _animator;

	[SerializeField][Range(0, GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP)] int mapType = 0;
	[SerializeField] Animator _storyAnimator;
	[SerializeField][TextArea(1, 3)] string[] storyDialogue;
	[SerializeField] float storySpeed = 0.25f;
	int storyCount = 0;

	void Awake() {
		_tutorialConfirmation = transform.Find("Tutorial Confirmation").gameObject;
		_storyText = transform.Find("Storyline Box").GetChild(0).GetComponent<Text>();
		_animator = GetComponent<Animator>();
	}

	void OnEnable() {
		StartCoroutine(ShowWithAnimateText(_storyText, storyDialogue[0], storySpeed ));
	}

	void Update() {
		_storyAnimator.SetInteger("storyCount", storyCount);
	}

	public void NextDialogueButton() {
		storyCount = Mathf.Min(storyCount + 1, storyDialogue.Length);
		if(storyCount < storyDialogue.Length) {
			StopCoroutine("ShowWithAnimateText");
			StartCoroutine(ShowWithAnimateText(_storyText, storyDialogue[storyCount], storySpeed));
		} else {
			SkipStorylineButton();
		}
	}

	public void SkipStorylineButton() {
		_animator.SetTrigger("quit");

		ProfileData.map_storyline_done[mapType] = true;
		ProfileData.SaveMapData();
	}

	public void ExitStoryline() {
		SceneManager.LoadScene("gameplay - " + LoadingScreenBehaviour.changeScene);
		Destroy(Singleton.instance.gameObject);
	}

	IEnumerator ShowWithAnimateText(Text text, string strComplete, float speed) {
		int i = 0;
		string str = "";
		while (i<strComplete.Length) {
			str += strComplete[i++];
			text.text = str;
			yield return new WaitForSeconds(speed * Time.deltaTime);
		}
	}
}
