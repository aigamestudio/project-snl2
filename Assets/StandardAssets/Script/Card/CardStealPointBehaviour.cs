﻿using UnityEngine;
using System.Collections.Generic;

public class CardStealPointBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	CardBehaviour _card;

	[SerializeField] int maxStolePoint = 20;
	[SerializeField] int[] maxSpecialStolePoint = new int[GameplayManagerBehaviour.MAX_CHARACTER_LEVEL];
	bool initialized = false;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		foreach(CharacterBehaviour target in listTarget) {
			int stolenPoint = Mathf.Min(target.currentPoint, (_card.cardUseSpecial) ? maxSpecialStolePoint[currentPlayer.GetCurrentLevel()] : maxStolePoint);
			currentPlayer.currentPoint = Mathf.Min(currentPlayer.currentPoint + stolenPoint, GameplayManagerBehaviour.MAX_POINT);
			target.currentPoint = Mathf.Max(0, target.currentPoint - stolenPoint);
			target.SetAnimation(CharacterBehaviour.AnimationType.Stolen);
		}

		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
