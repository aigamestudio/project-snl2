﻿using UnityEngine;
using System.Collections.Generic;

public class CardThrowCardBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	CardDeckBehaviour _cardDeck;
	CardBehaviour _card;

	bool initialized = false;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_cardDeck = GameObject.FindObjectOfType<CardDeckBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());

		int throwCard = currentPlayer.currentPoint / ((_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint());
		foreach(CharacterBehaviour target in listTarget) {
			target.SetAnimation(CharacterBehaviour.AnimationType.CardLoss);
			for(int i = 0; i < throwCard; i++) {
				if(target.GetCards().childCount > 0) {
					int randomCard = Random.Range(0, target.GetCards().childCount);
					target.GetCards().GetChild(randomCard).GetComponent<CardBehaviour>().PutCardToDeck(_cardDeck);
				}
			}
		}

		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
