﻿using UnityEngine;
using System.Collections.Generic;

public class CardSwapPositionBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;
	PhaseUseCardActionBehaviour _useCardAction;
	CardBehaviour _card;

	List<CharacterBehaviour> listTarget;
	CharacterBehaviour currentPlayer;
	MapTileBehaviour playerTile;
	MapTileBehaviour targetTile;

	bool initialized = false;
	bool[] targetHasArrived;
	bool playerHasArrived = false;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}
	
	void Update () {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			} else {
				HandleMovement();
				HandleNextPhase();
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		listTarget = _useCardAction.listTarget;
		currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		playerTile = _map.GetMapTile(currentPlayer.position);
		targetTile = _map.GetMapTile(listTarget[0].position);

		targetHasArrived = new bool[listTarget.Count];
		playerHasArrived = false;

		currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.Ladder);
		for(int i=0; i<listTarget.Count; i++) {
			listTarget[i].SetAnimation(CharacterBehaviour.AnimationType.Snake);
		}
	}

	private void HandleMovement() {
		Vector2 targetSlotTile = targetTile.GetCharacterSlot(currentPlayer.playerId).transform.position;
		playerHasArrived = currentPlayer.CheckArrivedInPosition(targetSlotTile, _characterManager.GetCheckRadius());
		if(playerHasArrived) {
			currentPlayer.position = targetTile.tileId;
			currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.Idle);
		} else {
			currentPlayer.MoveToPosition(targetSlotTile, _characterManager.GetMoveSpeed());
		}

		for(int i = 0; i < listTarget.Count; i++) {
			if(!targetHasArrived[i]) {
				Vector2 playerSlotTile = playerTile.GetCharacterSlot(listTarget[i].playerId).transform.position;
				listTarget[i].MoveToPosition(playerSlotTile, _characterManager.GetMoveSpeed());
				targetHasArrived[i] = listTarget[i].CheckArrivedInPosition(playerSlotTile, _characterManager.GetCheckRadius());
				if(targetHasArrived[i]) {
					listTarget[i].position = playerTile.tileId;
					listTarget[i].SetAnimation(CharacterBehaviour.AnimationType.Idle);
				}
			}
		}
	}

	private void HandleNextPhase() {
		if(!playerHasArrived) return;
		for(int i=0; i<targetHasArrived.Length; i++) {
			if(!targetHasArrived[i]) return;
		}

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
