﻿using UnityEngine;
using System.Collections.Generic;

public class CardHealBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	CardBehaviour _card;

	[SerializeField] int maxHealHP = 1;
	[SerializeField] int maxSpecialHealHP = 0;
	bool initialized = false;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;
		foreach(CharacterBehaviour target in listTarget) {
			int hpRestored = (_card.cardUseSpecial) ? maxSpecialHealHP : maxHealHP;
			target.currentHP = Mathf.Min(target.GetMaxHP(), target.currentHP + hpRestored);
			target.SetAnimation(CharacterBehaviour.AnimationType.Heal);
		}
		
		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
