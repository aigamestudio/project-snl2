﻿using UnityEngine;
using System.Collections.Generic;

public class CardSkipTurnBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	CardBehaviour _card;

	[SerializeField] StateSkipBehaviour _stateSkipPrefab;
	[SerializeField] StateSkipBehaviour[] _stateSpecialSkipPrefab = new StateSkipBehaviour[GameplayManagerBehaviour.MAX_CHARACTER_LEVEL];

	bool initialized = false;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;

		foreach(CharacterBehaviour target in listTarget) {
			StateSkipBehaviour skipState = (_card.cardUseSpecial) ? Instantiate(_stateSpecialSkipPrefab[currentPlayer.GetCurrentLevel()]) : Instantiate(_stateSkipPrefab);
			target.AddState(skipState.GetComponent<StateBehaviour>());
			target.SetAnimation(CharacterBehaviour.AnimationType.Skipped);
		}

		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
