﻿using UnityEngine;
using System.Collections.Generic;

public class CardMultiplierSpeedBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	CardBehaviour _card;

	[SerializeField] StateRollDiceBehaviour _stateRollDicePrefab;
	[SerializeField] StateRollDiceBehaviour[] _stateSpecialRollDicePrefab = new StateRollDiceBehaviour[GameplayManagerBehaviour.MAX_CHARACTER_LEVEL];

	bool initialized = false;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}
	
	void Update () {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
        List<CharacterBehaviour> listTarget = _useCardAction.listTarget;

		foreach(CharacterBehaviour target in listTarget) {
			StateRollDiceBehaviour rollDiceState = (_card.cardUseSpecial) ? Instantiate(_stateSpecialRollDicePrefab[currentPlayer.GetCurrentLevel()]) : Instantiate(_stateRollDicePrefab);
			target.AddState(rollDiceState.GetComponent<StateBehaviour>());
			target.SetAnimation(CharacterBehaviour.AnimationType.MultiplySpeed);
		}

		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
