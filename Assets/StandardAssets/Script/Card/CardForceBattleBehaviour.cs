﻿using UnityEngine;
using System.Collections.Generic;

public class CardForceBattleBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	PhaseCheckBattleBattleBehaviour _checkBattle_Battle;
	CardBehaviour _card;

	bool initialized = false;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_checkBattle_Battle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBattleBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		_checkBattle_Battle.SetIsBossBattle(false);

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		_checkBattle_Battle._challenger = currentPlayer;

		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;
		foreach(CharacterBehaviour target in listTarget) {
			_checkBattle_Battle._challenged = target;
		}

		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCardToBattlePhase();
	}
}
