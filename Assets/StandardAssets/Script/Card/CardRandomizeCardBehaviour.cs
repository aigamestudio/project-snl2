﻿using UnityEngine;
using System.Collections.Generic;

public class CardRandomizeCardBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	CardDeckBehaviour _cardDeck;
	CardBehaviour _card;

	bool initialized = false;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_cardDeck = GameObject.FindObjectOfType<CardDeckBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				// no update
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;
		List<CardBehaviour> listCard = new List<CardBehaviour>();
		int[] cardCount = new int[listTarget.Count];
		
		for(int i = 0; i < listTarget.Count; i++) {
			for(int j = 0; j < listTarget[i].GetCards().childCount; j++) {
				listCard.Add(listTarget[i].GetCards().GetChild(j).GetComponent<CardBehaviour>());
			}
		}

		for(int i=0; i<listCard.Count; i++) {
			int randomTargetId = Random.Range(0, listTarget.Count);
			if(listTarget[randomTargetId].GetCards().childCount < GameplayManagerBehaviour.MAX_CARD_SLOT) {
				int randomCard = Random.Range(0, listCard.Count);
				listCard[randomCard].AddCardToCharacter(listTarget[randomTargetId]);
				listCard.Remove(listCard[randomCard]);
			}
		}

		Invoke("HandleNextPhase", GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void HandleNextPhase() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
