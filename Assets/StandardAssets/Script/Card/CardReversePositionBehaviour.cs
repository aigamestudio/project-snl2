﻿using UnityEngine;
using System.Collections.Generic;

public class CardReversePositionBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;
	PhaseUseCardActionBehaviour _useCardAction;
	CardBehaviour _card;

	List<CharacterBehaviour> listTarget;
	CharacterBehaviour currentPlayer;
	MapTileBehaviour[] targetTile;

	bool initialized = false;
	bool[] targetHasArrived;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_card = GetComponent<CardBehaviour>();
	}

	void Update() {
		if(_card.cardActive) {
			if(!initialized) {
				OnInitialized();
			}
			else {
				HandleMovement();
				HandleNextPhase();
			}
		}
	}

	private void OnInitialized() {
		initialized = true;

		listTarget = _useCardAction.listTarget;
		currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		targetHasArrived = new bool[listTarget.Count];
		targetTile = new MapTileBehaviour[listTarget.Count];

		int reverse = currentPlayer.currentPoint / ((_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint());
		for(int i=0; i<listTarget.Count; i++) {
			int targetPos = Mathf.Max(0, listTarget[i].position - reverse);
			targetTile[i] = _map.GetMapTile(targetPos);
			listTarget[i].SetAnimation(CharacterBehaviour.AnimationType.Snake);
		}
	}

	private void HandleMovement() {
		for(int i = 0; i < listTarget.Count; i++) {
			if(!targetHasArrived[i]) {
				Vector2 targetSlotTile = targetTile[i].GetCharacterSlot(listTarget[i].playerId).transform.position;
				listTarget[i].MoveToPosition(targetSlotTile, _characterManager.GetMoveSpeed());
				targetHasArrived[i] = listTarget[i].CheckArrivedInPosition(targetSlotTile, _characterManager.GetCheckRadius());
				if(targetHasArrived[i]) {
					listTarget[i].position = targetTile[i].tileId;
					listTarget[i].SetAnimation(CharacterBehaviour.AnimationType.Idle);
				}
			}
		}
	}

	private void HandleNextPhase() {
		for(int i = 0; i < targetHasArrived.Length; i++) {
			if(!targetHasArrived[i]) return;
		}

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int pointUsed = (_card.cardUseSpecial) ? _card.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) : _card.GetCardReqPoint();
		currentPlayer.currentPoint = Mathf.Max(0, currentPlayer.currentPoint - pointUsed);

		initialized = false;
		_useCardAction.DiactivateCard();
	}
}
