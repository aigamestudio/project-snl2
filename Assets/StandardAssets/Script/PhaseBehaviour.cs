﻿using UnityEngine;

public class PhaseBehaviour : MonoBehaviour {
	public string phaseName = "";
	[SerializeField] UIGameplayScreenBehaviour.GameplayPanel _usePanel = UIGameplayScreenBehaviour.GameplayPanel.NoPanel;
	[SerializeField] CameraManagerBehaviour.CameraType _cameraType = CameraManagerBehaviour.CameraType.AllPlayer;
	
	UIGameplayScreenBehaviour _uigameplayscreen;
	CameraManagerBehaviour _cameraManager;

	void Awake () {
		_uigameplayscreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();	
		_cameraManager = GameObject.FindObjectOfType<CameraManagerBehaviour>();
	}
	
	void OnEnable () {
		_uigameplayscreen.ChangePanel(_usePanel);
		_cameraManager.SetCameraType(_cameraType);
	}
}
