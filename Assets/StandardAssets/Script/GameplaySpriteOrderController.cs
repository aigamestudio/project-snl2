﻿using UnityEngine;

public class GameplaySpriteOrderController : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	[SerializeField][Range(0, 3)] int decimalPrecision = 1;
	SpriteRenderer[] characterSprite;

	void Awake () {
		_characterManager = FindObjectOfType<CharacterManagerBehaviour>();
		_map = FindObjectOfType<MapBehaviour>();
	}

	void Start() {
		AssignCharacterSprite();
		HandleMapRenderer();
	}

	void Update() {
		HandleCharacterRenderer();
	}

	private void AssignCharacterSprite() {
		characterSprite = new SpriteRenderer[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			characterSprite[i] = _characterManager.GetCharacter(i).transform.Find("Sprites").GetChild(0).GetComponent<SpriteRenderer>();
		}
	}

	private void HandleCharacterRenderer() {
		for(int i=0; i<characterSprite.Length; i++) {
			int order = -(int)(characterSprite[i].transform.parent.position.y * Mathf.Pow(10, decimalPrecision));
			characterSprite[i].sortingOrder = order;
		}
	}

	private void HandleMapRenderer() {

		Transform backSprites = _map.transform.Find("Back Sprites");
		Transform tiles = _map.transform.Find("Tiles");
		Transform sprites = _map.transform.Find("Sprites");

        foreach(Transform sprite in sprites) {
			int order = -(int)(sprite.position.y * Mathf.Pow(10, decimalPrecision));
			SpriteRenderer spriteRenderer = sprite.GetComponent<SpriteRenderer>();
			spriteRenderer.sortingOrder = order;
		}
	}
}
