﻿using UnityEngine;

public class PanelRollDiceTutorialBehaviour : MonoBehaviour {
	enum RollDiceTutorialState { NotRedRollDice, RandomRollDice, PerfectRollDice, Done }
	RollDiceTutorialState rollDiceTutorialState = RollDiceTutorialState.NotRedRollDice;
	
	PanelRollDiceBehaviour _panelRollDice;

	GameObject _tutorialRollDiceRandom;
	GameObject _tutorialRollDicePerfect;

	[SerializeField] int newPerfectChance = 12;
	[SerializeField] int newRandomChance = 38;
	[SerializeField] int doneCount = 2;
	bool hasDoneCount = false;
	bool hasWrong = false;
	bool hasWrongChanged = false;

	void Awake() {
		_panelRollDice = GetComponent<PanelRollDiceBehaviour>();
		_tutorialRollDiceRandom = transform.Find("Tutorial Roll Dice Random").gameObject;
		_tutorialRollDicePerfect = transform.Find("Tutorial Roll Dice Perfect").gameObject;
	}

	void OnEnable() {
		hasDoneCount = false;
		_tutorialRollDiceRandom.SetActive(PanelBehaviour.userCanInput && (rollDiceTutorialState == RollDiceTutorialState.RandomRollDice || rollDiceTutorialState == RollDiceTutorialState.NotRedRollDice));
		_tutorialRollDicePerfect.SetActive(PanelBehaviour.userCanInput && rollDiceTutorialState == RollDiceTutorialState.PerfectRollDice);

		hasWrongChanged = false;
		if(hasWrong) {
			ShakeTutorialRollDice();
		}
	}

	void LateUpdate() {
		if(PanelBehaviour.userCanInput) {
			switch(rollDiceTutorialState) {
                case RollDiceTutorialState.NotRedRollDice: HandleNotRedRollDiceState(); break;
				case RollDiceTutorialState.RandomRollDice: HandleRandomRollDiceState(); break;
				case RollDiceTutorialState.PerfectRollDice: HandlePerfectRollDiceState(); break;
			}
		}
	}
    
    private void HandleNotRedRollDiceState()
    {
        if (_panelRollDice.GetRollDiceOutputType() == PhaseRollDiceBehaviour.RollDiceOutputType.Random || _panelRollDice.GetRollDiceOutputType() == PhaseRollDiceBehaviour.RollDiceOutputType.Perfect)
        {
            hasWrong = false;
            hasWrongChanged = true;
        }
        else if (!hasWrongChanged)
        {
            hasWrong = true;
        }
    }

	private void HandleRandomRollDiceState() {
		if(_panelRollDice.GetRollDiceOutputType() == PhaseRollDiceBehaviour.RollDiceOutputType.Random) {
			hasWrong = false;
			hasWrongChanged = true;
			rollDiceTutorialState = RollDiceTutorialState.PerfectRollDice;
			//_panelRollDice.SetNotForceOutputType(PhaseRollDiceBehaviour.RollDiceOutputType.Perfect);
			_panelRollDice.SetPerfectChance(newPerfectChance);
			_panelRollDice.SetRandomChance(newRandomChance);
		} else if(!hasWrongChanged) {
			hasWrong = true;
		}
	}

	private void HandlePerfectRollDiceState() {
		if(_panelRollDice.GetRollDiceOutputType() == PhaseRollDiceBehaviour.RollDiceOutputType.Perfect) {
			hasWrong = false;
			hasWrongChanged = true;
			if(doneCount > 0) {
				doneCount = !hasDoneCount ? doneCount - 1 : doneCount;
				hasDoneCount = true;
			} else {
				rollDiceTutorialState = RollDiceTutorialState.Done;
				//_panelRollDice.SetNotForceOutputType(PhaseRollDiceBehaviour.RollDiceOutputType.None);
			}
		}
		else if(!hasWrongChanged) { 
			hasWrong = true;
		}
	}

	private void ShakeTutorialRollDice() {
		_tutorialRollDiceRandom.GetComponent<Animator>().SetTrigger("hasWrong");
		_tutorialRollDicePerfect.GetComponent<Animator>().SetTrigger("hasWrong");
	}
}
