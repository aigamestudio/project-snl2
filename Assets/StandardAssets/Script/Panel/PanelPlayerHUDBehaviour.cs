﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PanelPlayerHUDBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	Animator _animator;

	GameObject[] _playerHUD;
	GameObject _highlight;

	[SerializeField] float highlightSpeed = 5.0f;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_animator = GetComponent<Animator>();

		_playerHUD = new GameObject[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i = 0; i < _playerHUD.Length; i++) {
			_playerHUD[i] = transform.Find("Player " + (i + 1).ToString()).gameObject;
		}

		_highlight = transform.Find("Highlight").gameObject;
	}
	
	void Update () {
		HandleHUD();
		HandleHighlight();
	}

	private void SetupHUD() {
		for(int i = 0; i < _playerHUD.Length; i++) {
			int characterType = _characterManager.GetCharacter(i).GetCharacterType();
			_playerHUD[i].GetComponent<Animator>().SetBool("isReady", true);
			_playerHUD[i].GetComponent<Animator>().SetInteger("characterType", characterType);
		}
	}

	private void HandleHUD() {
		for(int i = 0; i < _playerHUD.Length; i++) {
			Text healthText = _playerHUD[i].transform.Find("Health").GetComponent<Text>();
			Text pointText = _playerHUD[i].transform.Find("Point").GetComponent<Text>();
			CharacterBehaviour player = _characterManager.GetCharacter(i);
	        healthText.text = player.currentHP.ToString();
			pointText.text = player.currentPoint.ToString();

			Transform playerCards = player.GetCards();
			Transform cardsCount = _playerHUD[i].transform.Find("CardsCount");
			for(int j=0; j<cardsCount.childCount; j++) {
				cardsCount.GetChild(j).gameObject.SetActive(j<playerCards.childCount);
            }
			
			Text nameText = _playerHUD[i].transform.Find("Name").GetComponent<Text>();
			nameText.text = GameplayManagerBehaviour.playerName[i];
		}
	}

	private void HandleHighlight() {
		int currentPlayer = _characterManager.GetCurrentPlayer();
		Vector2 highlightPos = _highlight.transform.position;
		Vector2 playerHUDPos = _playerHUD[currentPlayer].transform.position;
        _highlight.transform.position = Vector2.MoveTowards(highlightPos, playerHUDPos, highlightSpeed * Time.deltaTime);
	}

	private void ResultScreen() {
        if(GameplayManagerBehaviour.isCampaign) SceneManager.LoadScene("resultscreen");
        else SceneManager.LoadScene("menuscreen");
    }
}
