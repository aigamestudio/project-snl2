﻿using UnityEngine;
using System.Collections.Generic;

public class PanelChooseCardAIController : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseDiscardCardBehaviour _discardCard;

	bool useAI = false;

	[SerializeField] bool isSelectiveOnDiscard = true;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_discardCard = Resources.FindObjectsOfTypeAll<PhaseDiscardCardBehaviour>()[0];
	}

	void OnEnable() {
		useAI = GameplayManagerBehaviour.playerIsBot[_characterManager.GetCurrentPlayer()];
		if(useAI) {
			Invoke("HandleChooseCard", GameplayManagerBehaviour.STANDARD_TIME);
		}
	}

	private void HandleChooseCard() {
		CharacterBehaviour currentAI = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		List<int> discardCardList = new List<int>();
		for(int i = 0; i < currentAI.GetCards().childCount; i++) {
			discardCardList.Add(i);
		}

		if(isSelectiveOnDiscard) {
			for(int i = 0; i < currentAI.GetCards().childCount; i++) {
				CardBehaviour card = currentAI.GetCards().GetChild(i).GetComponent<CardBehaviour>();
				if(card.GetExclusiveCharacter() != null && currentAI.GetCharacterType() == card.GetExclusiveCharacter().GetCharacterType()) {
					discardCardList.RemoveAt(i);
				}
			}
		}

		if(discardCardList.Count > 0) {
			int slot = discardCardList[Random.Range(0, discardCardList.Count)];
			_discardCard.ReplaceSlot(slot);
		}
		else _discardCard.DiscardNewCard();
	}
}
