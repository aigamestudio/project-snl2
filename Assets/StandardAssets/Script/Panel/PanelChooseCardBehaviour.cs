﻿using UnityEngine;
using UnityEngine.UI;

public class PanelChooseCardBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseDiscardCardBehaviour _discardCard;

	CardBehaviour _newCard;
	GameObject _newSlotButton;
	GameObject[] _slotButton;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_discardCard = Resources.FindObjectsOfTypeAll<PhaseDiscardCardBehaviour>()[0];
		_slotButton = new GameObject[GameplayManagerBehaviour.MAX_CARD_SLOT];
	}

	void OnEnable() {
		_newCard = _discardCard._newCard;
		_newSlotButton = transform.Find("New Slot").gameObject;
		for(int i = 0; i < _slotButton.Length; i++) {
			_slotButton[i] = transform.Find("Card Slot").GetChild(i).gameObject;
		}

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		_newSlotButton.GetComponent<Button>().interactable = PanelBehaviour.userCanInput;
		_newSlotButton.GetComponent<Animator>().SetInteger("cardType", _newCard.GetCardType());
		_newSlotButton.GetComponent<Animator>().SetBool("isVisible", PanelBehaviour.userCanInput);
		for(int i = 0; i < _slotButton.Length; i++) {
			CardBehaviour card = currentPlayer.GetCards().GetChild(i).GetComponent<CardBehaviour>();
			_slotButton[i].GetComponent<Button>().interactable = PanelBehaviour.userCanInput;
			_slotButton[i].GetComponent<Animator>().SetInteger("cardType", card.GetCardType());
			_slotButton[i].GetComponent<Animator>().SetBool("isVisible", PanelBehaviour.userCanInput);
		}
	}
	
	public void ReplaceSlotButton(int slot) {
		_discardCard.ReplaceSlot(slot);
	}
	
	public void DiscardNewCardButton() {
		_discardCard.DiscardNewCard();
	}
}
