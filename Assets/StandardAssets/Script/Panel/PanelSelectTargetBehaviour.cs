﻿using UnityEngine;
using UnityEngine.UI;

public class PanelSelectTargetBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardSelectTargetBehaviour _useCardSelectTarget;

	Button[] _arrowButton;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardSelectTarget = Resources.FindObjectsOfTypeAll<PhaseUseCardSelectTargetBehaviour>()[0];

		_arrowButton = new Button[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i=0; i<_arrowButton.Length; i++) {
			_arrowButton[i] = transform.Find("Target Player " + (i+1).ToString()).GetComponent<Button>();
		}
	}

	void OnEnable() {
		int currentPlayer = _characterManager.GetCurrentPlayer();
		for(int i = 0; i < _arrowButton.Length; i++) {
			if(i == currentPlayer) _arrowButton[i].interactable = false;
			else _arrowButton[i].interactable = PanelBehaviour.userCanInput;
			_arrowButton[i].GetComponent<Animator>().SetBool("interactable", _arrowButton[i].interactable);
		}
	}

	public void SetTargetButton(int playerId) {
		_useCardSelectTarget.UseCardActionPhase(playerId);
	}
}
