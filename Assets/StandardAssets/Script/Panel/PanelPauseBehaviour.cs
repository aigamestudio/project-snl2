﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelPauseBehaviour : MonoBehaviour {
	[SerializeField] bool canUseEscape = true;
	[SerializeField] bool isZeroTimeScale = false;

	AudioManagerBehaviour _audioManager;

	void Awake() {
		_audioManager = GameObject.FindObjectOfType<AudioManagerBehaviour>();
	}
	
	void Update () {
		GetComponent<Animator>().SetBool("musicIsMute", _audioManager.GetMusicIsMute());
		GetComponent<Animator>().SetBool("sfxIsMute", _audioManager.GetSFXIsMute());

		if(Input.GetKey(KeyCode.Escape)) {
			ExitPausePanelButton();
		}

		if(isZeroTimeScale) {
			GameplaySpeedController.PauseGameSpeed();
		}
	}

	public void MuteMusicButton() {
		_audioManager.ToggleMusic();
	}

	public void MuteSFXButton() {
		_audioManager.ToggleSFX();
	}

	public void RateButton() {
		Application.OpenURL("market://details?id=com.aigamestudio.dadufall");
	}

	public void ExitPausePanelButton() {
		gameObject.SetActive(false);
		GameplaySpeedController.RestoreGameSpeed();
	}

	public void QuitGameplayButton() {
		SceneManager.LoadScene("menuscreen");
	}
}
