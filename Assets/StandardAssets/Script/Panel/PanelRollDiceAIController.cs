﻿using UnityEngine;

public class PanelRollDiceAIController : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseRollDiceBehaviour _rollDice;
	PhaseCheckRollDicePairBehaviour _checkRollDicePair;

	bool useAI = false;
	[SerializeField][Range(0, 100)] int unluckyChance = 1;
	[SerializeField][Range(0, 100)] int perfectChance = 94;
	[SerializeField] bool cautionsOverluck = true;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_rollDice = Resources.FindObjectsOfTypeAll<PhaseRollDiceBehaviour>()[0];
		_checkRollDicePair = Resources.FindObjectsOfTypeAll<PhaseCheckRollDicePairBehaviour>()[0];
	}
	
	void OnEnable () {
		useAI = GameplayManagerBehaviour.playerIsBot[_characterManager.GetCurrentPlayer()];
		if(useAI) {
			Invoke("AIRollDice", GameplayManagerBehaviour.STANDARD_TIME);
		}
	}

	private void AIRollDice() {
		int randomChanceType = Random.Range(0, 100);
		PhaseRollDiceBehaviour.RollDiceOutputType rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Random;

		if(randomChanceType <= unluckyChance) rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Unlucky;
		else if(randomChanceType <= perfectChance + unluckyChance) {
			if(cautionsOverluck && _checkRollDicePair.GetPairedCount() + 1 >= GameplayManagerBehaviour.OVERLUCK_DICE_PAIRED) {
				rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Random;
			} else {
				rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Perfect;
			}
		}
	
		_rollDice.RandomizeDice(rollDiceOutputType, Random.Range(0.0f, 1.0f));
		_rollDice.SyncRollDiceInput(_rollDice.GetRollDiceInput(), (int)rollDiceOutputType);
	}
}
