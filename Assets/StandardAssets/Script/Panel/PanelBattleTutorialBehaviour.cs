﻿using UnityEngine;

public class PanelBattleTutorialBehaviour : MonoBehaviour {
	PhaseCheckBattleBattleBehaviour _checkBattle_battle;
	PanelBattleBehaviour _panelBattle;

	GameObject _tutorialAnswer1;
	GameObject _tutorialAnswer2;

	void Awake() {
		_checkBattle_battle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBattleBehaviour>()[0];
		_panelBattle = GetComponent<PanelBattleBehaviour>();
		_tutorialAnswer1 = _panelBattle._battleFrame_choiceButton1.transform.Find("Tutorial Answer").gameObject;
		_tutorialAnswer2 = _panelBattle._battleFrame_choiceButton2.transform.Find("Tutorial Answer").gameObject;
	}

	void Update() {
		bool isInChooseAnswer = _checkBattle_battle.GetBattlePhase() == PhaseCheckBattleBattleBehaviour.BattlePhase.ChooseAnswer;
		_tutorialAnswer1.SetActive(isInChooseAnswer && _checkBattle_battle.GetTrueChoice() == 0);
		_tutorialAnswer2.SetActive(isInChooseAnswer && _checkBattle_battle.GetTrueChoice() == 1);
	}
}
