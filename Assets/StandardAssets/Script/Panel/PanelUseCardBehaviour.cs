﻿using UnityEngine;
using UnityEngine.UI;

public class PanelUseCardBehaviour : MonoBehaviour {
	protected CharacterManagerBehaviour _characterManager;
	CardDeckBehaviour _cardDeck;
	PhaseUseCardBehaviour _useCard;

	Animator _animator;
	GameObject[] _slotButton;
	[HideInInspector] public Button _rollDiceButton;
	[HideInInspector] public Button _pauseButton;
	[HideInInspector] public Button _gameSpeedButton;
	[HideInInspector] public Button _deckDetailButton;

	Transform _cardDetail;
	GameObject _cardDetail_playerUI;
	Text _cardDetail_playerNameText;
	Text _cardDetail_playerMarblesText;
	GameObject _cardDetail_cardUI;
	Text _cardDetail_cardDescText;
	Button _cardDetail_useSpecialButton;
	[HideInInspector] public Button _cardDetail_useCardButton;

	CardBehaviour _cardUsed;
	int slotUsed = -1;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_cardDeck = GameObject.FindObjectOfType<CardDeckBehaviour>();
		_useCard = Resources.FindObjectsOfTypeAll<PhaseUseCardBehaviour>()[0];
		_animator = GetComponent<Animator>();

		_slotButton = new GameObject[GameplayManagerBehaviour.MAX_CARD_SLOT];

		_rollDiceButton = transform.Find("Roll Dice Button").GetComponent<Button>();
		_pauseButton = transform.Find("Pause Button").GetComponent<Button>();
		_gameSpeedButton = transform.Find("Game Speed Button").GetComponent<Button>();
		_deckDetailButton = transform.Find("Deck Detail Button").GetComponent<Button>();

		_cardDetail = transform.Find("CardDetail");
		_cardDetail_playerUI = _cardDetail.Find("Player Mask").GetChild(0).gameObject;
		_cardDetail_playerNameText = _cardDetail.Find("Player Name").GetComponent<Text>();
		_cardDetail_playerMarblesText = _cardDetail.Find("Player Marbles").GetComponent<Text>();
		_cardDetail_cardUI = _cardDetail.Find("Mask").Find("Scroll").Find("Card UI").gameObject;
		_cardDetail_cardDescText = _cardDetail.Find("Mask").Find("Scroll").Find("Card Desc").GetComponent<Text>();
		_cardDetail_useSpecialButton = _cardDetail.Find("Use Special Button").GetComponent<Button>();
		_cardDetail_useCardButton = _cardDetail.Find("Use Card Button").GetComponent<Button>();
	}

	void OnEnable() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		Transform playerCards = currentPlayer.GetCards();

		_rollDiceButton.gameObject.SetActive(PanelBehaviour.userCanInput);
		_pauseButton.gameObject.SetActive(PanelBehaviour.userCanInput);
		_gameSpeedButton.gameObject.SetActive(PanelBehaviour.userCanInput);
		_deckDetailButton.gameObject.SetActive(PanelBehaviour.userCanInput);

		_animator.SetInteger("timeScale", GameplaySpeedController.GetGameSpeed());
		_deckDetailButton.transform.Find("Last Card UI").GetComponent<Animator>().SetInteger("cardType", _cardDeck.ViewLastCardUsedType());
		_deckDetailButton.transform.Find("Last Card UI").GetComponent<Animator>().SetBool("isVisible", true);
		_deckDetailButton.transform.Find("Available Card Text").GetComponent<Text>().text = _cardDeck.GetAvailableDeck().childCount.ToString();

		for(int i=0; i<_slotButton.Length; i++) {
			_slotButton[i] = transform.Find("Cards UI").GetChild(i).gameObject;
			if(i < playerCards.childCount) {
				CardBehaviour card = playerCards.GetChild(i).GetComponent<CardBehaviour>();
				_slotButton[i].SetActive(true);
				_slotButton[i].GetComponent<Button>().interactable = PanelBehaviour.userCanInput && _useCard.canUseCard;
				_slotButton[i].GetComponent<Animator>().SetInteger("cardType", card.GetCardType());
				_slotButton[i].GetComponent<Animator>().SetBool("isVisible", PanelBehaviour.userCanInput);
			} else {
				_slotButton[i].SetActive(false);
			}
		}
	}

	public virtual void CheckCardDetailButton(int slot) {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		slotUsed = slot;
		_cardUsed = currentPlayer.GetCards().GetChild(slot).GetComponent<CardBehaviour>();

		_cardDetail_cardUI.GetComponent<Animator>().SetTrigger("hide");
		_cardDetail_cardUI.GetComponent<Animator>().SetInteger("cardType", _cardUsed.GetCardType());
		_cardDetail_cardUI.GetComponent<Animator>().SetBool("isVisible", PanelBehaviour.userCanInput);

		_cardDetail_playerUI.GetComponent<Animator>().SetInteger("characterType", currentPlayer.GetCharacterType());
		_cardDetail_playerNameText.text = currentPlayer.GetCharacterName();
		_cardDetail_playerMarblesText.text = currentPlayer.currentPoint + " Marbles";

		if(_cardUsed.GetExclusiveCharacter() && _cardUsed.IsExclusiveCharacter(currentPlayer)) {
			_cardDetail_cardDescText.text = _cardUsed.GetCardDesc() + "\n\n" + _cardUsed.GetCardSpecialDesc(currentPlayer.GetCurrentLevel());
		} else if(_cardUsed.GetHasSpecial()) {
			_cardDetail_cardDescText.text = _cardUsed.GetCardDesc() + "\n\n" + _cardUsed.GetCardSpecialDesc();
        } else {
			_cardDetail_cardDescText.text = _cardUsed.GetCardDesc();
		}

		_cardDetail_useSpecialButton.gameObject.SetActive(_cardUsed.GetHasSpecial());
		_cardDetail_useSpecialButton.interactable = PanelBehaviour.userCanInput && _cardUsed.CheckSpecialRequirement(currentPlayer);
		_cardDetail_useCardButton.interactable = PanelBehaviour.userCanInput && currentPlayer.currentPoint >= _cardUsed.GetCardReqPoint();

		string cardNormalMoveTextStr = _cardUsed.GetCardNormalMoveName() +
			"\n<size='10'>(" + _cardUsed.GetCardReqPoint() + " Marbles)</size>";
		_cardDetail_useCardButton.transform.GetChild(0).GetComponent<Text>().text = cardNormalMoveTextStr;

		if(_cardUsed.GetHasSpecial()) {
			string cardSpecialMoveTextStr = _cardUsed.GetCardSpecialMoveName() + "\n<size='10'>(" +
				_cardUsed.GetCardSpecialReqPoint(currentPlayer.GetCurrentLevel()) + " Marbles" +
				(_cardUsed.GetExclusiveCharacter() ? ", " + _cardUsed.GetExclusiveCharacter().GetCharacterName() + " only)</size>" : ")</size>");
			_cardDetail_useSpecialButton.transform.GetChild(0).GetComponent<Text>().text = cardSpecialMoveTextStr;
		}

		_animator.SetBool("showDetail", true);
	}

	public void UseCardButton() {
		_useCard.UseCard(slotUsed, false);
	}

	public void UseSpecialButton() {
		_useCard.UseCard(slotUsed, true);
	}

	public void HideDetailButton() {
		_animator.SetBool("showDetail", false);
	}

	public void RollDiceButton() {
		_useCard.RollDicePhase();
	}

	public void ChangeGameSpeedButton() {
		GameplaySpeedController.ChangeGameSpeed();
		_animator.SetInteger("timeScale", GameplaySpeedController.GetGameSpeed());
	}
}
