﻿using UnityEngine;
using System.Collections.Generic;

public class PanelUseCardAIController : MonoBehaviour {
	MapBehaviour _map;
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardBehaviour _useCard;

	[SerializeField] bool isManagementOnUseCard = true;
	[SerializeField] int useCardChance = 75;
	[SerializeField] int useSpecialChance = 95;

	bool useAI = false;

	void Awake () {
		_useCard = Resources.FindObjectsOfTypeAll<PhaseUseCardBehaviour>()[0];
		_map = GameObject.FindObjectOfType<MapBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
	}

	void OnEnable() {
		useAI = GameplayManagerBehaviour.playerIsBot[_characterManager.GetCurrentPlayer()];
		if(useAI) {
			Invoke("HandleUseCard", GameplayManagerBehaviour.STANDARD_TIME);
		}
	}
	
	private void HandleUseCard() {
		CharacterBehaviour currentAI = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		Transform currentAICards = currentAI.GetCards();
		if(_useCard.canUseCard && currentAICards.childCount > 0) {
			if(isManagementOnUseCard) ManagementUseCard(currentAI, currentAICards);
			else RandomUseCard(currentAI, currentAICards); 	
		} else {
			_useCard.RollDicePhase();
		}
	}

	private void RandomUseCard(CharacterBehaviour currentAI, Transform currentAICards) {
		List<CardBehaviour> listCardCanBeUsed = new List<CardBehaviour>();
		for(int i = 0; i < currentAICards.childCount; i++) {
			int cardReqPoint = currentAICards.GetChild(i).GetComponent<CardBehaviour>().GetCardReqPoint();
			if(currentAI.currentPoint >= cardReqPoint) {
				listCardCanBeUsed.Add(currentAICards.GetChild(i).GetComponent<CardBehaviour>());
			}
		}

		if(listCardCanBeUsed.Count > 0) {
			int useCardRandom = Random.Range(0, 100);
			int difficulty = GameplayManagerBehaviour.aiDifficulty - 1;
			if(useCardRandom < useCardChance) {
				int cardWillBeUsed = Random.Range(0, listCardCanBeUsed.Count);
				CardBehaviour cardUsed = listCardCanBeUsed[cardWillBeUsed];
				bool useSpecial = Random.Range(0, 100) <= useSpecialChance && cardUsed.GetHasSpecial() && cardUsed.CheckSpecialRequirement(currentAI);
				_useCard.UseCard(cardWillBeUsed, useSpecial);
				return;
			}
		}

		_useCard.RollDicePhase();
	}

	private void ManagementUseCard(CharacterBehaviour currentAI, Transform currentAICards) {
		for(int i = 0; i < currentAICards.childCount; i++) {
			CardBehaviour card = currentAICards.GetChild(i).GetComponent<CardBehaviour>();
			bool isValuable = GenerateCardValuable(currentAI, card);
			if(card.GetExclusiveCharacter() != null && currentAI.GetCharacterType() == card.GetExclusiveCharacter().GetCharacterType() && isValuable) {
				if(currentAI.currentPoint >= card.GetCardSpecialReqPoint(currentAI.GetCurrentLevel())) {
					_useCard.UseCard(i, true);
				}
				else _useCard.RollDicePhase();
				return;
            }
		}

		int difficulty = GameplayManagerBehaviour.aiDifficulty - 1;
		int[] slotPriorities = GenerateCardPriorities(currentAICards);
		int[] orderedSlotPriorites = new int[slotPriorities.Length];
		slotPriorities.CopyTo(orderedSlotPriorites, 0);
		System.Array.Sort(orderedSlotPriorites);
		for(int i=0; i<currentAICards.childCount; i++) {
			int index = System.Array.FindIndex(slotPriorities, j => j == orderedSlotPriorites[i]);
			CardBehaviour cardUsed = currentAICards.GetChild(index).GetComponent<CardBehaviour>();
			bool isValuable = GenerateCardValuable(currentAI, cardUsed);
			if(isValuable && currentAI.currentPoint >= cardUsed.GetCardReqPoint()) {
				bool useSpecial = Random.Range(0, 100) <= useSpecialChance && cardUsed.GetHasSpecial() && cardUsed.CheckSpecialRequirement(currentAI);
				_useCard.UseCard(index, useSpecial);
				return;
			}
		}

		_useCard.RollDicePhase();
	}

	private int[] GenerateCardPriorities(Transform currentAICards) {
		int[] slotPriorities = new int[] { int.MaxValue, int.MaxValue, int.MaxValue, int.MaxValue };
		for(int i = 0; i < currentAICards.childCount; i++) {
			CardBehaviour card = currentAICards.GetChild(i).GetComponent<CardBehaviour>();
			switch(card.GetCardType()) {
				case 3: case 4: slotPriorities[i] = 1; break;
				case 5: case 6: slotPriorities[i] = 4; break;
				case 7: case 8: slotPriorities[i] = 9; break;
				case 9: case 10: slotPriorities[i] = 2; break;
				case 11: case 12: slotPriorities[i] = 6; break;
				case 13: case 14: slotPriorities[i] = 7; break;
				case 15: case 16: slotPriorities[i] = 8; break;
				case 17: case 18: slotPriorities[i] = 3; break;
				case 19: slotPriorities[i] = 10; break;
				case 20: slotPriorities[i] = 5; break;
			}
		}

		return slotPriorities;
	}

	private bool GenerateCardValuable(CharacterBehaviour currentAI, CardBehaviour cardUsed) {
		switch(cardUsed.GetCardType()) {
			case 3: case 4:
				return _characterManager.GetClosestCharacters(currentAI)[0].position - currentAI.position >= 6;
			case 9:	case 10:
				return _map.GetMapTile(_map.GetMapTileCount() - 1).tileId - currentAI.position >= 6;
			case 17: case 18:
				return currentAI.currentHP <= 3;
			case 20:
				return currentAI.currentHP >= 3;
			default: return true;
		}
	}
}
