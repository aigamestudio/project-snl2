﻿using UnityEngine;
using UnityEngine.UI;

public class PanelWinBehaviour : MonoBehaviour {
	PanelPlayerHUDBehaviour _playerHUD;
	PhaseWinBehaviour _win;

	Animator _animator;
	GameObject _playerUI;
	GameObject[] _playerRank;

	[SerializeField] string pointCurrency = "Marbles";

	void Awake() {
		_playerHUD = GameObject.FindObjectOfType<PanelPlayerHUDBehaviour>();
		_win = Resources.FindObjectsOfTypeAll<PhaseWinBehaviour>()[0];

		_animator = GetComponent<Animator>();
		_playerUI = transform.Find("Player UI").gameObject;

		_playerRank = new GameObject[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			_playerRank[i] = transform.Find("Rank " + (i + 1)).gameObject;
		}
	}

	void Start() {
		_playerHUD.GetComponent<Animator>().SetTrigger("win");
		_playerUI.GetComponent<Animator>().SetInteger("characterType", _win.GetCharacterOrder()[0].GetCharacterType());

		for(int i = 0; i < _playerRank.Length; i++) {
			Text pointText = _playerRank[i].transform.Find("Point Text").GetComponent<Text>();
			pointText.text = _win.GetCharacterOrder()[i].currentPoint + " " + pointCurrency;

			Animator playerMask = _playerRank[i].transform.Find("Mask").GetChild(0).GetComponent<Animator>();
			playerMask.SetInteger("characterType", _win.GetCharacterOrder()[i].GetCharacterType());
			if(i == 0) playerMask.SetTrigger("win");
			else playerMask.SetTrigger("lose");
		}
	}

	public void NextButton() {
		_animator.SetInteger("next", _animator.GetInteger("next") + 1);
		if(_animator.GetInteger("next") > 1) _playerHUD.GetComponent<Animator>().SetTrigger("quit");
	}

	public void PlaySound(AudioClip clip) {
		GetComponent<AudioSource>().PlayOneShot(clip);
	}
}
