﻿using UnityEngine;
using UnityEngine.UI;

public class PanelBattleBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseCheckBattleBattleBehaviour _checkBattle_battle;
	Animator _animator;
	AudioSource _audioSource;

	Transform _initialFrame;
	GameObject _initialFrame_player1UI;
	Text _initialFrame_player1Name;
    Button _initialFrame_player1ReadyButton;
    GameObject _initialFrame_player2UI;
	Text _initialFrame_player2Name;
    Button _initialFrame_player2ReadyButton;

    Transform _battleFrame;
	GameObject _battleFrame_player1UI;
	GameObject _battleFrame_player2UI;
	Text _battleFrame_player1_calloutText;
	Text _battleFrame_player2_calloutText;
	Text _battleFrame_questionText;
	[HideInInspector] public Button _battleFrame_choiceButton1;
    Button _battleFrame_player1ChoiceButton1;
    Button _battleFrame_player2ChoiceButton1;
    [HideInInspector] public Button _battleFrame_choiceButton2;
    Button _battleFrame_player1ChoiceButton2;
    Button _battleFrame_player2ChoiceButton2;
    Text _battleFrame_player1_HPText;
	Text _battleFrame_player2_HPText;

	[SerializeField] AudioClip _trueChoiceSFX;
	[SerializeField] AudioClip _falseChoiceSFX;

	bool isInteractable = true;
    bool isChallengerReady = false;
    bool isChallengedReady = false;

    void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_checkBattle_battle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBattleBehaviour>()[0];
		_animator = GetComponent<Animator>();
		_audioSource = GetComponent<AudioSource>();

		_initialFrame = transform.Find("Initial");
		_initialFrame_player1UI = _initialFrame.Find("Frame 1").GetChild(0).Find("Player 1 UI").gameObject;
		_initialFrame_player1Name = _initialFrame.Find("Frame 1").GetChild(0).Find("Player 1 Name").GetComponent<Text>();
        _initialFrame_player1ReadyButton = _initialFrame.Find("Player 1 Ready Button").GetComponent<Button>();
        _initialFrame_player2UI = _initialFrame.Find("Frame 2").GetChild(0).Find("Player 2 UI").gameObject;
		_initialFrame_player2Name = _initialFrame.Find("Frame 2").GetChild(0).Find("Player 2 Name").GetComponent<Text>();
        _initialFrame_player2ReadyButton = _initialFrame.Find("Player 2 Ready Button").GetComponent<Button>();

        _battleFrame = transform.Find("Battle");
		_battleFrame_player1UI = _battleFrame.Find("Frame").GetChild(0).Find("Player 1 UI").gameObject;
		_battleFrame_player2UI = _battleFrame.Find("Frame").GetChild(0).Find("Player 2 UI").gameObject;
		_battleFrame_player1_calloutText = _battleFrame.Find("Frame").GetChild(0).Find("Callout 1").GetChild(0).GetComponent<Text>();
		_battleFrame_player2_calloutText = _battleFrame.Find("Frame").GetChild(0).Find("Callout 2").GetChild(0).GetComponent<Text>();
		_battleFrame_questionText = _battleFrame.Find("Soal").GetComponent<Text>();
		_battleFrame_choiceButton1 = _battleFrame.Find("Choice 1").GetComponent<Button>();
        _battleFrame_player1ChoiceButton1 = _battleFrame.Find("Player 1 Choice 1").GetComponent<Button>();
        _battleFrame_player2ChoiceButton1 = _battleFrame.Find("Player 2 Choice 1").GetComponent<Button>();
        _battleFrame_choiceButton2 = _battleFrame.Find("Choice 2").GetComponent<Button>();
        _battleFrame_player1ChoiceButton2 = _battleFrame.Find("Player 1 Choice 2").GetComponent<Button>();
        _battleFrame_player2ChoiceButton2 = _battleFrame.Find("Player 2 Choice 2").GetComponent<Button>();
        _battleFrame_player1_HPText = _battleFrame.Find("Frame").GetChild(0).Find("Player 1 HP").GetComponent<Text>();
		_battleFrame_player2_HPText = _battleFrame.Find("Frame").GetChild(0).Find("Player 2 HP").GetComponent<Text>();
	}

	void OnEnable() {
		bool challengerIsUser = !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenger.playerId];
		bool challengedIsUser = !_checkBattle_battle.GetIsBossBattle() && !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenged.playerId];
        bool isMultiplayerBattle = challengerIsUser && challengedIsUser;
        isChallengerReady = isChallengedReady = !(challengerIsUser && challengedIsUser);
		isInteractable = challengerIsUser || challengedIsUser;
		if(challengerIsUser || challengedIsUser) Handheld.Vibrate();

		_battleFrame_questionText.transform.GetChild(0).GetComponent<Animator>().speed = 8;
		_battleFrame_questionText.transform.GetChild(1).GetComponent<Animator>().speed = 8;
		_battleFrame_choiceButton1.gameObject.SetActive(challengerIsUser ^ challengedIsUser);
        _battleFrame_player1ChoiceButton1.gameObject.SetActive(isMultiplayerBattle);
        _battleFrame_player2ChoiceButton1.gameObject.SetActive(isMultiplayerBattle);
        _battleFrame_choiceButton2.gameObject.SetActive(challengerIsUser ^ challengedIsUser);
        _battleFrame_player1ChoiceButton2.gameObject.SetActive(isMultiplayerBattle);
        _battleFrame_player2ChoiceButton2.gameObject.SetActive(isMultiplayerBattle);

        SetupPlayerUI(isMultiplayerBattle);
        if(!isMultiplayerBattle) Invoke("StartBattle", GameplayManagerBehaviour.STANDARD_TIME * 3.0f);
	}

	void Update() {
        _animator.SetInteger("battleCount", _checkBattle_battle.GetBattleCount());
		_battleFrame_player1_HPText.text = _checkBattle_battle._challenger.currentHP.ToString();
		_battleFrame_player2_HPText.text = _checkBattle_battle._challenged.currentHP.ToString();
		switch(_checkBattle_battle.GetBattlePhase()) {
            case PhaseCheckBattleBattleBehaviour.BattlePhase.Idle:
                if(GenerateIsMultiplayerBattle())
                {
                    _initialFrame_player1ReadyButton.interactable = !isChallengerReady;
                    _initialFrame_player1ReadyButton.transform.GetChild(0).GetComponent<Text>().text = GameplayManagerBehaviour.playerName[_checkBattle_battle._challenger.playerId] + "\nready!";
                    _initialFrame_player2ReadyButton.interactable = !isChallengedReady;
                    _initialFrame_player2ReadyButton.transform.GetChild(0).GetComponent<Text>().text = GameplayManagerBehaviour.playerName[_checkBattle_battle._challenged.playerId] + "\nready!";
                }
                break;
			case PhaseCheckBattleBattleBehaviour.BattlePhase.SetupQuestion:
                _battleFrame_choiceButton1.interactable = isInteractable; // penting ga si variable isinteractablenya? ga true aja getu
                _battleFrame_player1ChoiceButton1.interactable = isInteractable;
                _battleFrame_player2ChoiceButton1.interactable = isInteractable;
                _battleFrame_choiceButton2.interactable = isInteractable;
                _battleFrame_player1ChoiceButton2.interactable = isInteractable;
                _battleFrame_player2ChoiceButton2.interactable = isInteractable;
                _battleFrame_questionText.transform.GetChild(0).GetComponent<Animator>().Play("dice_idle");
				_battleFrame_questionText.transform.GetChild(1).GetComponent<Animator>().Play("dice_idle");
				break;
			case PhaseCheckBattleBattleBehaviour.BattlePhase.ChooseAnswer:
				_battleFrame_player1_calloutText.text = "...";
				_battleFrame_player2_calloutText.text = "...";
				_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("idle");
				_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("idle");
				_battleFrame_questionText.text = GenerateQuestionText();
				_battleFrame_choiceButton1.transform.GetChild(0).GetComponent<Text>().text = _checkBattle_battle.GetChoiceNum()[0].ToString();
                _battleFrame_player1ChoiceButton1.transform.GetChild(0).GetComponent<Text>().text = _checkBattle_battle.GetChoiceNum()[0].ToString();
                _battleFrame_player2ChoiceButton1.transform.GetChild(0).GetComponent<Text>().text = _checkBattle_battle.GetChoiceNum()[0].ToString();
                _battleFrame_choiceButton2.transform.GetChild(0).GetComponent<Text>().text = _checkBattle_battle.GetChoiceNum()[1].ToString();
                _battleFrame_player1ChoiceButton2.transform.GetChild(0).GetComponent<Text>().text = _checkBattle_battle.GetChoiceNum()[1].ToString();
                _battleFrame_player2ChoiceButton2.transform.GetChild(0).GetComponent<Text>().text = _checkBattle_battle.GetChoiceNum()[1].ToString();
                break;
			case PhaseCheckBattleBattleBehaviour.BattlePhase.CheckExit:
				_battleFrame_choiceButton1.interactable = false;
                _battleFrame_player1ChoiceButton1.interactable = false;
                _battleFrame_player2ChoiceButton1.interactable = false;
                _battleFrame_choiceButton2.interactable = false;
                _battleFrame_player1ChoiceButton2.interactable = false;
                _battleFrame_player2ChoiceButton2.interactable = false;

                if (_checkBattle_battle.challengerChoice != -1) {
					_battleFrame_player1_calloutText.text = _checkBattle_battle.GetChoiceNum()[_checkBattle_battle.challengerChoice].ToString();
					_battleFrame_player2_calloutText.text = "...";
					if(_checkBattle_battle.challengerChoice == _checkBattle_battle.GetTrueChoice()) {
						_audioSource.PlayOneShot(_trueChoiceSFX);
						if(!_checkBattle_battle.GetIsBossBattle() && !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenged.playerId]) Handheld.Vibrate();
						if(_checkBattle_battle._challenged.currentHP == 0) {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("win");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("lose");
						}
						else {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("attack");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("hit");
						}
					}
					else {
						_audioSource.PlayOneShot(_falseChoiceSFX);
						if(!GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenger.playerId]) Handheld.Vibrate();
						if(_checkBattle_battle._challenger.currentHP == 0) {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("lose");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("win");
						}
						else {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("hit");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("attack");
						}
					}
				}
				else if(_checkBattle_battle.challengedChoice != -1) {
					_battleFrame_player1_calloutText.text = "...";
					_battleFrame_player2_calloutText.text = _checkBattle_battle.GetChoiceNum()[_checkBattle_battle.challengedChoice].ToString();
					if(_checkBattle_battle.challengedChoice == _checkBattle_battle.GetTrueChoice()) {
						_audioSource.PlayOneShot(_trueChoiceSFX);
						if(!GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenger.playerId]) Handheld.Vibrate();
						if(_checkBattle_battle._challenger.currentHP == 0) {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("lose");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("win");
						}
						else {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("hit");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("attack");
						}
					}
					else {
						_audioSource.PlayOneShot(_falseChoiceSFX);
						if(!_checkBattle_battle.GetIsBossBattle() && !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenged.playerId]) Handheld.Vibrate();
						if(_checkBattle_battle._challenged.currentHP == 0) {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("win");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("lose");
						}
						else {
							_battleFrame_player1UI.GetComponent<Animator>().SetTrigger("attack");
							_battleFrame_player2UI.GetComponent<Animator>().SetTrigger("hit");
						}
					}
				}
				break;
		}
	}

	private void SetupPlayerUI(bool isMultiplayerBattle) {
		CharacterBehaviour challenger = _checkBattle_battle._challenger;
		CharacterBehaviour challenged = _checkBattle_battle._challenged;

        _initialFrame_player1UI.GetComponent<Animator>().SetInteger("characterType", challenger.GetCharacterType());
		_initialFrame_player1UI.GetComponent<Animator>().SetTrigger("attack");
		_initialFrame_player1Name.text = challenger.GetCharacterName();
        _initialFrame_player1ReadyButton.gameObject.SetActive(isMultiplayerBattle);
		_initialFrame_player2UI.GetComponent<Animator>().SetInteger("characterType", challenged.GetCharacterType());
		_initialFrame_player2UI.GetComponent<Animator>().SetTrigger("attack");
		_initialFrame_player2Name.text = challenged.GetCharacterName();
        _initialFrame_player2ReadyButton.gameObject.SetActive(isMultiplayerBattle);
    }

	private string GenerateQuestionText() {
		_battleFrame_questionText.transform.GetChild(0).GetComponent<Animator>().SetInteger("output", _checkBattle_battle.GetNum()[0]);
		_battleFrame_questionText.transform.GetChild(1).GetComponent<Animator>().SetInteger("output", _checkBattle_battle.GetNum()[1]);

		PhaseCheckBattleBattleBehaviour.BattleType battleType = _checkBattle_battle.GetBattleType();
		switch(battleType) {
			case PhaseCheckBattleBattleBehaviour.BattleType.Addition: return " + ";
			case PhaseCheckBattleBattleBehaviour.BattleType.Substraction: return " - ";
			case PhaseCheckBattleBattleBehaviour.BattleType.Multiplication: return " x ";
			default: return "";
		}
	}

	private void StartBattle() {
		_animator.SetTrigger("startBattle");

		CharacterBehaviour challenger = _checkBattle_battle._challenger;
		CharacterBehaviour challenged = _checkBattle_battle._challenged;
		_battleFrame_player1UI.GetComponent<Animator>().SetInteger("characterType", challenger.GetCharacterType());
		_battleFrame_player2UI.GetComponent<Animator>().SetInteger("characterType", challenged.GetCharacterType());
	}

    private bool GenerateIsMultiplayerBattle()
    {
        bool challengerIsUser = !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenger.playerId];
        bool challengedIsUser = !_checkBattle_battle.GetIsBossBattle() && !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenged.playerId];
        return challengerIsUser && challengedIsUser;
    }

	public void ResetBattle() {
		_checkBattle_battle.ResetBattle();
	}

    public void SetPlayerUserChallengerChoiceButton(int choice) { _checkBattle_battle.challengerChoice = choice; } // kalo multiplayer pake ini
    public void SetPlayerUserChallengedChoiceButton(int choice) { _checkBattle_battle.challengedChoice = choice; } 
    public void SetPlayerChoiceButton(int choice) { // kalo campaign pake ini
		if(!GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenger.playerId]){
            SetPlayerUserChallengerChoiceButton(choice);
            return;
        }

		if(!_checkBattle_battle.GetIsBossBattle() && !GameplayManagerBehaviour.playerIsBot[_checkBattle_battle._challenged.playerId]) {
            SetPlayerUserChallengedChoiceButton(choice);
            return;
		}
    }

    public void ReadyBattleButton(bool isChallenger)
    {
        if (isChallenger) isChallengerReady = true;
        else isChallengedReady = true;

        if (isChallengerReady && isChallengedReady) StartBattle();
    }
}