﻿using UnityEngine;
using UnityEngine.UI;

public class PanelDeckDetailBehaviour : MonoBehaviour {
	CardDeckBehaviour _cardDeck;

	Transform _content;
	Transform _cardDetail;

	void Awake () {
		_cardDeck = GameObject.FindObjectOfType<CardDeckBehaviour>();
		_content = transform.Find("Frame").GetChild(0).GetChild(0);
		_cardDetail = _content.Find("Card Detail");
	}

	void OnEnable() {
		_content.transform.Find("Last Card UI").GetComponent<Animator>().SetInteger("cardType", _cardDeck.ViewLastCardUsedType());
		_content.transform.Find("Last Card UI").GetComponent<Animator>().SetBool("isVisible", true);
		_content.transform.Find("Available Card Text").GetComponent<Text>().text = _cardDeck.GetAvailableDeck().childCount.ToString();

		for(int i=0; i<_cardDetail.childCount; i++) {
			_cardDetail.GetChild(i).Find("Card Name").GetComponent<Text>().text = _cardDeck.GetAvailableCardTypePrefab()[i].GetCardName();
			_cardDetail.GetChild(i).Find("Card UI").GetComponent<Animator>().SetInteger("cardType", _cardDeck.GetAvailableCardTypePrefab()[i].GetCardType());
			_cardDetail.GetChild(i).Find("Card UI").GetComponent<Animator>().SetBool("isVisible", true);
			_cardDetail.GetChild(i).Find("Card Count").GetComponent<Text>().text = _cardDeck.CountCardTypeOnAvailableDeck(i).ToString();
        }
	}
}
