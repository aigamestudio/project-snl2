﻿using UnityEngine;

public class PanelUseCardTutorialBehaviour : MonoBehaviour {
	enum UseCardTutorialState { TapToRollDice, TapToUseCard, TapToUseSpecialMove, Done }
	UseCardTutorialState useCardTutorialState = UseCardTutorialState.TapToRollDice;

	PanelUseCardBehaviour _panelUseCard;

	GameObject _tutorialTapToRollDice;
	GameObject _tutorialTapToUseCard;
	GameObject _tutorialTapToUseSpecialMove;

	void Awake() {
		_panelUseCard = GetComponent<PanelUseCardBehaviour>();
		_tutorialTapToRollDice = transform.Find("Tutorial Tap to Roll Dice").gameObject;
		_tutorialTapToUseCard = transform.Find("Tutorial Tap to Use Card").gameObject;
		_tutorialTapToUseSpecialMove = transform.Find("Tutorial Tap to Use Special Move").gameObject;
	}

	void Update() {
		UpdateTutorialNotification();

		if(PanelBehaviour.userCanInput) {
			bool isDone = useCardTutorialState == UseCardTutorialState.Done;
			_panelUseCard._pauseButton.interactable = isDone;
			_panelUseCard._deckDetailButton.interactable = isDone;
			_panelUseCard._gameSpeedButton.interactable = isDone;
			_panelUseCard._rollDiceButton.interactable = useCardTutorialState == UseCardTutorialState.TapToRollDice || isDone;
		}
	}

	private void UpdateTutorialNotification() {
		_tutorialTapToRollDice.SetActive(PanelBehaviour.userCanInput && useCardTutorialState == UseCardTutorialState.TapToRollDice);
		_tutorialTapToUseCard.SetActive(PanelBehaviour.userCanInput && useCardTutorialState == UseCardTutorialState.TapToUseCard);
		_tutorialTapToUseSpecialMove.SetActive(PanelBehaviour.userCanInput && useCardTutorialState == UseCardTutorialState.TapToUseSpecialMove);
	}

	public void CheckCardDetailButton() {
		if(useCardTutorialState == UseCardTutorialState.TapToUseSpecialMove) {
			_panelUseCard._cardDetail_useCardButton.interactable = false;
		}
	}

	public void SetUseCardTutorialState(int state) {
		if(useCardTutorialState != UseCardTutorialState.Done) {
			useCardTutorialState = (UseCardTutorialState)state;
			UpdateTutorialNotification();
		}
	}
}
