﻿using UnityEngine;
using UnityEngine.UI;

public class PanelRollDiceBehaviour : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseRollDiceBehaviour _rollDice;

	Animator _animator;
	Animator _playerUIAnimator;
	Animator[] _diceAnimator;
	Text _diceText;
	RectTransform _rollDiceMeter;

	[TextArea(1, 2)][SerializeField] string defaultDiceText = "";
	[SerializeField][Range(0.0f, 5.0f)] float diceMeterSpeed = 3.0f;
	[SerializeField][Range(0, 100)] int randomChance = 20;
	[SerializeField][Range(0, 100)] int perfectChance = 5;
    [SerializeField] bool useRandomStart = false;
	[SerializeField][Range(0, 100)] int minStartChance = 40;
	[SerializeField] PhaseRollDiceBehaviour.RollDiceOutputType notForceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.None;
    [SerializeField] Text _perfectTextEffect;
	[SerializeField] Text _pairTextEffect;
	[SerializeField] Text _unluckyTextEffect;
	float startShakingTime = 0.0f;

	bool hasInput = false;
	bool isEnded = false;
	float diceMeter = 0;
	bool isDiceMeterPositive = true;
	int diceMeterStartChance = 0;

	Text textEffect;
	protected PhaseRollDiceBehaviour.RollDiceOutputType rollDiceOutputType;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_rollDice = Resources.FindObjectsOfTypeAll<PhaseRollDiceBehaviour>()[0];
		_animator = GetComponent<Animator>();
		_playerUIAnimator = transform.Find("Player UI").GetComponent<Animator>();
		_diceText = transform.Find("Dice Text").GetComponent<Text>();
		_rollDiceMeter = transform.Find("Roll Dice Meter").GetComponent<RectTransform>();

		_diceAnimator = new Animator[GameplayManagerBehaviour.NUMBER_OF_DICE];
		for(int i = 0; i < _diceAnimator.Length; i++) {
			_diceAnimator[i] = transform.Find("Dice " + (i + 1).ToString()).GetComponent<Animator>();
		}
	}

	void OnEnable() {
		hasInput = false;
		isEnded = false;
		diceMeter = 0;
		diceMeterStartChance = 0;
		rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.None;

		SetupPlayerUIAnimator();
	}

	void FixedUpdate() {
		HandleInput();
		HandleText();
		HandleDiceAnimator();
		HandleRollDiceMeter();
		CheckForExit();
	}

	private void SetupPlayerUIAnimator() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		_playerUIAnimator.SetInteger("characterType", currentPlayer.GetCharacterType());
	}

	private void HandleInput() {
		if(PanelBehaviour.userCanInput) {
			bool useTouch = Input.GetMouseButton(0);
			bool hasRollDice = _rollDice.GetHasRollDiced();

			if((useTouch)) {
				if(!hasInput) {
					hasInput = true;
				} else {
                    if(diceMeter + diceMeterSpeed >= 100 && isDiceMeterPositive) isDiceMeterPositive = false;
                    else if(diceMeter - diceMeterSpeed < 0 && !isDiceMeterPositive) isDiceMeterPositive = true;
                    diceMeter = diceMeter + ((isDiceMeterPositive) ? diceMeterSpeed : -diceMeterSpeed);
                }
			}

			if(hasInput && !useTouch && !hasRollDice) {
				EndInput();
			}
		}
	}

	private void HandleText() {
		_diceText.text = ((!hasInput && !_rollDice.GetHasRollDiced()) && PanelBehaviour.userCanInput) ? defaultDiceText : "";
	}

	private void HandleDiceAnimator() {
		_animator.SetBool("rollDice", _rollDice.GetHasRollDiced() || hasInput);
		_playerUIAnimator.SetBool("rollDice", _rollDice.GetHasRollDiced() || hasInput);

		for(int i = 0; i < _diceAnimator.Length; i++) {
			int output = _rollDice.GetRollDiceInput()[i];
			_diceAnimator[i].SetInteger("output", output);
		}
	}

	private void HandleRollDiceMeter() {
		if(!hasInput) {
			if(diceMeterStartChance == 0) {
				_rollDiceMeter.gameObject.SetActive(PanelBehaviour.userCanInput);
				float rollDiceMeterWidth = _rollDiceMeter.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
				diceMeterStartChance = (useRandomStart) ? Random.Range(minStartChance, 100 - (randomChance + perfectChance)) : minStartChance;

				RectTransform randomMeter = _rollDiceMeter.GetChild(0).Find("Random Meter").GetComponent<RectTransform>();
				randomMeter.anchoredPosition = new Vector2(diceMeterStartChance * (rollDiceMeterWidth / 100), randomMeter.anchoredPosition.y);
				randomMeter.sizeDelta = new Vector2((randomChance + perfectChance) * (rollDiceMeterWidth / 100), randomMeter.sizeDelta.y);

				RectTransform pairMeter = _rollDiceMeter.GetChild(0).Find("Pair Meter").GetComponent<RectTransform>();
                pairMeter.anchoredPosition = (_rollDice.GetPerfectRollDiceIsAvailable()) ? new Vector2((diceMeterStartChance + randomChance/2) * (rollDiceMeterWidth / 100), pairMeter.anchoredPosition.y) : Vector2.zero;
				pairMeter.sizeDelta = (_rollDice.GetPerfectRollDiceIsAvailable()) ? new Vector2(perfectChance * (rollDiceMeterWidth / 100), pairMeter.sizeDelta.y) : new Vector2(0.0f, pairMeter.sizeDelta.y);

                RectTransform perfectMeter = _rollDiceMeter.GetChild(0).Find("Perfect Meter").GetComponent<RectTransform>();
                perfectMeter.anchoredPosition = new Vector2((diceMeterStartChance + randomChance / 2 + perfectChance / 2) * (rollDiceMeterWidth / 100), perfectMeter.anchoredPosition.y);
                perfectMeter.gameObject.SetActive(_rollDice.GetPerfectRollDiceIsAvailable());
            }
		} if(!_rollDice.GetHasRollDiced()) {
			RectTransform arrow = _rollDiceMeter.GetChild(0).Find("Arrow").GetComponent<RectTransform>();
			float rollDiceMeterWidth = _rollDiceMeter.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
			arrow.anchoredPosition = new Vector2(diceMeter * (rollDiceMeterWidth / 100), arrow.anchoredPosition.y);
		} else if(!textEffect) {
			PhaseRollDiceBehaviour.RollDiceOutputType outputType = (PhaseRollDiceBehaviour.RollDiceOutputType)_rollDice.GetOutputType();
			if(outputType != PhaseRollDiceBehaviour.RollDiceOutputType.Random) {
				bool isPairOutput = outputType == PhaseRollDiceBehaviour.RollDiceOutputType.Perfect;
				textEffect = Instantiate((!isPairOutput) ? _unluckyTextEffect : (CheckForPerfectRollDice()) ? _perfectTextEffect : _pairTextEffect);
				textEffect.transform.SetParent(transform, false);
				textEffect.rectTransform.position = _playerUIAnimator.GetComponent<RectTransform>().GetChild(0).position;
				Destroy(textEffect.gameObject, GameplayManagerBehaviour.STANDARD_TIME * ((CheckForPerfectRollDice()) ? 4 : 2));
			}
		}
	}

    private bool CheckForPerfectRollDice()
    {
        for(int i=0; i<_rollDice.GetRollDiceInput().Length; i++)
        {
            if (_rollDice.GetRollDiceInput()[i] != GameplayManagerBehaviour.MAX_DICE) return false;
        }

        return true;
    }

	private void CheckForExit() {
		for(int i = 0; i < _diceAnimator.Length; i++) {
			AnimatorStateInfo state = _diceAnimator[i].GetCurrentAnimatorStateInfo(0);
			int output = _rollDice.GetRollDiceInput()[i];
			bool isName = state.IsName("dice_" + output);
			bool isPlaying = state.normalizedTime >= state.length;
			if(isName && !isPlaying && !isEnded) {
				isEnded = true;
				_rollDice.Invoke("RollDiceMovePhase", GameplayManagerBehaviour.STANDARD_TIME * ((CheckForPerfectRollDice()) ? 3 : 1));
			}
		}
	}

	private void EndInput() {
		GetComponent<AudioSource>().loop = false;
		bool perfect = _rollDice.GetPerfectRollDiceIsAvailable() && diceMeter >= diceMeterStartChance + randomChance/2 && diceMeter < diceMeterStartChance + randomChance/2 + perfectChance;
        bool random = !perfect && diceMeter >= diceMeterStartChance && diceMeter < diceMeterStartChance + randomChance + perfectChance;
        rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Unlucky;
		if(random) rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Random;
		if(perfect) rollDiceOutputType = PhaseRollDiceBehaviour.RollDiceOutputType.Perfect;

		if(notForceOutputType == PhaseRollDiceBehaviour.RollDiceOutputType.None || rollDiceOutputType != notForceOutputType) {
			_rollDice.RandomizeDice(rollDiceOutputType, GenerateOutputTypeChance(rollDiceOutputType));
			_rollDice.SyncRollDiceInput(_rollDice.GetRollDiceInput(), (int)rollDiceOutputType);
        } else {
			_rollDice.RollDicePhase();
        }
	}

    private float GenerateOutputTypeChance(PhaseRollDiceBehaviour.RollDiceOutputType outputType)
    {
        int dmscRcPc = diceMeterStartChance + randomChance + perfectChance;
        switch (outputType)
        {
            case PhaseRollDiceBehaviour.RollDiceOutputType.Unlucky:
                if (diceMeter < diceMeterStartChance) return (diceMeter / diceMeterStartChance) * 0.5f;
                else return 0.5f + ((diceMeter - dmscRcPc) / (100 - dmscRcPc));
            case PhaseRollDiceBehaviour.RollDiceOutputType.Random:
                if (_rollDice.GetPerfectRollDiceIsAvailable())
                {
                    if (diceMeter < diceMeterStartChance + (1.0f * randomChance / 2)) return ((diceMeter - diceMeterStartChance) / (1.0f * randomChance / 2)) * 0.5f;
                    else return 0.5f + ((diceMeter - (diceMeterStartChance + randomChance / 2 + perfectChance)) / (1.0f * randomChance / 2) * 0.5f);
                }
                else return (diceMeter - diceMeterStartChance) / (randomChance + perfectChance);
            case PhaseRollDiceBehaviour.RollDiceOutputType.Perfect:
                return (diceMeter - (diceMeterStartChance + (1.0f * randomChance / 2))) / perfectChance;
            default:
                return 0.0f;
        }
    }

	public void PlaySound(AudioClip sfx) {
		GetComponent<AudioSource>().loop = true;
		GetComponent<AudioSource>().clip = sfx;
		GetComponent<AudioSource>().Play();
	}

	public PhaseRollDiceBehaviour.RollDiceOutputType GetRollDiceOutputType() { return rollDiceOutputType; }
	public void SetNotForceOutputType(PhaseRollDiceBehaviour.RollDiceOutputType newNotForceOutputType) { notForceOutputType = newNotForceOutputType; }
	public void SetPerfectChance(int newPerfectChance) { perfectChance = newPerfectChance; }
	public void SetRandomChance(int newRandomChance) { randomChance = newRandomChance; }
}
