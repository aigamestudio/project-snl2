﻿using UnityEngine;
using System.Collections.Generic;

public class PanelSelectTargetAIController : MonoBehaviour {
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardSelectTargetBehaviour _useCardSelectTarget;
	MapBehaviour _map;

	bool useAI = false;

	[SerializeField] bool isFirstPosTarget = true;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardSelectTarget = Resources.FindObjectsOfTypeAll<PhaseUseCardSelectTargetBehaviour>()[0];
		_map = GameObject.FindObjectOfType<MapBehaviour>();
	}

	void OnEnable() {
		useAI = GameplayManagerBehaviour.playerIsBot[_characterManager.GetCurrentPlayer()];
		if(useAI) {
			Invoke("HandleSelectTarget", GameplayManagerBehaviour.STANDARD_TIME);
		}
	}

	private void HandleSelectTarget() {
		CharacterBehaviour currentAI = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		if(isFirstPosTarget) {
			int playerId = currentAI.playerId;
			for(int i = _map.GetMapTileCount() - 1; i >= 0; i--) {
				if(playerId == currentAI.playerId) {
					MapTileBehaviour tile = _map.GetMapTile(i);
					List<CharacterBehaviour> listCharacterInTile = _characterManager.GetListCharactersInTile(tile, null);
					for(int j = 0; j < listCharacterInTile.Count; j++) {
						if(listCharacterInTile[j].playerId != currentAI.playerId) {
							playerId = listCharacterInTile[j].playerId;
							_useCardSelectTarget.UseCardActionPhase(playerId);
							break;
						}
					}
				}
			}
		} else {
			int randomId = currentAI.playerId;
			do {
				randomId = Random.Range(0, GameplayManagerBehaviour.MAX_PLAYER);
			} while(randomId == currentAI.playerId);

			_useCardSelectTarget.UseCardActionPhase(randomId);
		}
	}
}
