﻿using UnityEngine;

public class PanelBattleAIController : MonoBehaviour {
	PhaseCheckBattleBattleBehaviour _checkBattleBattle;
	PanelBattleBehaviour _panelBattle;

	bool challengerUseAI = false;
	bool challengedUseAI = false;

	[SerializeField][Range(0, 100)]	int battleAcc = 90;
	[SerializeField][Range(0, 100)]	int battleAccBoss = 100;
	[SerializeField][Range(0.0f, 10.0f)] float battleSpeedMin = 1.55f;
	[SerializeField][Range(0.0f, 10.0f)] float battleSpeedMax = 0.65f;
	[SerializeField][Range(0.0f, 10.0f)] float battleSpeedMinBoss = 1.35f;
	[SerializeField][Range(0.0f, 10.0f)] float battleSpeedMaxBoss = 0.7f;

	int challengerAcc = 0;
	float challengerTime = 0.0f;
	int challengedAcc = 0;
	float challengedTime = 0.0f;

	void Awake() {
		_checkBattleBattle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBattleBehaviour>()[0];
		_panelBattle = GetComponent<PanelBattleBehaviour>();
	}

	void OnEnable() {
		challengerUseAI = GameplayManagerBehaviour.playerIsBot[_checkBattleBattle._challenger.playerId];
		challengedUseAI = _checkBattleBattle.GetIsBossBattle() || GameplayManagerBehaviour.playerIsBot[_checkBattleBattle._challenged.playerId];
	}

	void Update() {
		HandleChallengerAI();
		HandleChallengedAI();
	}

	private void HandleChallengerAI() {
		if(challengerUseAI) {
			switch(_checkBattleBattle.GetBattlePhase()) {
				case PhaseCheckBattleBattleBehaviour.BattlePhase.SetupQuestion:
					challengerAcc = battleAcc;
					challengerTime = Random.Range(battleSpeedMax, battleSpeedMin);
					Invoke("ChallengerAnswer", challengerTime);
					break;
				case PhaseCheckBattleBattleBehaviour.BattlePhase.ProcessAnswer:
					CancelInvoke("ChallengerAnswer");
					break;
			}
		}
	}

	private void ChallengerAnswer() {
		int trueChoice = _checkBattleBattle.GetTrueChoice();
		int randomOption = Random.Range(0, 100);
		if(randomOption <= challengerAcc) _checkBattleBattle.challengerChoice = trueChoice;
		else _checkBattleBattle.challengerChoice = 1 - trueChoice;
	}

	private void HandleChallengedAI() {
		if(challengedUseAI) {
			switch(_checkBattleBattle.GetBattlePhase()) {
				case PhaseCheckBattleBattleBehaviour.BattlePhase.SetupQuestion:
					if(_checkBattleBattle.GetIsBossBattle()) {
						challengedAcc = battleAccBoss;
						challengedTime = Random.Range(battleSpeedMaxBoss, battleSpeedMinBoss);
					}
					else {
						challengedAcc = battleAcc;
						challengedTime = Random.Range(battleSpeedMax, battleSpeedMin);
					}
					Invoke("ChallengedAnswer", challengedTime);
					break;
				case PhaseCheckBattleBattleBehaviour.BattlePhase.ProcessAnswer:
					CancelInvoke("ChallengedAnswer");
					break;
			}
		}
	}

	private void ChallengedAnswer() {
		int trueChoice = _checkBattleBattle.GetTrueChoice();
		int randomOption = Random.Range(0, 100);
		if(randomOption <= challengedAcc) _checkBattleBattle.challengedChoice = trueChoice;
		else _checkBattleBattle.challengedChoice = 1 - trueChoice;
	}

	public bool CheckBattleBothUsingAI() { return challengerUseAI && challengedUseAI; }
}