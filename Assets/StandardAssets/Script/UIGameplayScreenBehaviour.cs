﻿using UnityEngine;
using UnityEngine.UI;

public class UIGameplayScreenBehaviour : MonoBehaviour {
	public enum GameplayPanel {
		NoPanel, SelectTargetPanel,
		UseCardPanel, RollDicePanel,
		BattlePanel, ChooseCardPanel,
		WinPanel
	} 

	GameplayPanel _currentPanel = GameplayPanel.NoPanel;

	PanelSelectTargetBehaviour _selectTarget;
	PanelUseCardBehaviour _useCard;
	PanelRollDiceBehaviour _rollDice;
	PanelBattleBehaviour _battle;
	PanelChooseCardBehaviour _chooseCard;
	PanelWinBehaviour _win;

	PanelPlayerHUDBehaviour _playerHUD;

	[SerializeField] GameObject _panelShowCardPrefab;
	[SerializeField] float showCardDelay = 1.0f;
	[SerializeField] float showCardExclusiveDelay = 5.0f;

	[SerializeField] GameObject _panelNotificationPrefab;
	[SerializeField] float notificationDelay = 1.0f;

	[SerializeField] GameObject _effectTextPrefab;
	[SerializeField] float showTextDelay = 1.0f;

	[SerializeField] GameObject _rushPanelPrefab;
	[SerializeField] string holdToRushStr = "Hold to Rush";
	[SerializeField] string rushStr = "Rushing ...";
    GameObject rushPanel;

	void Awake () {
		_selectTarget = transform.GetComponentInChildren<PanelSelectTargetBehaviour>(true);
		_useCard = transform.GetComponentInChildren<PanelUseCardBehaviour>(true);
		_rollDice = transform.GetComponentInChildren<PanelRollDiceBehaviour>(true);
		_battle = transform.GetComponentInChildren<PanelBattleBehaviour>(true);
		_chooseCard = transform.GetComponentInChildren<PanelChooseCardBehaviour>(true);
		_win = transform.GetComponentInChildren<PanelWinBehaviour>(true);

		_playerHUD = transform.GetComponentInChildren<PanelPlayerHUDBehaviour>(true);
	}

	private void DiactivatePanel() {
		_selectTarget.gameObject.SetActive(false);
		_useCard.gameObject.SetActive(false);
		_rollDice.gameObject.SetActive(false);
		_battle.gameObject.SetActive(false);
		_chooseCard.gameObject.SetActive(false);
		_win.gameObject.SetActive(false);
	}

	private void ActivatePanel(GameplayPanel panel) {
		_currentPanel = panel;

		switch(panel) {
			case GameplayPanel.SelectTargetPanel: _selectTarget.gameObject.SetActive(true); break;
			case GameplayPanel.UseCardPanel: _useCard.gameObject.SetActive(true); break;
			case GameplayPanel.RollDicePanel: _rollDice.gameObject.SetActive(true); break;
			case GameplayPanel.BattlePanel: _battle.gameObject.SetActive(true); break;
			case GameplayPanel.ChooseCardPanel: _chooseCard.gameObject.SetActive(true); break;
			case GameplayPanel.WinPanel: _win.gameObject.SetActive(true); break;
		}
	}

	public void ChangePanel(GameplayPanel panel) {
		DiactivatePanel();
		ActivatePanel(panel);
	}

	public void ShowCard(CardBehaviour cardUsed, CharacterBehaviour currentCharacter, bool isVisible, bool isUseSpecial, string titleStr) {
		GameObject cardObject = Instantiate(_panelShowCardPrefab);
		cardObject.transform.SetParent(transform, false);

		cardObject.GetComponent<Animator>().SetBool("isUseSpecial", isUseSpecial);
		cardObject.transform.Find("Card UI").GetComponent<Animator>().SetInteger("cardType", cardUsed.GetCardType());
		cardObject.transform.Find("Card UI").GetComponent<Animator>().SetBool("isVisible", isVisible);

		cardObject.transform.Find("Title Text").GetComponent<Text>().text = titleStr;

		if(isUseSpecial && cardUsed.GetExclusiveCharacter() && cardUsed.GetExclusiveCharacter().GetCharacterType() == currentCharacter.GetCharacterType()) {
			Animator playerUIAnimator = cardObject.transform.Find("Player UI").GetComponent<Animator>();
			playerUIAnimator.SetInteger("characterType", currentCharacter.GetCharacterType());
			playerUIAnimator.SetTrigger("win");
			cardObject.GetComponent<Animator>().SetBool("isExclusive", true);
			Destroy(cardObject, showCardExclusiveDelay);
		} else {
			Destroy(cardObject, showCardDelay);
		}
	}

	public void ShowNotification(string title, string desc) {
		GameObject notificationObject = Instantiate(_panelNotificationPrefab);
		notificationObject.transform.SetParent(transform, false);

		Text titleText = notificationObject.transform.GetChild(0).Find("Title Text").GetComponent<Text>();
		Text descText = notificationObject.transform.GetChild(0).Find("Desc Text").GetComponent<Text>();
		titleText.text = title;
		descText.text = desc;

		Destroy(notificationObject, notificationDelay);
	}

	public void ShowText(string textStr) {
		GameObject textObject = Instantiate(_effectTextPrefab);
		textObject.transform.SetParent(transform, false);

		textObject.GetComponent<Text>().text = textStr;
		Destroy(textObject, showTextDelay);
	}

	public void ShowRushPanel(bool canRush) {
		if(canRush) {
			if(!rushPanel) {
				rushPanel = Instantiate(_rushPanelPrefab) as GameObject;
				rushPanel.transform.SetParent(transform, false);
			} else {
				Text rushText = rushPanel.transform.Find("BackPlate").GetChild(0).GetComponent<Text>();
				rushText.text = (GameplaySpeedController.GetIsRushing()) ? rushStr : holdToRushStr;
			}
		} else {
			if(rushPanel) {
				Destroy(rushPanel.gameObject);
			}
		}
	}

	public float GetShowCardDelay() { return showCardDelay; }
	public float GetShowCardExclusiveDelay() { return showCardExclusiveDelay; }
	public float GetNotificationDelay() { return notificationDelay; }
	public float GetShowTextDelay() { return showTextDelay; }
}
