﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterBehaviour : MonoBehaviour {
	public enum AnimationType {
		Idle, Run, Ladder, Snake, Win, Lose, TakePoint, TakeCard, Overluck,
        UseCard, Skipped, Heal, Stolen, MultiplySpeed, Hit, CardLoss		
	}

	Animator _animator;
	Rigidbody2D _rb2d;

	Transform _sprites;
	Transform _cards;
	Transform _states;
	Transform _playerCanvas;

	[SerializeField][Range(0, GameplayManagerBehaviour.AVAILABLE_CHARACTER)] int characterType = 0;

	[SerializeField] string characterName = "";
	[SerializeField][TextArea(1, 5)] string storyDesc = "";
	string specialDesc = "";

	[SerializeField] CharacterLevel[] characterLevels = new CharacterLevel[GameplayManagerBehaviour.MAX_CHARACTER_LEVEL];
	int currentLevel = 0;

	[SerializeField] int initialPoint = 0;
	[HideInInspector] public int currentPoint = 0;
	
	int maxHP = 0;
	[HideInInspector] public int currentHP = 0;

	[HideInInspector] public int position = 0;
	[HideInInspector] public int playerId = -1;

	[SerializeField] Text _playerTextEffectPrefab;
	[SerializeField] ParticleSystem _cardInParticleEffectPrefab;
	[SerializeField] ParticleSystem _cardOutParticleEffectPrefab;
	[SerializeField] ParticleSystem _marbleInParticleEffectPrefab;
	[SerializeField] ParticleSystem _marbleOutParticleEffectPrefab;
	[SerializeField] ParticleSystem _staminaInParticleEffectPrefab;
	[SerializeField] ParticleSystem _staminaOutParticleEffectPrefab;
    [SerializeField] ParticleSystem _skipInParticleEffectPrefab;
    [SerializeField] ParticleSystem _diceInParticleEffectPrefab;

    [System.Serializable]
	public class CharacterLevel : System.Object {
		[SerializeField] string levelItemName = "";
		[SerializeField] int mapIdRequirement = 0;
		[SerializeField] int coinRequirement = 0;
		[SerializeField] int maxHP = 1;
		[SerializeField][TextArea(1,3)] string specialDesc = "";
		public string GetLevelItemName() { return levelItemName; }
		public int GetMapIdRequirement() { return mapIdRequirement; }
		public int GetCoinRequirement() { return coinRequirement; }
		public int GetMaxHP() { return maxHP; }
		public string GetSpecialDesc() { return specialDesc; }
	}

	void Awake () {
		_animator = GetComponent<Animator>();
		_rb2d = GetComponent<Rigidbody2D>();
		_sprites = transform.Find("Sprites");
		_cards = transform.Find("Cards");
		_states = transform.Find("States");
		_playerCanvas = transform.Find("Player Canvas");
	}

	void OnEnable() {
		currentPoint = initialPoint;
		currentHP = maxHP;
		position = GameplayManagerBehaviour.START_POSITION;
	}

	private void HandleFacingDirection(Vector2 targetSlotPos) {
		Vector2 facingRight = new Vector2(1.0f, 1.0f);
		Vector2 facingLeft = new Vector2(-1.0f, 1.0f);
		Vector2 deltaPosition = targetSlotPos - (Vector2)transform.position;
		if(deltaPosition.x < 0.0f) _sprites.transform.localScale = facingLeft;
		else if(deltaPosition.x > 0.0f) _sprites.transform.localScale = facingRight;
	}

	public void MoveToPosition(Vector2 targetSlotPos, float moveSpeed) {
		Vector2 pos = transform.position;
		Vector2 velocity = Vector2.MoveTowards(pos, targetSlotPos, moveSpeed * Time.deltaTime);
		_rb2d.MovePosition(velocity);
		HandleFacingDirection(targetSlotPos);
	}

	public bool CheckArrivedInPosition(Vector2 targetSlotPos, float radius) {
		return Vector2.Distance(transform.position, targetSlotPos) < radius;
	}

	// belom fix, mau overwrite state?
	public void AddState(StateBehaviour state) {
		state.transform.SetParent(_states);
	}

	public T GetState<T>() {
		foreach(Transform children in _states) {
			T state = children.GetComponent<T>();
			if(state != null) {
				return state;
			}
		}

		return default(T);
	}

	public void DecreaseStateTurnRemain() {
		foreach(Transform children in _states) {
			children.GetComponent<StateBehaviour>().turnRemain--;
		}
	}

	public void SetAnimation(AnimationType animationType) {
		switch(animationType) {
			case AnimationType.Run:
				_animator.SetTrigger("run");
				break;
			case AnimationType.Ladder:
				_animator.SetTrigger("ladder");
				break;
			case AnimationType.Snake:
				_animator.SetTrigger("snake");
				break;
			case AnimationType.Win:
				ShowTextEffect("Win!!!");
				_animator.SetTrigger("win");
				break;
			case AnimationType.Lose:
				ShowTextEffect("Lose ...");
				_animator.SetTrigger("lose");
				break;
			case AnimationType.TakePoint:
				//ShowTextEffect("I've got Marble!!");
				ShowParticleEffect(_marbleInParticleEffectPrefab);
				_animator.SetTrigger("positive");
				break;
			case AnimationType.TakeCard:
				_animator.SetTrigger("positive");
				ShowParticleEffect(_cardInParticleEffectPrefab);
				break;
			case AnimationType.UseCard:
				_animator.SetTrigger("positive");
				break;
			case AnimationType.Skipped:
                //ShowTextEffect("Skipped ...");
                ShowParticleEffect(_skipInParticleEffectPrefab);
                _animator.SetTrigger("negative");
				break;
			case AnimationType.Heal:
				//ShowTextEffect("So Refreshing!");
				ShowParticleEffect(_staminaInParticleEffectPrefab);
				_animator.SetTrigger("positive");
				break;
			case AnimationType.Stolen:
				//ShowTextEffect("Lost Marbles!!");
				ShowParticleEffect(_marbleOutParticleEffectPrefab);
				_animator.SetTrigger("negative");
				break;
			case AnimationType.MultiplySpeed:
                //ShowTextEffect("RUNNN!!!!");
                ShowParticleEffect(_diceInParticleEffectPrefab);
                _animator.SetTrigger("positive");
				break;
			case AnimationType.Hit:
				ShowParticleEffect(_staminaOutParticleEffectPrefab);
				_animator.SetTrigger("negative");
				break;
			case AnimationType.CardLoss:
				//ShowTextEffect("Lost Chances ...");
				ShowParticleEffect(_cardOutParticleEffectPrefab);
				_animator.SetTrigger("negative");
				break;
			case AnimationType.Overluck:
				//ShowTextEffect("Overluck T_T");
				_animator.SetTrigger("negative");
				break;
			default: _animator.SetTrigger("idle"); break;
		}
	}

	private void ShowTextEffect(string textString) {
		Text text = Instantiate(_playerTextEffectPrefab);
		text.transform.SetParent(_playerCanvas, false);
		text.text = textString;
		Destroy(text.gameObject, GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void ShowParticleEffect(ParticleSystem particle) {
		ParticleSystem newParticle = Instantiate(particle);
		newParticle.transform.SetParent(_sprites, false);
		Destroy(newParticle.gameObject, GameplayManagerBehaviour.STANDARD_TIME);
	}

	public void ChangeCharacterLevel(int level) {
		currentLevel = level;
		maxHP = characterLevels[currentLevel].GetMaxHP();
		specialDesc = characterLevels[currentLevel].GetSpecialDesc();
		currentHP = maxHP;
	}

    public void SetPlayerNameText(string name)
    {
        _playerCanvas.GetChild(0).GetComponent<Text>().text = name;
    }

	public Transform GetCards() { return _cards; }
	public int GetCharacterType() { return characterType; }
	public string GetCharacterName() { return characterName; }
	public int GetCurrentLevel() { return currentLevel; }
	public string GetStoryDesc() { return storyDesc; }
	public string GetSpecialDesc() { return specialDesc; }
	public int GetMaxHP() { return maxHP; }
	public void PlaySound(AudioClip sfx) { GetComponent<AudioSource>().PlayOneShot(sfx); }
	public CharacterLevel[] GetCharacterLevels() { return characterLevels; }
}
