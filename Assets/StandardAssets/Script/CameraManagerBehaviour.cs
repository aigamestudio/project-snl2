﻿using UnityEngine;
using System.Collections.Generic;

public class CameraManagerBehaviour : MonoBehaviour {
	public enum CameraType {
		AllPlayer, CurrentPlayer, FreeRoam, UseCard, CheckKO, Battle, CheckTeleportMove
	}
	
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCardAction;
	PhaseCheckKOBehaviour _checkKO;
	PhaseCheckBattleBattleBehaviour _checkBattle_Battle;
	PhaseCheckTeleportMoveBehaviour _checkTeleportMove;

	[SerializeField] float cameraMoveSpeed = 3.0f;
	[SerializeField] float cameraMinSize = 4.0f;
	[SerializeField] float cameraMaxSize = 20.0f;
	[SerializeField] float cameraMinX = -10.0f;
	[SerializeField] float cameraMaxX = 10.0f;
	[SerializeField] float cameraMinY = -5.0f;
	[SerializeField] float cameraMaxY = 5.0f;

	bool canFreeRoam = true;

	CameraType _cameraType = CameraType.AllPlayer;
    TouchCamera touchCamera;

	void Awake () {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCardAction = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_checkKO = Resources.FindObjectsOfTypeAll<PhaseCheckKOBehaviour>()[0];
		_checkBattle_Battle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBattleBehaviour>()[0];
		_checkTeleportMove = Resources.FindObjectsOfTypeAll<PhaseCheckTeleportMoveBehaviour>()[0];

        touchCamera = GetComponent<TouchCamera>();
	}

	void Update () {
		switch(_cameraType) {
			case CameraType.CurrentPlayer: HandleCameraTypeCurrentPlayer(); break;
			case CameraType.FreeRoam: HandleCameraTypeFreeRoam(); break;
			case CameraType.UseCard: HandleCameraTypeUseCard(); break;
			case CameraType.CheckKO: HandleCameraCheckKO(); break;
			case CameraType.Battle: HandleCameraBattle(); break;
			case CameraType.CheckTeleportMove: HandleCameraCheckTeleportMove(); break;
			default: HandleCameraTypeAllPlayers(); break;
		}
	}

	private void HandleCameraTypeAllPlayers() {
		Bounds bounds = new Bounds(_characterManager.GetCharacter(0).transform.position, Vector2.zero);
		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			Vector2 characterPosition = _characterManager.GetCharacter(i).transform.position;
			bounds.Encapsulate(characterPosition);
		}

		HandleCameraBoundsCenterView(bounds);
		HandleCameraBoundsSize(bounds);
	}

	private void HandleCameraTypeCurrentPlayer() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		Bounds bounds = new Bounds(currentPlayer.transform.position, Vector2.zero);

		HandleCameraBoundsCenterView(bounds);
		HandleCameraBoundsSize(bounds);
	}

	private void HandleCameraTypeFreeRoam() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		if(!GameplayManagerBehaviour.playerIsBot[currentPlayer.playerId] && canFreeRoam) {
			touchCamera.UpdateCamera();

			float xPos = Mathf.Clamp(transform.position.x, cameraMinX, cameraMaxX);
			float yPos = Mathf.Clamp(transform.position.y, cameraMinY, cameraMaxY);
			Camera.main.transform.position = new Vector3(xPos, yPos, -10.0f);
			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, cameraMinSize, cameraMaxSize);
		} else {
			touchCamera.ResetTouch();
		}
	}

	private void HandleCameraTypeUseCard() { 
		List<CharacterBehaviour> listTarget = _useCardAction.listTarget;
		Bounds bounds = new Bounds(listTarget[0].transform.position, Vector2.zero);
		for(int i=0; i<listTarget.Count; i++) {
			Vector2 characterPosition = listTarget[i].transform.position;
			bounds.Encapsulate(characterPosition);
		}

		HandleCameraBoundsCenterView(bounds);
		HandleCameraBoundsSize(bounds);
	}

	private void HandleCameraCheckKO() {
		List<CharacterBehaviour> listKOCharacter = _checkKO.GetListKOCharacter();
		Bounds bounds = new Bounds(listKOCharacter[0].transform.position, Vector2.zero);
		for(int i=0; i<listKOCharacter.Count; i++) {
			Vector2 characterPosition = listKOCharacter[i].transform.position;
			bounds.Encapsulate(characterPosition);
		}

		HandleCameraBoundsCenterView(bounds);
		HandleCameraBoundsSize(bounds);
	}

	private void HandleCameraBattle() {
		CharacterBehaviour challenger = _checkBattle_Battle._challenger;
		CharacterBehaviour challenged = _checkBattle_Battle._challenged;
		Bounds bounds = new Bounds(challenger.transform.position, Vector2.zero);
		bounds.Encapsulate(challenged.transform.position);

		HandleCameraBoundsCenterView(bounds);
		HandleCameraBoundsSize(bounds);
	}

	private void HandleCameraCheckTeleportMove() {
		List<CharacterBehaviour> listTarget = _checkTeleportMove.listTarget;
		Bounds bounds = new Bounds(listTarget[0].transform.position, Vector2.zero);
		for(int i = 0; i < listTarget.Count; i++) {
			Vector2 characterPosition = listTarget[i].transform.position;
			bounds.Encapsulate(characterPosition);
		}

		HandleCameraBoundsCenterView(bounds);
		HandleCameraBoundsSize(bounds);
	}

	private void HandleCameraBoundsCenterView(Bounds cameraBounds) {
		Vector3 newPos = new Vector3(cameraBounds.center.x, cameraBounds.center.y, transform.position.z);
		transform.position = Vector3.MoveTowards(transform.position, newPos, cameraMoveSpeed * Time.deltaTime);
	}

	private void HandleCameraBoundsSize(Bounds cameraBounds) {
		float cameraSize = Camera.main.orthographicSize;
		float targetSize = Mathf.Max(Mathf.Max(cameraBounds.size.x, cameraBounds.size.y), cameraMinSize);
        Camera.main.orthographicSize = Mathf.MoveTowards(cameraSize, targetSize, cameraMoveSpeed * Time.deltaTime);
	}

	public void SetCameraType(CameraType cameraType) {	_cameraType = cameraType; }
	public void SetCanFreeRoam(bool cond) { canFreeRoam = cond; }
}
