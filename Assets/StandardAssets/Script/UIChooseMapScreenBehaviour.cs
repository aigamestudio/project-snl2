﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIChooseMapScreenBehaviour : MonoBehaviour {
	Animator _animator;

	[SerializeField] GameObject _transition;
	[SerializeField] float transitionTime = 1.0f;

	SwipeToChangeObjectBehaviour _swipeToChangeObject;
	Text _stageNameText;
	Button _nextButton;

	[SerializeField] string lockedStr = "Terkunci";
	[SerializeField] string lockedDescStr;

	void Awake () {
		_animator = GetComponent<Animator>();
		_stageNameText = transform.Find("Stage Name").Find("Text").GetComponent<Text>();
		_swipeToChangeObject = GameObject.FindObjectOfType<SwipeToChangeObjectBehaviour>();
		_nextButton = transform.Find("Next").GetComponent<Button>();
	}
	
	void Update () {
		bool currentMapIsUnlocked = ProfileData.map_unlocked[_swipeToChangeObject.GetCurrentSlide()];
		int currentSlide = _swipeToChangeObject.GetCurrentSlide();
		HandleMapLoadingVariable(currentSlide);
		_animator.SetInteger("currentSlide", currentSlide);
		_animator.SetInteger("mapWinDifficulty", ProfileData.map_win_difficulty[currentSlide]);

		_stageNameText.text = (currentMapIsUnlocked) ? GetStageName(currentSlide) : lockedStr;
		_stageNameText.transform.GetChild(0).GetComponent<Text>().text = (currentMapIsUnlocked) ? "" : lockedDescStr;
		_nextButton.interactable = currentMapIsUnlocked;

		if(Input.GetKey(KeyCode.Escape)) {
			BackButton();
		}
	}

	private string GetStageName(int mapId) {
		return _swipeToChangeObject.transform.GetChild(mapId).GetComponent<MapBehaviour>().GetMapName();
	}

	private bool GetStageHasStoryline(int mapId) {
		return _swipeToChangeObject.transform.GetChild(mapId).GetComponent<MapBehaviour>().GetMapHasStoryline();
	}
	
	private void HandleMapLoadingVariable(int currentSlide) {
		LoadingScreenBehaviour.changeScene = GetStageName(currentSlide);
		LoadingScreenBehaviour.SetInitialGameSpeed(2);
		LoadingScreenBehaviour.skipStoryline = !(GameplayManagerBehaviour.isCampaign && GetStageHasStoryline(currentSlide));
	}
    
    public void NextButton()
    {
        GameObject transition = Instantiate(_transition);
        transition.transform.SetParent(transform, false);
        StartCoroutine(ChangeScene("choosecharacter"));
    }

    public void BackButton()
    {
        SceneManager.LoadScene("menuscreen");
    }

    IEnumerator ChangeScene(string newScene)
    {
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(newScene);
    }
}
