﻿using UnityEngine;
using UnityEngine.UI;

public class UINotificationBehaviour : MonoBehaviour {
	public static UINotificationBehaviour instance;

	public static UINotificationBehaviour Instance { get { return instance; } }

	[SerializeField] string cannotConnectToServerErrorStr = "Couldn't connect to the internet. Make sure you're already connected to internet and try again.";
	[SerializeField] string noRandomRoomErrorStr = "No available room right now. Create your own room and wait for other player.";
	[SerializeField] string roomNotFoundErrorStr = "Couldn't find room. Try to correct your room code input value.";

	GameObject _errorNotification;
	GameObject _warningNotification;
	GameObject _pingNotification;
	GameObject _waitForOtherPlayerNotification;

	void Awake() {
		_errorNotification = transform.Find("Error Notification").gameObject;
		_warningNotification = transform.Find("Warning Notification").gameObject;
		_pingNotification = transform.Find("Ping Notification").gameObject;
		_waitForOtherPlayerNotification = transform.Find("Wait For Other Player Notification").gameObject;

		SetupSingleton();
	}

	private void SetupSingleton() {
		if(instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		}
		else {
			instance = this;
		}

		DontDestroyOnLoad(this.gameObject);
	}

	public void HandlePlayerCurrentPhase(GameplayManagerBehaviour.GameplayPhase[] playerCurrentPhase) {
		for(int i=1; i<GameplayManagerBehaviour.MAX_PLAYER; i++) {
			if(playerCurrentPhase[i-1] != playerCurrentPhase[i]) {
				_waitForOtherPlayerNotification.SetActive(true);
				return;
			}
		}

		_waitForOtherPlayerNotification.SetActive(false);
	}

	public void ShowErrorNotification(string errorText) {
		HideNotification();
		_errorNotification.transform.Find("BackPlate").GetChild(0).GetComponent<Text>().text = errorText;
		_errorNotification.SetActive(true);
	}

	public void ShowWarningNotification(string warningText) {
		HideNotification();
		_warningNotification.transform.Find("BackPlate").GetChild(0).GetComponent<Text>().text = warningText;
		_warningNotification.SetActive(true);
	}

	public void HideNotification() {
		_errorNotification.SetActive(false);
		_warningNotification.SetActive(false);
	}
}
