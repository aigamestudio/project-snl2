﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using UnityEngine.Advertisements;
using System.Collections;

public class UIResultScreenBehaviour : MonoBehaviour {
	ResultScreenBehaviour _resultScreen;

	Animator _animator;
	GameObject _playerUI;
	Text _titleText;
	Text _rankText;
	Text _coinText;
	Button _nextStageButton;

	Button _upgradeButton;
	Button _upgradePanelButton;
	//Button _upgradePanelWatchAdsButton;
	Text _upgradePanelCurrentMoneyText;
	Text _upgradePanelNextLevelText;
	Text _upgradePanelPrevLevelText;

	[SerializeField] string winStr = "You Win!";
	[SerializeField] string loseStr = "You Lose :(";
	[SerializeField] string[] orderStr = new string[GameplayManagerBehaviour.MAX_PLAYER];
	[SerializeField] int speedCountPerFrame = 100;

	[SerializeField] string upgradeNotAvailableStr = "Item is not available";

	int currentRewardCoin = 0;
	int currentPerfectRollDiceRewardCoin = 0;
	int currentTotalCoin = 0;

	bool isPaidAudience = false;

	string nextScene = "menuscreen";

	void Awake () {
		_resultScreen = GameObject.FindObjectOfType<ResultScreenBehaviour>();
		_animator = GetComponent<Animator>();
		_playerUI = transform.Find("Player UI").gameObject;
		_titleText = transform.Find("Title Text").GetComponent<Text>();
		_rankText = transform.Find("Rank Text").GetComponent<Text>();
		_coinText = transform.Find("Coin Text").GetComponent<Text>();
		_nextStageButton = transform.Find("Next Stage Button").GetComponent<Button>();

		_upgradeButton = transform.Find("Upgrade Button").GetComponent<Button>();
		_upgradePanelButton = transform.Find("Upgrade Panel").Find("BackPlate").Find("OK Button").GetComponent<Button>();
		//_upgradePanelWatchAdsButton = transform.Find("Upgrade Panel").Find("BackPlate").Find("Watch Ads Button").GetComponent<Button>();
		_upgradePanelCurrentMoneyText = transform.Find("Upgrade Panel").Find("BackPlate").Find("Current Money Text").GetChild(0).GetComponent<Text>();
		_upgradePanelNextLevelText = transform.Find("Upgrade Panel").Find("BackPlate").Find("Next Level Text").GetComponent<Text>();
		_upgradePanelPrevLevelText = transform.Find("Upgrade Panel").Find("BackPlate").Find("Previous Level Text").GetComponent<Text>();
	}

	void Start() {
		_nextStageButton.transform.GetChild(0).GetComponent<Text>().text = (ResultScreenBehaviour.playerOrder == 0) ? "Next" : "Retry";

		HandlePlayerUI();
		HandleRankText();
		Invoke("HandleNotification", GameplayManagerBehaviour.STANDARD_TIME/2.0f);
		StartCoroutine(RewardCoinCountUp(_resultScreen.GetRewardCoin()));
		StartCoroutine(PerfectRollDiceCountUp(_resultScreen.GetPerfectRollDiceRewardCoin()));
		StartCoroutine(TotalCoinCountUp(_resultScreen.GetTotalCoin()));
	}

	void Update() {
		HandleUpgradeCharacterLevel();
		_coinText.text = "Rp " + currentRewardCoin + 
						",-\nRp " + currentPerfectRollDiceRewardCoin + 
						",-\n\nRp " + currentTotalCoin + ",-";
	}

	private void HandlePlayerUI() {
		int characterType = GameplayManagerBehaviour.initialPrefab[GameplayManagerBehaviour.playerIdTurn].GetCharacterType();
		_playerUI.GetComponent<Animator>().SetInteger("characterType", characterType);
		_playerUI.GetComponent<Animator>().SetTrigger((ResultScreenBehaviour.playerOrder == 0) ? "win" : "lose");
	}

	private void HandleRankText() {
		_titleText.text = (ResultScreenBehaviour.playerOrder == 0) ? winStr : loseStr;
		_rankText.text = orderStr[ResultScreenBehaviour.playerOrder];
	}

	private void HandleUpgradeCharacterLevel() {
		CharacterBehaviour currentCharacter = GameplayManagerBehaviour.initialPrefab[GameplayManagerBehaviour.playerIdTurn];
		int currentLevelData = ProfileData.character_level[currentCharacter.GetCharacterType()];
		bool canUpgrade = currentLevelData + 1 < GameplayManagerBehaviour.MAX_CHARACTER_LEVEL;
		_upgradeButton.interactable = canUpgrade;
		if(canUpgrade) {
			_upgradePanelCurrentMoneyText.text = "Rp " + ProfileData.profile_coin + ",-";
			CharacterBehaviour.CharacterLevel currentLevel = currentCharacter.GetCharacterLevels()[currentLevelData];
			CharacterBehaviour.CharacterLevel nextLevel = currentCharacter.GetCharacterLevels()[currentLevelData + 1];

			int currentCharacterMaxHP = currentLevel.GetMaxHP();
			string currentCharacterSpecialDesc = currentLevel.GetSpecialDesc();
			_upgradePanelPrevLevelText.text = "From " + currentLevel.GetLevelItemName() + "\n\nStamina : " + currentCharacterMaxHP + "\n\nSpecial : \n" + currentCharacterSpecialDesc;

			int currentCharacterNextMaxHP = nextLevel.GetMaxHP();
			string currentCharacterNextSpecialDesc = nextLevel.GetSpecialDesc();
			_upgradePanelNextLevelText.text = "Upgrade to " + nextLevel.GetLevelItemName() + "\n\nStamina : " + currentCharacterNextMaxHP + "\n\nSpecial : \n" + currentCharacterNextSpecialDesc;

			int nextLevelMapIdRequirement = nextLevel.GetMapIdRequirement();
			bool isUpgradeAvailable = ProfileData.character_campaign_lastMapUnlocked[currentCharacter.GetCharacterType()] >= nextLevelMapIdRequirement;
			bool isCoinAvailable = ProfileData.profile_coin >= nextLevel.GetCoinRequirement();
			_upgradePanelButton.transform.GetChild(0).GetComponent<Text>().text = (isUpgradeAvailable) ? "<size='8'>Upgrade " + currentCharacter.GetCharacterName() + " for\n</size>Rp " + nextLevel.GetCoinRequirement() + ",-" : upgradeNotAvailableStr;
			_upgradePanelButton.interactable = isUpgradeAvailable && isCoinAvailable;

			//bool adsIsReady = Advertisement.IsReady();
            //_upgradePanelWatchAdsButton.interactable = adsIsReady;
			//_upgradePanelWatchAdsButton.transform.GetChild(0).GetComponent<Text>().text = (adsIsReady) ? "<size='8'>Watch Ads to Get\n</size> Rp 10000,-" : "Ads not ready :(";
		}
	}

	public void UpgradeButton() {
		CharacterBehaviour currentCharacter = GameplayManagerBehaviour.initialPrefab[GameplayManagerBehaviour.playerIdTurn];
		int currentLevel = ++ProfileData.character_level[currentCharacter.GetCharacterType()];
		ProfileData.profile_coin -= currentCharacter.GetCharacterLevels()[currentLevel].GetCoinRequirement();
		ProfileData.SaveCharacterData();
		ProfileData.SaveProfileData();
	}

	private void HandleNotification() {
		if(_resultScreen.GetNewCharacterLevel() != null) {
			string itemName = _resultScreen.GetNewCharacterLevel().GetLevelItemName();
			int itemCoin = _resultScreen.GetNewCharacterLevel().GetCoinRequirement();
			string characterName = GameplayManagerBehaviour.initialPrefab[GameplayManagerBehaviour.playerIdTurn].GetCharacterName();
			string str = "Congratulations! You've unlocked " + itemName +
						", an upgrade for " + characterName +
						". Buy it for Rp " + itemCoin + ",-";
            UINotificationBehaviour.instance.ShowErrorNotification(str);
			return;
        }

		if(_resultScreen.GetNewCharacterUnlocked() != "") {
			string str = "Congratulations! You've unlocked " + _resultScreen.GetNewCharacterUnlocked() + " to be played with";
			UINotificationBehaviour.instance.ShowErrorNotification(str);
			return;
		}
    }

	/*private void HandleShowResult(ShowResult result) {
		switch(result) {
			case ShowResult.Finished:
				_resultScreen.GiveRewardWatchAds(isPaidAudience);
				if(isPaidAudience) NextStage();
				break;
			case ShowResult.Skipped:
				UINotificationBehaviour.instance.ShowErrorNotification("Don't Skip the Ads :(");
                break;
			case ShowResult.Failed:
				UINotificationBehaviour.instance.ShowErrorNotification("Failed to retrieve the Ads");
				break;
		}
	}*/

	IEnumerator RewardCoinCountUp(int finalRewardCoin) {
		for(int i = 0; i < finalRewardCoin; i++) {
			currentRewardCoin += speedCountPerFrame;
			if(currentRewardCoin >= finalRewardCoin) break;
			yield return new WaitForSeconds(1 / 1000);
		}
	}

	IEnumerator PerfectRollDiceCountUp(int finalPerfectRollDice) {
		while(currentRewardCoin != _resultScreen.GetRewardCoin()) {
			yield return null;
		}

		for(int i = 0; i < finalPerfectRollDice; i++) {
			currentPerfectRollDiceRewardCoin += speedCountPerFrame;
			if(currentPerfectRollDiceRewardCoin >= finalPerfectRollDice) break;
			yield return new WaitForSeconds(1 / 1000);
		}
	}

	IEnumerator TotalCoinCountUp(int finalTotalCoin) {
		while(currentPerfectRollDiceRewardCoin != _resultScreen.GetPerfectRollDiceRewardCoin() || currentRewardCoin != _resultScreen.GetRewardCoin()) {
			yield return null;
		}

		for(int i = 0; i < finalTotalCoin; i++) {
			currentTotalCoin += speedCountPerFrame;
			if(currentTotalCoin >= finalTotalCoin) break;
			yield return new WaitForSeconds(1 / 1000);
		}
	}

	public void NextStageButton() {
		nextScene = "loading";
		//if(_resultScreen.GetAdsAvailable() && Advertisement.IsReady()) {
		//	transform.Find("Watch Ads").gameObject.SetActive(true);
		//} else {
			NextStage();
		//}
	}

	public void HomeButton() {
		nextScene = "menuscreen";
		_animator.SetTrigger("exit");
	}

	public void WatchAdsButton() {
		//var options = new ShowOptions { resultCallback = HandleShowResult };
		//Advertisement.Show("rewardedVideo", options);
	}

	public void NextStage() { _animator.SetTrigger("exit"); _resultScreen.NextStage(); }
	public void SetIsPaidAudience(bool cond) { isPaidAudience = cond; }
	public void ChangeScene() { SceneManager.LoadScene(nextScene); }
}
