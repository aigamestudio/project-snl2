﻿using UnityEngine;

public class StateBehaviour : MonoBehaviour {
	public string stateName = "";
	public int turnRemain = 1;
	bool isDestroyed = false;

	void Update() {
		if (turnRemain == 0 && !isDestroyed) {
			isDestroyed = true;
			Destroy(this.gameObject, GameplayManagerBehaviour.STANDARD_TIME);
		}
	}
}
