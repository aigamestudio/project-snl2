﻿using UnityEngine;

public class GameplaySpeedController : MonoBehaviour {
	static int gameSpeed = 1;

	void OnEnable() {
		gameSpeed = GameplayManagerBehaviour.initialGameSpeed;
		Time.timeScale = gameSpeed;
	}

	void OnDisable() {
		Time.timeScale = 1;
	}

	public static void ChangeGameSpeed() {
		gameSpeed = (gameSpeed + 1 > GameplayManagerBehaviour.MAX_GAMESPEED) ? GameplayManagerBehaviour.MIN_GAMESPEED : gameSpeed + 1;
		Time.timeScale = gameSpeed;
	}

	public static void RestoreGameSpeed() {
		Time.timeScale = gameSpeed;
	}

	public static void MaxGameSpeed() {
		Time.timeScale = GameplayManagerBehaviour.MAX_GAMESPEED;
	}

	public static void RushGameSpeed() {
		Time.timeScale = GameplayManagerBehaviour.RUSH_GAMESPEED;
	}

	public static void MinGameSpeed() {
		Time.timeScale = GameplayManagerBehaviour.MIN_GAMESPEED;
	}

	public static void PauseGameSpeed() {
		Time.timeScale = 0;
	}

	public static int GetGameSpeed() { return gameSpeed; }
	public static bool GetIsRushing() { return Time.timeScale == GameplayManagerBehaviour.RUSH_GAMESPEED; }
}
