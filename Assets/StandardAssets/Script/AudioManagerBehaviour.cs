﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManagerBehaviour : MonoBehaviour {
	public const float MAX_VOLUME_IN_FLOAT = 0.0f;
	public const float MIN_VOLUME_IN_FLOAT = -80.0f;

	[SerializeField] AudioMixer _masterMixer;

	void OnEnable () {
		_masterMixer.SetFloat("musicVol", ProfileData.settings_music);
		_masterMixer.SetFloat("sfxVol", ProfileData.settings_sfx);
	}

	public void ToggleMusic() {
		ProfileData.settings_music = (ProfileData.settings_music == MAX_VOLUME_IN_FLOAT) ? MIN_VOLUME_IN_FLOAT : MAX_VOLUME_IN_FLOAT;
		_masterMixer.SetFloat("musicVol", ProfileData.settings_music);

		ProfileData.SaveSettingsData();
	}

	public void ToggleSFX() {
		ProfileData.settings_sfx = (ProfileData.settings_sfx == MAX_VOLUME_IN_FLOAT) ? MIN_VOLUME_IN_FLOAT : MAX_VOLUME_IN_FLOAT;
		_masterMixer.SetFloat("sfxVol", ProfileData.settings_sfx);

		ProfileData.SaveSettingsData();
	}

	public bool GetMusicIsMute() { return ProfileData.settings_music == MAX_VOLUME_IN_FLOAT; }
	public bool GetSFXIsMute() { return ProfileData.settings_sfx == MAX_VOLUME_IN_FLOAT; }
}
