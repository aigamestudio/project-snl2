﻿using UnityEngine;
using System.Collections.Generic;

public class ChooseCharacterController : MonoBehaviour {
	const int CAMPAIGN_AI_CHARACTER_AVAILABLE = 4;

	ChooseCharacterManagerBehaviour _chooseCharacterManager;

	bool hasGenerated = false;
	List<int> availableCharacterType;

	void Awake () {
		_chooseCharacterManager = FindObjectOfType<ChooseCharacterManagerBehaviour>();

		availableCharacterType = new List<int>();
	}

	void OnEnable() {
		int playerId = _chooseCharacterManager.playerId = 0; // multiplayer internet update
		for(int i=0; i<ChooseCharacterManagerBehaviour.userCount; i++)
        {
            _chooseCharacterManager.SetPlayerName(i, "Player "+(i+1));
            _chooseCharacterManager.SetPlayerIsBot(i, false);
        }

		for(int i = 0; i < CAMPAIGN_AI_CHARACTER_AVAILABLE; i++) {
			availableCharacterType.Add(i);
		}
	}

	void Update() {
		HandleAIController();
	}

	private void HandleAIController() {
		if(_chooseCharacterManager.GetIsReady(ChooseCharacterManagerBehaviour.userCount - 1)) {
			if(!hasGenerated) {
				hasGenerated = true;

				for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
					if(_chooseCharacterManager.GetPlayerIsBot(i)) {
                        _chooseCharacterManager.SetPlayerName(i, "CPU " + (i+1));
                        _chooseCharacterManager.SetPlayerIsBot(i, true);
						_chooseCharacterManager.SetIsReady(i, true);

						int newCharacterType = availableCharacterType[Random.Range(0, availableCharacterType.Count)];
						int newCharacterLevel = (GameplayManagerBehaviour.isCampaign) ? 0 : ProfileData.character_level[newCharacterType]; // ganti jadi sesuai profile?
						_chooseCharacterManager.SetCharacterType(i, newCharacterType, newCharacterLevel);
						GetComponent<SwipeToChangeObjectBehaviour>().transform.GetChild(newCharacterType).GetComponent<CharacterBehaviour>().ChangeCharacterLevel(newCharacterLevel);
                        availableCharacterType.Remove(newCharacterType);
					} else {
						int playerCharacterType = _chooseCharacterManager.GetCharacterType(i);
						if(availableCharacterType.Contains(playerCharacterType)) {
							availableCharacterType.Remove(playerCharacterType);
						}
					}
				}
			}
		}
	}
}
