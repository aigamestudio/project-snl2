﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UICreditScreenBehaviour : MonoBehaviour {
	[SerializeField] string nextSceneName = "menuscreen";

	void OnEnable () {
		if (ProfileData.profile_campaign_lastMap >= CampaignScreenBehaviour.MAX_CAMPAIGN_MAP) {
			ProfileData.profile_campaign_lastMap = 0;
			ProfileData.SaveProfileData();
		}
	}
	
	public void ChangeScene() {
		SceneManager.LoadScene(nextSceneName);
	}

	public void SetNextSceneName(string newNextSceneName) { nextSceneName = newNextSceneName; }
}
