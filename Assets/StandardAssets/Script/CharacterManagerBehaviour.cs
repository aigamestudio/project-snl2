﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterManagerBehaviour : MonoBehaviour {
	CharacterBehaviour[] _characters = new CharacterBehaviour[GameplayManagerBehaviour.MAX_PLAYER];

	[SerializeField][Range(1.0f, 10.0f)] float moveSpeed = 3.0f;
	[SerializeField][Range(0.1f, 1.0f)] float checkRadius = 0.1f;

	int currentPlayer = 0;

	public void InstantiateInitialCharacter(CharacterBehaviour[] initialCharacters, int[] characterLevel) {
		for(int i=0; i<_characters.Length; i++) {
			_characters[i] = Instantiate(initialCharacters[i]);
			_characters[i].transform.SetParent(transform);
			_characters[i].playerId = i;
			_characters[i].ChangeCharacterLevel(characterLevel[i]);
            _characters[i].SetPlayerNameText(GameplayManagerBehaviour.playerName[i]);
		}
	}

	public void SetCharacterToPosition(MapTileBehaviour mapTile) {
		for(int i=0; i<_characters.Length; i++) {
			_characters[i].transform.position = mapTile.GetCharacterSlot(_characters[i].playerId).transform.position;
		}
	}

	public List<CharacterBehaviour> GetListCharactersInTile(MapTileBehaviour mapTile, CharacterBehaviour exceptionCharacter) {
		int tileId = mapTile.tileId;
		List<CharacterBehaviour> listCharactersInTile = new List<CharacterBehaviour>();
		for(int i=0; i<_characters.Length; i++) {
			if(_characters[i].position == tileId && _characters[i] != exceptionCharacter) {
				listCharactersInTile.Add(_characters[i]);
			}
		}

		return listCharactersInTile;
	}

	public List<CharacterBehaviour> GetListKOCharacters() {
		List<CharacterBehaviour> listKOCharacter = new List<CharacterBehaviour>();
		for(int i=0; i<_characters.Length; i++) {
			if(_characters[i].currentHP <= 0) {
				listKOCharacter.Add(_characters[i]);
			}
		}
		return listKOCharacter;
	}

	// ada bug disini, kalo dua karakter di tile berbeda tapi deltanya sama
	public List<CharacterBehaviour> GetClosestCharacters(CharacterBehaviour fromCharacter) {
		List<CharacterBehaviour> closestCharacters = new List<CharacterBehaviour>();
		int closestTile = int.MaxValue;
		for(int i=0; i<_characters.Length; i++) {
			if(i != fromCharacter.playerId) {
				int deltaTile = Mathf.Abs(fromCharacter.position - _characters[i].position);
				if(deltaTile < Mathf.Abs(fromCharacter.position - closestTile)) {
					closestTile = _characters[i].position;
				}
			}
		}

		for(int i=0; i<_characters.Length; i++) {
			if(i != fromCharacter.playerId) {
				if(_characters[i].position == closestTile) closestCharacters.Add(_characters[i]);
            }
		}

		return closestCharacters;
	}

	public float GetMoveSpeed() { return moveSpeed; }
	public float GetCheckRadius() { return checkRadius; }
	public T GetCharacterState<T>(int playerId) { return _characters[playerId].GetState<T>(); }
	public void NextPlayerIdTurn() { currentPlayer = (currentPlayer + 1) % GameplayManagerBehaviour.MAX_PLAYER; }
	public int GetCurrentPlayer() { return currentPlayer; }
	public CharacterBehaviour GetCharacter(int playerId) { return _characters[playerId]; }
}
