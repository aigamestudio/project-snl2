﻿using UnityEngine;
using System.Collections.Generic;

public class BattleGeneratorBehaviour : MonoBehaviour {
	[SerializeField] bool canAdditionType = true;
	[SerializeField] bool canSubstractionType = true;
	[SerializeField] bool canMultiplicationType = true;
	[SerializeField] int minRandomWrongChoice = -5;
	[SerializeField] int maxRandomWrongChoice = 6;

	public PhaseCheckBattleBattleBehaviour.BattleType GenerateBattleType() {
		List<PhaseCheckBattleBattleBehaviour.BattleType> listBattleType = new List<PhaseCheckBattleBattleBehaviour.BattleType>();
		if(canAdditionType) listBattleType.Add(PhaseCheckBattleBattleBehaviour.BattleType.Addition);
		if(canSubstractionType) listBattleType.Add(PhaseCheckBattleBattleBehaviour.BattleType.Substraction);
		if(canMultiplicationType) listBattleType.Add(PhaseCheckBattleBattleBehaviour.BattleType.Multiplication);

		return listBattleType[Random.Range(0, listBattleType.Count)];
	}

	public void GenerateBattleQuestion(PhaseCheckBattleBattleBehaviour.BattleType battleType, int[] num, int[] choiceNum, int trueChoice) {
		switch(battleType) {
			case PhaseCheckBattleBattleBehaviour.BattleType.Addition: HandleBattleTypeAddition(num, choiceNum, trueChoice); break;
			case PhaseCheckBattleBattleBehaviour.BattleType.Substraction: HandleBattleTypeSubstraction(num, choiceNum, trueChoice); break;
			case PhaseCheckBattleBattleBehaviour.BattleType.Multiplication: HandleBattleTypeMultiplication(num, choiceNum, trueChoice); break;
		}
	}

	private void HandleBattleTypeAddition(int[] num, int[] choiceNum, int trueChoice) {
		int result = 0;
		for(int i = 0; i < num.Length; i++) {
			num[i] = Random.Range(1, GameplayManagerBehaviour.MAX_DICE + 1);
			result += num[i];
		}

		for(int i = 0; i < choiceNum.Length; i++) {
			if(i == trueChoice) {
				choiceNum[i] = result;
			}
			else {
				do {
					choiceNum[i] = Mathf.Max(1, result + Random.Range(minRandomWrongChoice, maxRandomWrongChoice));
				} while(choiceNum[i] == result);
			}
		}
	}

	private void HandleBattleTypeSubstraction(int[] num, int[] choiceNum, int trueChoice) {
		int result = num[0] = Random.Range(1, GameplayManagerBehaviour.MAX_DICE + 1);
		for(int i = 1; i < num.Length; i++) {
			num[i] = Random.Range(1, GameplayManagerBehaviour.MAX_DICE + 1);
			result -= num[i];
		}

		for(int i = 0; i < choiceNum.Length; i++) {
			if(i == trueChoice) {
				choiceNum[i] = result;
			}
			else {
				do {
					choiceNum[i] = result + Random.Range(minRandomWrongChoice, maxRandomWrongChoice);
				} while(choiceNum[i] == result);
			}
		}
	}

	private void HandleBattleTypeMultiplication(int[] num, int[] choiceNum, int trueChoice) {
		int result = 1;
		for(int i = 0; i < num.Length; i++) {
			num[i] = Random.Range(1, GameplayManagerBehaviour.MAX_DICE + 1);
			result *= num[i];
		}

		for(int i = 0; i < choiceNum.Length; i++) {
			if(i == trueChoice) {
				choiceNum[i] = result;
			}
			else {
				do {
					choiceNum[i] = result / num[0] * Mathf.Max(1, (result / num[0] + Random.Range(minRandomWrongChoice, maxRandomWrongChoice)));
				} while(choiceNum[i] == result);
			}
		}
	}
}
