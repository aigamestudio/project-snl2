﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;
using CloudOnce;

public static class Achievement
{
    public static void UpdateGameStats()
    {
        if(GameplayManagerBehaviour.isCampaign)
        {
            Cloud.Achievements.UnlockAchievement("lets_go_to_the_school");
        } else
        {
            ProfileData.achievement_multiplayer_play_count++;
            ProfileData.SaveAchievementData();

            Cloud.Achievements.IncrementAchievement("lets_play_together", ProfileData.achievement_multiplayer_play_count * 100 / 5, onComplete:null);
            Cloud.Achievements.IncrementAchievement("lets_play_together_2", ProfileData.achievement_multiplayer_play_count * 100 / 10, onComplete:null);
        }
    }

    public static void UpdateRollDiceStats(int playerId, bool isPaired)
    {
        if (GameplayManagerBehaviour.isCampaign && playerId == GameplayManagerBehaviour.playerIdTurn && isPaired)
            Cloud.Achievements.UnlockAchievement("why_the_dice_is_cube");
    }

    public static void UpdateWinStats(bool isWin, int turnCount)
    {
        if(GameplayManagerBehaviour.isCampaign)
        {
            if (isWin) Cloud.Achievements.UnlockAchievement("mommy_i_win_yeaaay");
            else Cloud.Achievements.UnlockAchievement("i_lose_because_youre_cheating");
        }

        if (turnCount >= 30) Cloud.Achievements.UnlockAchievement("its_really_tough_competition");
        else Cloud.Achievements.UnlockAchievement("im_speedrunner");
    }

    public static void UpdateCardUsed(CharacterBehaviour character, CardBehaviour card, bool isUseSpecial)
    {
        if(!GameplayManagerBehaviour.playerIsBot[character.playerId])
        {
            if(isUseSpecial)
            {
                switch(card.GetCardType())
                {
                    case 4:
                        Cloud.Achievements.UnlockAchievement("secret_ninjutsu");
                        break;
                    case 6:
                        Cloud.Achievements.UnlockAchievement("anyone_want_snack");
                        break;
                    case 8:
                        Cloud.Achievements.UnlockAchievement("dont_forget_to_pay_debt");
                        break;
                    case 10:
                        Cloud.Achievements.UnlockAchievement("lets_play_skipping_rope");
                        break;
                    default:
                        return;
                }
            }
        }
    }
}
