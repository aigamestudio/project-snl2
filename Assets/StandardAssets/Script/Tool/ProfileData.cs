﻿using UnityEngine;

public static class ProfileData {
	public static float settings_music = AudioManagerBehaviour.MAX_VOLUME_IN_FLOAT;
	public static float settings_sfx = AudioManagerBehaviour.MAX_VOLUME_IN_FLOAT;
	public static bool[] map_unlocked = new bool[GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP];
	public static int[] map_win_difficulty = new int[GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP];
	public static bool[] map_storyline_done = new bool[GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP];
	public static bool[] character_unlocked = new bool[GameplayManagerBehaviour.AVAILABLE_CHARACTER];
	public static int[] character_level = new int[GameplayManagerBehaviour.AVAILABLE_CHARACTER];
	public static int[] character_campaign_lastMapUnlocked = new int[GameplayManagerBehaviour.AVAILABLE_CHARACTER];
	public static int profile_coin = 0;
	public static int profile_campaign_playerId = 0;
	public static int profile_campaign_lastMap = 0;
	public static int[] profile_campaign_lastCharacters = new int[GameplayManagerBehaviour.MAX_PLAYER];
    public static int achievement_multiplayer_play_count = 0;

    const int INITIAL_AVAILABLE_MAP = 1;
	const int INITIAL_AVAILABLE_CHARACTER = 4;

	public static void SaveSettingsData() {
		PlayerPrefs.SetFloat("settings_music", settings_music);
		PlayerPrefs.SetFloat("settings_sfx", settings_sfx);
		PlayerPrefs.Save();
	}

	public static void SaveMapData() {
		for(int i=0; i<GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP; i++) {
			PlayerPrefs.SetString("map_unlocked" + i, map_unlocked[i].ToString());
			PlayerPrefs.SetInt("map_win_difficulty" + i, map_win_difficulty[i]);
			PlayerPrefs.SetString("map_storyline_done" + i, map_storyline_done[i].ToString());
		}
		PlayerPrefs.Save();
	}

	public static void SaveCharacterData() {
		for(int i = 0; i < GameplayManagerBehaviour.AVAILABLE_CHARACTER; i++) {
			PlayerPrefs.SetString("character_unlocked" + i, character_unlocked[i].ToString());
			PlayerPrefs.SetInt("character_level" + i, character_level[i]);
			PlayerPrefs.SetInt("character_campaign_lastMapUnlocked" + i, character_campaign_lastMapUnlocked[i]);
		}
		PlayerPrefs.Save();
	}

	public static void SaveProfileData() {
		PlayerPrefs.SetInt("profile_coin", profile_coin);
		PlayerPrefs.SetInt("profile_campaign_playerId", profile_campaign_playerId);
		PlayerPrefs.SetInt("profile_campaign_lastMap", profile_campaign_lastMap);

		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			PlayerPrefs.SetInt("profile_campaign_lastCharacter" + i, profile_campaign_lastCharacters[i]);
		}

		PlayerPrefs.Save();
	}
    
    public static void SaveAchievementData()
    {
        PlayerPrefs.SetInt("achievement_multiplayer_play_count", achievement_multiplayer_play_count);

        PlayerPrefs.Save();
    }

    public static void LoadSettingsData() {
		settings_music = PlayerPrefs.GetFloat("settings_music", AudioManagerBehaviour.MAX_VOLUME_IN_FLOAT);
		settings_sfx = PlayerPrefs.GetFloat("settings_sfx", AudioManagerBehaviour.MAX_VOLUME_IN_FLOAT);
	}

	public static void LoadMapData() {
		for(int i=0; i<GameplayManagerBehaviour.AVAILABLE_CAMPAIGN_MAP; i++) {
			string unlockedDefaultValue = (i < INITIAL_AVAILABLE_MAP) ? "True" : "False";
			string storylineDoneDefaultValue = !(i == 0 || i == 4 || i == 8) ? "True" : "False";
			map_unlocked[i] = PlayerPrefs.GetString("map_unlocked" + i, unlockedDefaultValue) == "True";
			map_win_difficulty[i] = PlayerPrefs.GetInt("map_win_difficulty" + i, 0);
			map_storyline_done[i] = PlayerPrefs.GetString("map_storyline_done" + i, storylineDoneDefaultValue) == "True";
		}
	}

	public static void LoadCharacterData() {
		for(int i=0; i<GameplayManagerBehaviour.AVAILABLE_CHARACTER; i++) {
			string defaultValue = (i < INITIAL_AVAILABLE_CHARACTER) ? "True" : "False";
			character_unlocked[i] = PlayerPrefs.GetString("character_unlocked" + i, defaultValue) == "True";
			character_level[i] = PlayerPrefs.GetInt("character_level" + i, 0);
			character_campaign_lastMapUnlocked[i] = PlayerPrefs.GetInt("character_campaign_lastMapUnlocked" + i, 0);
		}
	}

	public static void LoadProfileData() {
		profile_coin = PlayerPrefs.GetInt("profile_coin", 0);
		profile_campaign_playerId = PlayerPrefs.GetInt("profile_campaign_playerId", 0);
		profile_campaign_lastMap = PlayerPrefs.GetInt("profile_campaign_lastMap", 0);

		for(int i=0; i<GameplayManagerBehaviour.MAX_PLAYER; i++) {
			profile_campaign_lastCharacters[i] = PlayerPrefs.GetInt("profile_campaign_lastCharacter" + i, i);
		}
	}

    public static void LoadAchievementData()
    {
        achievement_multiplayer_play_count = PlayerPrefs.GetInt("achievement_multiplayer_play_count", 0);
    }
}
