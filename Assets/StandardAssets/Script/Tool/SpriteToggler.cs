﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpriteToggler : MonoBehaviour {

	[SerializeField] List<Sprite> sprites = new List<Sprite>();
	Image img;
	int wrapIndex = 0;

	void Awake () {
		img = GetComponent<Image> ();
	}

	public void Toggle(Sprite spr0, Sprite spr1) {
		img.sprite = (img.sprite == spr0) ? spr1 : spr0;
	}

	public void ToggleSprite (int index) {
		Toggle (sprites[index], sprites[index + 1]);
	}

	public void SetWrapIndex(int index) {
		wrapIndex = index;
		img.sprite = sprites[wrapIndex];
	}

	public void WrapSprite() {
		wrapIndex = (wrapIndex + 1 > sprites.Count) ? 0 : wrapIndex + 1;
		img.sprite = sprites[wrapIndex];
	}

	public Image Img {
		get {
			return this.img;
		}
		set {
			img = value;
		}
	}
}
