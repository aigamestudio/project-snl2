﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class SwipeToChangeObjectBehaviour : MonoBehaviour {
	[Range(10.0f, 100.0f)] [SerializeField] float speed = 10.0f;
	List<GameObject> _slide;
	int currentSlide = 0;
	bool onSlideCurrent = false;
	bool onSlideNext = false;

	Vector2 firstPressPos;

	void Awake() {
		_slide = new List<GameObject>();
		foreach(Transform children in transform) {
			_slide.Add(children.gameObject);
		}
	}

	void Update() {
		HandleSwipe();
	}

	private void HandleSwipe() {
		if(Input.GetMouseButtonDown(0)) {
			firstPressPos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
		}

		if(Input.GetMouseButtonUp(0)) {
			Vector2 secondPressPos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
			Vector2 currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

			if(currentSwipe.x < -0.25 && !onSlideCurrent && !onSlideNext) {
				NextSlide();
			}
			if(currentSwipe.x > 0.25 && !onSlideCurrent && !onSlideNext) {
				PreviousSlide();
			}
		}
	}

	public void NextSlide() {
		int nextSlide = (int)Mathf.Repeat(currentSlide + 1, _slide.Count);
		_slide[nextSlide].transform.position = new Vector3(15f, _slide[nextSlide].transform.position.y, 0);
		float distanceObject = Vector2.Distance(_slide[currentSlide].transform.position, _slide[nextSlide].transform.position);
		float lastPositionObject = _slide[currentSlide].transform.position.x;
		StartCoroutine(MoveObjectTowardsPosition(_slide[currentSlide], _slide[currentSlide].transform.position, new Vector3(-distanceObject, _slide[nextSlide].transform.position.y, 0), true));
		StartCoroutine(MoveObjectTowardsPosition(_slide[nextSlide], _slide[nextSlide].transform.position, new Vector3(lastPositionObject, _slide[currentSlide].transform.position.y, 0), false));
		currentSlide = nextSlide;
	}

	public void PreviousSlide() {
		int prevSlide = (int)Mathf.Repeat(currentSlide - 1, _slide.Count);
		_slide[prevSlide].transform.position = new Vector3(-15f, _slide[prevSlide].transform.position.y, 0);
		float distanceObject = Vector2.Distance(_slide[currentSlide].transform.position, _slide[prevSlide].transform.position);
		float lastPositionObject = _slide[currentSlide].transform.position.x;
		StartCoroutine(MoveObjectTowardsPosition(_slide[currentSlide], _slide[currentSlide].transform.position, new Vector3(distanceObject + lastPositionObject, _slide[prevSlide].transform.position.y, 0), true));
		StartCoroutine(MoveObjectTowardsPosition(_slide[prevSlide], _slide[prevSlide].transform.position, new Vector3(lastPositionObject, _slide[currentSlide].transform.position.y, 0), false));
		currentSlide = prevSlide;
	}

	IEnumerator MoveObjectTowardsPosition(GameObject go, Vector3 startPos, Vector3 endPos, bool isCurrent) {
		float startTime = Time.time;
		float journeyLength = Vector3.Distance(startPos, endPos);

		if(isCurrent) onSlideCurrent = true;
		else onSlideNext = true;

		while(go.transform.position != endPos) {
			float distCovered = (Time.time - startTime) * speed;
			float fracJourney = distCovered / journeyLength;
			go.transform.position = Vector3.Lerp(startPos, endPos, fracJourney);
			yield return new WaitForSeconds(0.01f);
		}

		if(isCurrent) onSlideCurrent = false;
		else onSlideNext = false;
	}

	public int GetCurrentSlide() { return currentSlide; }
}
