﻿using UnityEngine;
using UnityEngine.UI;

public class CopyComponent : MonoBehaviour {
	enum ComponentType { Text, Image }
	[SerializeField] ComponentType _copyComponentType = ComponentType.Text;

	[SerializeField] GameObject _copyComponent;

	void Update() {
		switch(_copyComponentType) {
			case ComponentType.Text:
				Text copyComponentText = _copyComponent.GetComponent<Text>();
				Text componentText = GetComponent<Text>();
				componentText.text = copyComponentText.text;
				break;
			case ComponentType.Image:
				Image copyComponentImage = _copyComponent.GetComponent<Image>();
				Image componentImage = GetComponent<Image>();
				componentImage.sprite = copyComponentImage.sprite;
				break;
		}
	}
}
