﻿using UnityEngine;

public class GameplayManagerBehaviour : MonoBehaviour {
	public enum GameplayPhase {
		CheckStates,
		UseCard, UseCard_SelectTarget, UseCard_Action,							// check target pas use card; action ke checkko dulu; kasih state pas animasi;
		RollDice, RollDice_Move,                                                // roll dice isinya check kalo udah >0 maka next phase
		CheckBattle, CheckBattle_Battle,                                        // checkbattle_battle cek boss mati apa engga
		CheckTeleport, CheckTeleport_Move,										// teleportsnake teleportladder sebelum pindah tentuin animasi
		CheckCheckpoint, CheckKO, CheckRollDicePair,                            // checkcheckpoint nambah darahnya di animasi <-- belom fix, alt : tambah phase baru
		TakeCard, DiscardCard,
		NextPlayer, Win
	}

	public const int MAX_PLAYER = 4;
	public const int MIN_GAMESPEED = 1;
	public const int MAX_GAMESPEED = 4;
	public const int RUSH_GAMESPEED = 6;
	public const float STANDARD_TIME = 1.0f;
	public const int START_POSITION = 0;
	public const int MAX_POINT = 99;
	public const int NUMBER_OF_DICE = 2;
	public const int MAX_DICE = 6;
	public const int AVAILABLE_CARD_TYPE = 21;
	public const int MAX_CARD_SLOT = 4;
	public const int BATTLE_TOTAL_CHOICE = 2;
	public const int BATTLE_MAX_COUNT = 5;
	public const int MAX_AI_DIFFICULTY = 2;
	public const int MIN_AI_DIFFICULTY = 1;
	public const int AVAILABLE_CAMPAIGN_MAP = 9;
	public const int AVAILABLE_CHARACTER = 6;
	public const int AVAILABLE_CHARACTER_DEFAULT = 4;
	public const int MAX_CHARACTER_LEVEL = 4;
	public const int OVERLUCK_DICE_PAIRED = 2;

	public static bool isCampaign = true;
	public static int playerIdTurn = -1;
	public static int aiDifficulty = 1;
	public static int initialGameSpeed = 1;
	public static string[] playerName = new string[MAX_PLAYER];
	public static bool[] playerIsBot = new bool[MAX_PLAYER];
	public static CharacterBehaviour[] initialPrefab = new CharacterBehaviour[MAX_PLAYER];
	public static int[] initialPrefabLevel = new int[MAX_PLAYER];

	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	GameplayPhase _currentPhase = GameplayPhase.CheckStates;
	PhaseCheckStatesBehaviour _checkStates;
	PhaseUseCardBehaviour _useCard;
	PhaseUseCardSelectTargetBehaviour _useCardSelectTarget;
	PhaseUseCardActionBehaviour _useCardAction;
	PhaseRollDiceBehaviour _rollDice;
	PhaseRollDiceMoveBehaviour _rollDiceMove;
	PhaseCheckBattleBehaviour _checkBattle;
	PhaseCheckBattleBattleBehaviour _checkBattleBattle;
	PhaseCheckTeleportBehaviour _checkTeleport;
	PhaseCheckTeleportMoveBehaviour _checkTeleportMove;
	PhaseCheckCheckpointBehaviour _checkCheckpoint;
	PhaseCheckKOBehaviour _checkKO;
	PhaseCheckRollDicePairBehaviour _checkRollDicePair;
	PhaseTakeCardBehaviour _takeCard;
	PhaseDiscardCardBehaviour _discardCard;
	PhaseNextPlayerBehaviour _nextPlayer;
	PhaseWinBehaviour _win;

	void Awake() {
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();
		_checkStates = transform.GetComponentInChildren<PhaseCheckStatesBehaviour>(true);
		_useCard = transform.GetComponentInChildren<PhaseUseCardBehaviour>(true);
		_useCardSelectTarget = transform.GetComponentInChildren<PhaseUseCardSelectTargetBehaviour>(true);
		_useCardAction = transform.GetComponentInChildren<PhaseUseCardActionBehaviour>(true);
		_rollDice = transform.GetComponentInChildren<PhaseRollDiceBehaviour>(true);
		_rollDiceMove = transform.GetComponentInChildren<PhaseRollDiceMoveBehaviour>(true);
		_checkBattle = transform.GetComponentInChildren<PhaseCheckBattleBehaviour>(true);
		_checkBattleBattle = transform.GetComponentInChildren<PhaseCheckBattleBattleBehaviour>(true);
		_checkTeleport = transform.GetComponentInChildren<PhaseCheckTeleportBehaviour>(true);
		_checkTeleportMove = transform.GetComponentInChildren<PhaseCheckTeleportMoveBehaviour>(true);
		_checkCheckpoint = transform.GetComponentInChildren<PhaseCheckCheckpointBehaviour>(true);
		_checkKO = transform.GetComponentInChildren<PhaseCheckKOBehaviour>(true);
		_checkRollDicePair = transform.GetComponentInChildren<PhaseCheckRollDicePairBehaviour>(true);
		_takeCard = transform.GetComponentInChildren<PhaseTakeCardBehaviour>(true);
		_discardCard = transform.GetComponentInChildren<PhaseDiscardCardBehaviour>(true);
		_nextPlayer = transform.GetComponentInChildren<PhaseNextPlayerBehaviour>(true);
		_win = transform.GetComponentInChildren<PhaseWinBehaviour>(true);
	}

	void OnEnable() {
		_characterManager.InstantiateInitialCharacter(initialPrefab, initialPrefabLevel);
		_characterManager.SetCharacterToPosition(_map.GetMapTile(START_POSITION));

		ChangePhase(_currentPhase);

        Achievement.UpdateGameStats();
	}

	private void ActivatePhase(GameplayPhase phase) {
		_currentPhase = phase;

		switch(_currentPhase) {
			case GameplayPhase.CheckStates: _checkStates.gameObject.SetActive(true); break;
			case GameplayPhase.UseCard: _useCard.gameObject.SetActive(true); break;
			case GameplayPhase.UseCard_SelectTarget: _useCardSelectTarget.gameObject.SetActive(true); break;
			case GameplayPhase.UseCard_Action: _useCardAction.gameObject.SetActive(true); break;
			case GameplayPhase.RollDice: _rollDice.gameObject.SetActive(true); break;
			case GameplayPhase.RollDice_Move: _rollDiceMove.gameObject.SetActive(true); break;
			case GameplayPhase.CheckBattle: _checkBattle.gameObject.SetActive(true); break;
			case GameplayPhase.CheckBattle_Battle: _checkBattleBattle.gameObject.SetActive(true); break;
			case GameplayPhase.CheckTeleport: _checkTeleport.gameObject.SetActive(true); break;
			case GameplayPhase.CheckTeleport_Move: _checkTeleportMove.gameObject.SetActive(true); break;
			case GameplayPhase.CheckCheckpoint: _checkCheckpoint.gameObject.SetActive(true); break;
			case GameplayPhase.CheckKO: _checkKO.gameObject.SetActive(true); break;
			case GameplayPhase.CheckRollDicePair: _checkRollDicePair.gameObject.SetActive(true); break;
			case GameplayPhase.TakeCard: _takeCard.gameObject.SetActive(true); break;
			case GameplayPhase.DiscardCard: _discardCard.gameObject.SetActive(true); break;
			case GameplayPhase.NextPlayer: _nextPlayer.gameObject.SetActive(true); break;
			case GameplayPhase.Win: _win.gameObject.SetActive(true); break;
		}
	}

	private void DeactiveAllPhase() {
		foreach(Transform children in transform) {
			children.gameObject.SetActive(false);
		}
	}

	public void ChangePhase(GameplayPhase phase) {
		DeactiveAllPhase();
		ActivatePhase(phase);
	}
	
	public GameplayPhase GetCurrentPhase() { return _currentPhase; }
}
