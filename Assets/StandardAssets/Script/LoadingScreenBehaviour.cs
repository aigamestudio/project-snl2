﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenBehaviour : MonoBehaviour {
	public static string changeScene = "hutan tropis";
	public static bool skipStoryline = false;

	[SerializeField] float loadingTime = 2.0f;
	[SerializeField] Animator _storyProgress;
	[SerializeField] Text _factText;
	[SerializeField][TextArea(1, 3)] string[] fact;

	void OnEnable () {
		if(GameObject.FindObjectOfType<Singleton>()) Destroy(Singleton.instance.gameObject);
		Invoke("ChangeScene", loadingTime);

		_factText.text = fact[Random.Range(0, fact.Length)];
		if(GameplayManagerBehaviour.isCampaign) {
			_factText.rectTransform.anchoredPosition = new Vector2(0.0f, 125.0f);
			_storyProgress.gameObject.SetActive(true);
			_storyProgress.SetTrigger(changeScene);
			_storyProgress.transform.Find("Level Name Text").GetComponent<Text>().text = changeScene;
		}
	}

	void ChangeScene() {
		if (!skipStoryline) {
			SceneManager.LoadScene("storyline - " + changeScene);
		} else {
			SceneManager.LoadScene("gameplay - " + changeScene);
		}
	}
	
	public static void SetAIDifficulty(int difficulty) { GameplayManagerBehaviour.aiDifficulty = difficulty; }
	public static void SetPlayerIdTurn(int playerId) { GameplayManagerBehaviour.playerIdTurn = playerId; }
	public static void SetInitialGameSpeed(int initialGameSpeed) { GameplayManagerBehaviour.initialGameSpeed = initialGameSpeed; }
	public static void SetPlayerName(string[] playerName) { GameplayManagerBehaviour.playerName = playerName; }
	public static void SetPlayerIsBot(bool[] playerIsBot) { GameplayManagerBehaviour.playerIsBot = playerIsBot; }
	public static void SetInitialCharacter(CharacterBehaviour[] initialPrefab) { GameplayManagerBehaviour.initialPrefab = initialPrefab; }
	public static void SetInitialCharacterLevel(int[] initialPrefabLevel) { GameplayManagerBehaviour.initialPrefabLevel = initialPrefabLevel; }
}
