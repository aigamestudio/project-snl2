﻿using UnityEngine;

public class CampaignScreenBehaviour : MonoBehaviour {
	[SerializeField] CharacterBehaviour[] _availableCharacterPrefab;
	[SerializeField] MapBehaviour[] _campaignMaps;

	[HideInInspector] Animator _animator;

	public const int MAX_CAMPAIGN_MAP = 8;

	void Awake() {
		_animator = GetComponent<Animator>();
	}

    void OnEnable() {
        _animator.SetBool("loadIsExist", ProfileData.profile_campaign_lastMap > 0);
        if (ProfileData.profile_campaign_lastMap == 0)
        {
            NewGameButton();
            ChooseCharacterManagerBehaviour.userCount = 1;
            FindObjectOfType<UIMenuScreenBehaviour>().ChangeScene("choosecharacter");
        }
	}

	private void HandleMapLoadingVariable(int levelId) {
		LoadingScreenBehaviour.SetInitialGameSpeed(2);
		LoadingScreenBehaviour.changeScene = _campaignMaps[levelId].GetMapName();
		LoadingScreenBehaviour.skipStoryline = !(GameplayManagerBehaviour.isCampaign && _campaignMaps[levelId].GetMapHasStoryline() && !ProfileData.map_storyline_done[levelId]);
	}

	private void HandleCharacterLoadingVariable() {
		LoadingScreenBehaviour.SetPlayerIdTurn(ProfileData.profile_campaign_playerId);
		LoadingScreenBehaviour.SetPlayerName(GenerateNameCPUForCampaign());
		LoadingScreenBehaviour.SetPlayerIsBot(GenerateIsBotForCampaign());
		LoadingScreenBehaviour.SetInitialCharacter(GenerateInitialPrefabForCampaign());
		LoadingScreenBehaviour.SetInitialCharacterLevel(GenerateLevelForCampaign());
	}

	private string[] GenerateNameCPUForCampaign() {
		string[] playersName = new string[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i = 0; i < playersName.Length; i++) {
			if(i == ProfileData.profile_campaign_playerId) playersName[i] = "Player";
			else playersName[i] = "CPU";
		}
	
		return playersName;
	}

	private static bool[] GenerateIsBotForCampaign() {
		bool[] playerIsBot = new bool[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i = 0; i < playerIsBot.Length; i++) {
			if(i == ProfileData.profile_campaign_playerId) playerIsBot[i] = false;
			else playerIsBot[i] = true;
		}

		return playerIsBot;
	}

	private int[] GenerateLevelForCampaign() {
		int[] playersLevel = new int[GameplayManagerBehaviour.MAX_PLAYER];
		int playerLevel = ProfileData.character_level[ProfileData.profile_campaign_lastCharacters[ProfileData.profile_campaign_playerId]];
		for(int i = 0; i < playersLevel.Length; i++) {
			if(i == ProfileData.profile_campaign_playerId) playersLevel[i] = playerLevel;
			else playersLevel[i] = _campaignMaps[ProfileData.profile_campaign_lastMap].GetBotLevel();
        }

		return playersLevel;
	}

	private CharacterBehaviour[] GenerateInitialPrefabForCampaign() {
		CharacterBehaviour[] initialPrefab = new CharacterBehaviour[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			initialPrefab[i] = _availableCharacterPrefab[ProfileData.profile_campaign_lastCharacters[i]];
		}

		return initialPrefab;
	}

	public void NewGameButton() {
		HandleMapLoadingVariable(0);

		ProfileData.profile_campaign_lastMap = 0;
		ProfileData.SaveProfileData();
	}

	public void ContinueButton() {
		HandleMapLoadingVariable(ProfileData.profile_campaign_lastMap);
		HandleCharacterLoadingVariable();
	}

	public void SetIsCampaign(bool cond) { GameplayManagerBehaviour.isCampaign = cond; }
}