﻿using UnityEngine;
using System.Collections;

public class PhaseNetworkBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;

	bool[] isPlayerReady = new bool[GameplayManagerBehaviour.MAX_PLAYER];

	void Awake() {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
	}

	void OnEnable() {
		for(int i=0; i<isPlayerReady.Length; i++) {
			isPlayerReady[i] = false;
		}
	}

	private void SetupChangePhase() {
		SyncPlayerReady(GameplayManagerBehaviour.playerIdTurn);
		for(int i = 0; i < GameplayManagerBehaviour.playerIsBot.Length; i++) {
			if(GameplayManagerBehaviour.playerIsBot[i]) SyncPlayerReady(i);
		}
	}
	
	private void SyncPlayerReady(int playerId) {
		isPlayerReady[playerId] = true;
	}

	public void ChangePhase(GameplayManagerBehaviour.GameplayPhase phase) {
		SetupChangePhase();
		StartCoroutine(WaitForChangePhase(phase));
	}

	IEnumerator WaitForChangePhase(GameplayManagerBehaviour.GameplayPhase phase) {
		bool isReady = false;
		while(!isReady) {
			for(int i = 0; i < isPlayerReady.Length; i++) {
				if(!isPlayerReady[i]) yield return null;
			}

			isReady = true;
		}

		_gameplayManager.ChangePhase(phase);
	}
}
