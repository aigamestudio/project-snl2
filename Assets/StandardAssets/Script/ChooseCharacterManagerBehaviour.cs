﻿using UnityEngine;

public class ChooseCharacterManagerBehaviour : MonoBehaviour {
	[SerializeField] CharacterBehaviour[] _availableCharacterPrefab;

    public static int userCount = 1;

	public int playerId = 0;
	string[] playerName = new string[GameplayManagerBehaviour.MAX_PLAYER];
	bool[] playerIsBot = new bool[GameplayManagerBehaviour.MAX_PLAYER]; 
	int[] characterType = new int[GameplayManagerBehaviour.MAX_PLAYER];
	int[] characterLevel = new int[GameplayManagerBehaviour.MAX_PLAYER];
	bool[] isReady = new bool[GameplayManagerBehaviour.MAX_PLAYER];

	bool canPlay = false;

	void Awake() {
		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			playerName[i] = "";
			playerIsBot[i] = true;
			characterType[i] = -1;
			isReady[i] = false;
		}
	}

	void Update() {
		CheckCanPlay();
		HandleLoadingCharacterType();
	}

	private void CheckCanPlay() {
		for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
			canPlay = isReady[i];
			if(!canPlay) return;
		}
	}

	private void HandleLoadingCharacterType() {
		if(canPlay) {
			CharacterBehaviour[] initialPrefab = new CharacterBehaviour[GameplayManagerBehaviour.MAX_PLAYER];
			for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
				initialPrefab[i] = _availableCharacterPrefab[characterType[i]];
			}

			LoadingScreenBehaviour.SetInitialCharacter(initialPrefab);
			LoadingScreenBehaviour.SetInitialCharacterLevel(characterLevel);
		}
	}

	public void LoadingScene() {
        LoadingScreenBehaviour.SetPlayerName(playerName);
        LoadingScreenBehaviour.SetPlayerIsBot(playerIsBot);
        LoadingScreenBehaviour.SetPlayerIdTurn(playerId);
	}

	public bool GetCanPlay() { return canPlay; }
	public bool GetIsReady(int id) { return isReady[id]; }
	public string GetPlayerName(int id) { return playerName[id]; }
	public bool GetPlayerIsBot(int id) { return playerIsBot[id]; }
	public int GetCharacterType(int id) { return characterType[id]; }
	public void SetPlayerName(int id, string name) { playerName[id] = name; }
	public void SetPlayerIsBot(int id, bool cond) { playerIsBot[id] = cond; }
	public void SetIsReady(int id, bool cond) { isReady[id] = cond; }
	public void SetCharacterType(int id, int type, int level) { characterType[id] = type; characterLevel[id] = level; }
}
