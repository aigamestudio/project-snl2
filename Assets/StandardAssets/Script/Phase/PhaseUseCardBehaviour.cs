﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseUseCardBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	CardDeckBehaviour _cardDeck;

	PhaseUseCardSelectTargetBehaviour _useCard_selectTarget;
	PhaseUseCardActionBehaviour _useCard_action;
	PhaseNetworkBehaviour _phaseNetwork;

	[HideInInspector] public bool canUseCard = true;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_cardDeck = GameObject.FindObjectOfType<CardDeckBehaviour>();

		_useCard_selectTarget = Resources.FindObjectsOfTypeAll<PhaseUseCardSelectTargetBehaviour>()[0];
		_useCard_action = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_phaseNetwork = GetComponent<PhaseNetworkBehaviour>();
	}
	
	public void UseCard(int slot, bool isUseSpecial) {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
        CardBehaviour card = currentPlayer.GetCards().GetChild(slot).GetComponent<CardBehaviour>();
        Achievement.UpdateCardUsed(currentPlayer, card, isUseSpecial);
		card.PutCardToDeck(_cardDeck);
		_useCard_action._cardUsed = card;
		_useCard_action.isUseSpecial = isUseSpecial;
		if(isUseSpecial) {
			if(card.GetCardSpecialTargetType() == CardBehaviour.TargetType.Target) {
				UseCardSelectTargetPhase();
			} else {
				_useCard_action.listTarget = GenerateListTarget(card.GetCardSpecialTargetType());
				UseCardActionPhase();
			}
		} else {
			if(card.GetCardTargetType() == CardBehaviour.TargetType.Target) {
				UseCardSelectTargetPhase();
			} else {
				_useCard_action.listTarget = GenerateListTarget(card.GetCardTargetType());
				UseCardActionPhase();
			}
		}
	}

	private List<CharacterBehaviour> GenerateListTarget(CardBehaviour.TargetType targetType) {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		List<CharacterBehaviour> listTarget = new List<CharacterBehaviour>();
		switch(targetType) {
			case CardBehaviour.TargetType.Self:
				listTarget.Add(currentPlayer);
				break;
			case CardBehaviour.TargetType.AllExceptSelf:
				for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
					if(i != _characterManager.GetCurrentPlayer()) {
						listTarget.Add(_characterManager.GetCharacter(i));
					}
				}
				break;
			case CardBehaviour.TargetType.All:
				for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
					listTarget.Add(_characterManager.GetCharacter(i));
				}
				break;
			case CardBehaviour.TargetType.ClosestPlayer:
				listTarget = _characterManager.GetClosestCharacters(currentPlayer);
				break;
		}

		return listTarget;
	}
	
	public void RollDicePhase() {
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.RollDice);
	}

	public void UseCardSelectTargetPhase() { 
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.UseCard_SelectTarget);
	}

	public void UseCardActionPhase() {
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.UseCard_Action);
	}
}
