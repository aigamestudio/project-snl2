﻿using UnityEngine;

public class PhaseCheckRollDicePairBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;

	[SerializeField] StateSkipBehaviour _stateOverluckPrefab;

	[HideInInspector] public bool isPaired = false;
	int pairedCount = 0;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
	}

	void OnEnable() {
		if(isPaired) {
			pairedCount++;
			if(pairedCount >= GameplayManagerBehaviour.OVERLUCK_DICE_PAIRED) {
				pairedCount = 0;
				_uiGameplayScreen.ShowText("Overluck!");

				CharacterBehaviour currentCharacter = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
				StateSkipBehaviour overluckState = Instantiate(_stateOverluckPrefab);
				currentCharacter.AddState(overluckState.GetComponent<StateBehaviour>());
				currentCharacter.SetAnimation(CharacterBehaviour.AnimationType.Overluck);
				Invoke("NextPlayerPhase", _uiGameplayScreen.GetShowTextDelay());
			} else {
				Invoke("RollDicePhase", GameplayManagerBehaviour.STANDARD_TIME);
			}
		} else {
			pairedCount = 0;
			Invoke("TakeCardPhase", GameplayManagerBehaviour.STANDARD_TIME);
		}
	}

	private void TakeCardPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.TakeCard);
	}

	private void RollDicePhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.RollDice);
	}

	private void NextPlayerPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.NextPlayer);
	}

	public int GetPairedCount() { return pairedCount; }
}
