﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseCheckTeleportMoveBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	PhaseCheckKOBehaviour _checkKO;
	MapTileBehaviour _targetTile;
	[HideInInspector] public List<CharacterBehaviour> listTarget = new List<CharacterBehaviour>();

	bool[] targetHasArrived;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();

		_checkKO = Resources.FindObjectsOfTypeAll<PhaseCheckKOBehaviour>()[0];
	}

	void OnEnable() {
		targetHasArrived = new bool[listTarget.Count];
	}
	
	void Update () {
		HandleMovement();
		CheckTeleportPhase();
	}

	void OnDisable() {
		listTarget.Clear();
	}

	private void HandleMovement() {
		for(int i = 0; i < listTarget.Count; i++) {
			if(!targetHasArrived[i]) {
				Vector2 targetSlotTile = _targetTile.GetCharacterSlot(listTarget[i].playerId).transform.position;
				listTarget[i].MoveToPosition(targetSlotTile, _characterManager.GetMoveSpeed());
				targetHasArrived[i] = listTarget[i].CheckArrivedInPosition(targetSlotTile, _characterManager.GetCheckRadius());
				if(targetHasArrived[i]) {
					listTarget[i].position = _targetTile.tileId;
					listTarget[i].SetAnimation(CharacterBehaviour.AnimationType.Idle);
				}
			}
		}
	}

	private void CheckTeleportPhase() {
		for(int i = 0; i < targetHasArrived.Length; i++) {
			if(!targetHasArrived[i]) return;
		}

		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckTeleport);
	}

	public void SetTargetTile(MapTileBehaviour targetTile) { _targetTile = targetTile; }
}
