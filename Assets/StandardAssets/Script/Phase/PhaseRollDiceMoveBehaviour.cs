﻿using UnityEngine;

public class PhaseRollDiceMoveBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	PhaseCheckBattleBehaviour _checkBattle;
	PhaseCheckTeleportBehaviour _checkTeleport;

	int moveInput = 0;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();

		_checkBattle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBehaviour>()[0];
		_checkTeleport = Resources.FindObjectsOfTypeAll<PhaseCheckTeleportBehaviour>()[0];
	}

	void OnEnable() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.Run);
	}

	void Update() {
		HandleMovement();
	}

	void OnDisable() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		MapTileBehaviour currentTile = _map.GetMapTile(currentPlayer.position);
		currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.Idle);

		_checkBattle.ResetHasGenerated();
		_checkBattle.ResetIndexer();

		_checkTeleport.SetHasMoved(_characterManager.GetCurrentPlayer(), true);
	}

	private void HandleMovement() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		int playerPos = currentPlayer.position;
		int maxPos = _map.GetMapTileCount();

		MapTileBehaviour targetTile = _map.GetMapTile(Mathf.Min(maxPos - 1, playerPos + 1));
		Vector2 targetTilePos = targetTile.GetCharacterSlot(currentPlayer.playerId).transform.position;

		currentPlayer.MoveToPosition(targetTilePos, _characterManager.GetMoveSpeed());
		if(currentPlayer.CheckArrivedInPosition(targetTilePos, _characterManager.GetCheckRadius())) {
			moveInput--;
			currentPlayer.position = targetTile.tileId;
			if(moveInput == 0) {
				CheckBattlePhase();
			}
		}
	}

	private void CheckBattlePhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckBattle);
	}

	public void SetMoveInput(int input) { moveInput = input; }
}
