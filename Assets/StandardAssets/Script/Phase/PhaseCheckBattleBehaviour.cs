﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseCheckBattleBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	PhaseCheckKOBehaviour _checkKO;

	PhaseCheckBattleBattleBehaviour _checkBattle_Battle;
	List<CharacterBehaviour> characterOnTileBeforeBattle;

	bool hasGenerated = false;
	int characterOnTileIndexer = 0;
	bool hasFightBoss = false;

	void Awake() {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();

		_checkKO = Resources.FindObjectsOfTypeAll<PhaseCheckKOBehaviour>()[0];
		_checkBattle_Battle = Resources.FindObjectsOfTypeAll<PhaseCheckBattleBattleBehaviour>()[0];
	}

	void OnEnable() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		MapTileBehaviour currentTile = _map.GetMapTile(currentPlayer.position);
		List<CharacterBehaviour> currentCharacterOnTile = _characterManager.GetListCharactersInTile(currentTile, currentPlayer);
		bool isBossTile = currentTile.GetTileType() == MapTileBehaviour.TileType.Finish;
		
		if(!hasGenerated) {
			hasGenerated = true;
			characterOnTileBeforeBattle = _characterManager.GetListCharactersInTile(currentTile, currentPlayer);
		}

		if(characterOnTileBeforeBattle.Count > 0 && characterOnTileIndexer < characterOnTileBeforeBattle.Count) {
			_checkBattle_Battle._challenger = currentPlayer;
			_checkBattle_Battle._challenged = characterOnTileBeforeBattle[characterOnTileIndexer];
			_checkBattle_Battle.SetIsBossBattle(false);
			Invoke("BattlePhase", GameplayManagerBehaviour.STANDARD_TIME);
		} else if(isBossTile && !hasFightBoss && currentCharacterOnTile.Count == 0) {
			hasFightBoss = true;
			_checkBattle_Battle._challenger = currentPlayer;
			_checkBattle_Battle._challenged = _map.GetBossCharacter();
			_checkBattle_Battle.SetIsBossBattle(true);
			Invoke("BattlePhase", GameplayManagerBehaviour.STANDARD_TIME);
		} else {
			CheckTeleportPhase();
		}
	}

	private void BattlePhase() {
		characterOnTileIndexer++;
		_checkKO.SetNextPhase(GameplayManagerBehaviour.GameplayPhase.CheckBattle);
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckBattle_Battle);
	}

	private void CheckTeleportPhase() {
		_checkKO.SetNextPhase(GameplayManagerBehaviour.GameplayPhase.CheckCheckpoint);
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckTeleport);
	}

	public void ResetHasGenerated() { hasGenerated = false; }
	public void ResetIndexer() { characterOnTileIndexer = 0; hasFightBoss = false; }
}
