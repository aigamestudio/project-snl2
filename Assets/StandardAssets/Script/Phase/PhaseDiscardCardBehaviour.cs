﻿using UnityEngine;

public class PhaseDiscardCardBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	CardDeckBehaviour _cardDeck;

	PhaseNetworkBehaviour _phaseNetwork;
	
	[HideInInspector] public CardBehaviour _newCard;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_cardDeck = Resources.FindObjectsOfTypeAll<CardDeckBehaviour>()[0];

		_phaseNetwork = GetComponent<PhaseNetworkBehaviour>();
	}
	
	public void ReplaceSlot(int slot) { 
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		currentPlayer.GetCards().GetChild(slot).GetComponent<CardBehaviour>().PutCardToDeck(_cardDeck);
		_newCard.AddCardToCharacter(currentPlayer);
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.NextPlayer);
	}
	
	public void DiscardNewCard() {
		_newCard.PutCardToDeck(_cardDeck);
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.NextPlayer);
	}
}
