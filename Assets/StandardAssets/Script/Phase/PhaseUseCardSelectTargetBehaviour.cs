﻿using UnityEngine;

public class PhaseUseCardSelectTargetBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	PhaseUseCardActionBehaviour _useCard_action;
	PhaseNetworkBehaviour _phaseNetwork;

	CharacterBehaviour target;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_useCard_action = Resources.FindObjectsOfTypeAll<PhaseUseCardActionBehaviour>()[0];
		_phaseNetwork = GetComponent<PhaseNetworkBehaviour>();
	}
	
	public void UseCardActionPhase(int playerId) {
		target = _characterManager.GetCharacter(playerId);
		_useCard_action.listTarget.Add(target);
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.UseCard_Action);
	}

	public CardBehaviour GetCardUsed() { return _useCard_action._cardUsed; }
}
