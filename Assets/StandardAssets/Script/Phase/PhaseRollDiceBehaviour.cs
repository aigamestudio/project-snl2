﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseRollDiceBehaviour : MonoBehaviour {
	public enum RollDiceOutputType { Random, Unlucky, Perfect, None }

	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;

	PhaseRollDiceMoveBehaviour _rollDiceMove;
	PhaseCheckRollDicePairBehaviour _checkRollDicePair;
	PhaseNetworkBehaviour _phaseNetwork;

	int[] rolldiceInput = new int[GameplayManagerBehaviour.NUMBER_OF_DICE];
	int multiplier = 1;
	bool hasRollDiced = false;
	int[] playersPerfectCount = new int[GameplayManagerBehaviour.MAX_PLAYER];
	int outputType = 0;

    [SerializeField] DiceOutputType[] diceOutputTypes;

    [SerializeField] bool legacyRollDice = false;
	[SerializeField] int unluckyMaxOutput = 3;
	[SerializeField] int perfectMinOutput = 3;
	[SerializeField] int perfectMaxOutput = 4;
	[SerializeField][Range(0,100)] int[] normalRandomChances = new int[GameplayManagerBehaviour.MAX_DICE] { 16, 41, 71, 94, 99, 100 };
	[SerializeField] List<int> customRollDiceInput = new List<int>(); 

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();

		_rollDiceMove = Resources.FindObjectsOfTypeAll<PhaseRollDiceMoveBehaviour>()[0];
		_checkRollDicePair = Resources.FindObjectsOfTypeAll<PhaseCheckRollDicePairBehaviour>()[0];
		_phaseNetwork = GetComponent<PhaseNetworkBehaviour>();
	}

	void OnEnable() {
		hasRollDiced = false;
		for(int i = 0; i < rolldiceInput.Length; i++) {
			rolldiceInput[i] = 0;
		}
	}

	void OnDisable() {
		int totalRollDiceInput = (rolldiceInput[0] + rolldiceInput[1]) * multiplier;
		bool isPaired = rolldiceInput[0] == rolldiceInput[1];

		_rollDiceMove.SetMoveInput(totalRollDiceInput);
		_checkRollDicePair.isPaired = isPaired;

        Achievement.UpdateRollDiceStats(_characterManager.GetCurrentPlayer(), isPaired);
	}

	public void RollDiceMovePhase() {
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.RollDice_Move);
	}

	public void RollDicePhase() {
		_phaseNetwork.ChangePhase(GameplayManagerBehaviour.GameplayPhase.RollDice);
	}

	public void RandomizeDice(RollDiceOutputType rollDiceOutputType, float percentage) { // ga sekedar output type, tapi sama persentasenya(?)
		if(!hasRollDiced) {
			for(int i = 0; i < rolldiceInput.Length; i++) {
				if(customRollDiceInput.Count > 0 && customRollDiceInput[0] > 0) {
					rolldiceInput[i] = customRollDiceInput[0];
					customRollDiceInput.RemoveAt(0);
				}
				else if(legacyRollDice) { 
					int randomOutput = 0;
					switch(rollDiceOutputType) {
						case RollDiceOutputType.Unlucky:
							do {
								randomOutput = Random.Range(1, unluckyMaxOutput + 1);
							} while(i > 0 && randomOutput == rolldiceInput[i - 1]);
							break;
						case RollDiceOutputType.Perfect:
                            randomOutput = (i == 0) ? Random.Range(perfectMinOutput, perfectMaxOutput + 1) : rolldiceInput[i-1];
							playersPerfectCount[_characterManager.GetCurrentPlayer()]++;
							break;
						default:
                            do {
								randomOutput = GenerateNormalRandomOutput();
							} while(i > 0 && randomOutput == rolldiceInput[i - 1]);
							break;
					}

					rolldiceInput[i] = randomOutput;
				} else
                { // berdasarkan persentase itu, gmana dapetnya, belom ditambahin perfect count
                    for(int j=0; j<diceOutputTypes.Length; j++)
                    {
                        if(diceOutputTypes[j].outputType == rollDiceOutputType)
                        {
                            for(int k=0; k<diceOutputTypes[j].diceOutputTypeDatabases.Length; k++)
                            {
                                if(percentage < diceOutputTypes[j].diceOutputTypeDatabases[k].chance)
                                {
                                    rolldiceInput[i] = diceOutputTypes[j].diceOutputTypeDatabases[k].diceOutput[i];
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    if(i-1 == 0 && rolldiceInput[i] == GameplayManagerBehaviour.MAX_DICE && rolldiceInput[i-1] == GameplayManagerBehaviour.MAX_DICE)
                        playersPerfectCount[_characterManager.GetCurrentPlayer()]++;
                }
			}
		}
	}

	private int GenerateNormalRandomOutput () {
		int randomChance = Random.Range(0, 101);
		for(int i=0; i<normalRandomChances.Length; i++) {
			if(randomChance < normalRandomChances[i]) return i + 1;
		}
		return GameplayManagerBehaviour.MAX_DICE / 2;
	}
	
	public void SyncRollDiceInput(int[] input, int rollDiceOutputType) {
		hasRollDiced = true;
		rolldiceInput = input;
		outputType = rollDiceOutputType;
	}

    public bool GetPerfectRollDiceIsAvailable() { return _checkRollDicePair.GetPairedCount() < GameplayManagerBehaviour.OVERLUCK_DICE_PAIRED - 1; }
	public bool GetHasRollDiced() { return hasRollDiced; }
	public int[] GetRollDiceInput() { return rolldiceInput; }
	public int[] GetPlayersPerfectCount() { return playersPerfectCount; }
	public int GetOutputType() { return outputType; }
	public void SetMultiplier(int newMultiplier) { multiplier = newMultiplier; }
	public void SetCustomRollDiceInput(List<int> newCustomRollDiceInput) { customRollDiceInput = newCustomRollDiceInput; }
}

[System.Serializable]
class DiceOutputType
{
    public PhaseRollDiceBehaviour.RollDiceOutputType outputType;
    public DiceOutputTypeDatabase[] diceOutputTypeDatabases;
}

[System.Serializable]
class DiceOutputTypeDatabase
{
    [Range(0.0f, 1.0f)] public float chance = 0.1f;
    public int[] diceOutput;
}