﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseUseCardActionBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;
	AudioManagerBehaviour _audioManager;
	PhaseUseCardBehaviour _useCard;
	PhaseCheckKOBehaviour _checkKO;

	[HideInInspector] public List<CharacterBehaviour> listTarget = new List<CharacterBehaviour>();
	[HideInInspector] public CardBehaviour _cardUsed;
	[HideInInspector] public bool isUseSpecial;

	[SerializeField] string useCardStr = "Menggunakan Kartu!!!";

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
		_audioManager = GameObject.FindObjectOfType<AudioManagerBehaviour>();
		_useCard = Resources.FindObjectsOfTypeAll<PhaseUseCardBehaviour>()[0];
		_checkKO = Resources.FindObjectsOfTypeAll<PhaseCheckKOBehaviour>()[0];
	}
	
	void OnEnable () {
		_useCard.canUseCard = false;
		_checkKO.SetNextPhase(GameplayManagerBehaviour.GameplayPhase.CheckStates);

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.UseCard);

		string cardName = (isUseSpecial) ? _cardUsed.GetCardSpecialMoveName() : _cardUsed.GetCardNormalMoveName();
        string showCardTextStr = "<size='18'>" + currentPlayer.GetCharacterName() + " " + useCardStr + "</size>\n" + cardName;
		_uiGameplayScreen.ShowCard(_cardUsed, currentPlayer, true, isUseSpecial, showCardTextStr);

		bool isExclusiveMove = _cardUsed.GetExclusiveCharacter() && _cardUsed.GetExclusiveCharacter().GetCharacterType() == currentPlayer.GetCharacterType();
		if(isExclusiveMove && isUseSpecial) _audioManager.GetComponent<AudioSource>().mute = true;
		float activateCardDelay = (isExclusiveMove && isUseSpecial) ? _uiGameplayScreen.GetShowCardExclusiveDelay() : _uiGameplayScreen.GetShowCardDelay();
		Invoke("ActivateCard", activateCardDelay);
	}

	void OnDisable () {
		listTarget.Clear();
	}

	private void ActivateCard() {
		_cardUsed.cardActive = true;
		_cardUsed.cardUseSpecial = isUseSpecial;
	}

	private void NextPhase(GameplayManagerBehaviour.GameplayPhase phase) {
		_audioManager.GetComponent<AudioSource>().mute = false;
		_gameplayManager.ChangePhase(phase);
	}

	public void DiactivateCard() {
		_cardUsed.cardActive = false;
		NextPhase(GameplayManagerBehaviour.GameplayPhase.CheckTeleport);
	}

	public void DiactivateCardToBattlePhase() {
		_cardUsed.cardActive = false;
		NextPhase(GameplayManagerBehaviour.GameplayPhase.CheckBattle_Battle);
	}
}
