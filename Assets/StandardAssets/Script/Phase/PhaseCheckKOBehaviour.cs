﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseCheckKOBehaviour : MonoBehaviour {
	enum CheckKOPhase { SelectTarget, Ready, Move }
	CheckKOPhase checkKOPhase = CheckKOPhase.SelectTarget;

	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;
	MapBehaviour _map;

	GameplayManagerBehaviour.GameplayPhase _nextPhase = GameplayManagerBehaviour.GameplayPhase.CheckRollDicePair;
	List<CharacterBehaviour> _listKOCharacter;

	[SerializeField] string koTitleText = "Yahh";
	[SerializeField] string koDescText = "lelah dan harus kembali ke pos istirahat terakhir ...";
	int targetCount = 0;
	bool[] hasArrived;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();
	}

	void OnEnable() {
		_listKOCharacter = _characterManager.GetListKOCharacters();
		if(_listKOCharacter.Count == 0) {
			NextPhase();
		} else {
			checkKOPhase = CheckKOPhase.SelectTarget;
			hasArrived = new bool[_listKOCharacter.Count];
			targetCount = 0;
		}
	}

	void Update() {
		switch(checkKOPhase) {
			case CheckKOPhase.SelectTarget: HandleSelectTarget(); break;
			case CheckKOPhase.Ready: break;
			case CheckKOPhase.Move:	HandleMovement(); break;
		}
		HandleNextPhase();
	}

	private void HandleSelectTarget() {
		_uiGameplayScreen.ShowNotification(koTitleText, _listKOCharacter[targetCount].GetCharacterName() + " " + koDescText);
		checkKOPhase = CheckKOPhase.Ready;
		Invoke("ChangeCheckKOToMove", _uiGameplayScreen.GetNotificationDelay());
	}

	private void ChangeCheckKOToMove() {
		_listKOCharacter[targetCount].SetAnimation(CharacterBehaviour.AnimationType.Snake);
		checkKOPhase = CheckKOPhase.Move;
	}

	private void HandleMovement() {
		if(!hasArrived[targetCount]) {
			MapTileBehaviour currentPlayerTile = _map.GetMapTile(_listKOCharacter[targetCount].position);
			MapTileBehaviour lastCheckpointTile = _map.GetLastCheckpointTile(currentPlayerTile);
			Vector2 targetSlotPos = lastCheckpointTile.GetCharacterSlot(_listKOCharacter[targetCount].playerId).transform.position;

			_listKOCharacter[targetCount].MoveToPosition(targetSlotPos, _characterManager.GetMoveSpeed());
			hasArrived[targetCount] = _listKOCharacter[targetCount].CheckArrivedInPosition(targetSlotPos, _characterManager.GetCheckRadius());
			if(hasArrived[targetCount]) {
				_listKOCharacter[targetCount].position = lastCheckpointTile.tileId;
				_listKOCharacter[targetCount].currentHP = _listKOCharacter[targetCount].GetMaxHP();
				_listKOCharacter[targetCount].SetAnimation(CharacterBehaviour.AnimationType.Idle);

				checkKOPhase = CheckKOPhase.SelectTarget;
				targetCount++;
			}
		}
	}

	private void HandleNextPhase() {
		for(int i=0; i<hasArrived.Length; i++) {
			if(!hasArrived[i]) return;
		}
		CheckCurrentPlayerIsKO();
		NextPhase();
	}

	private void NextPhase() {
		_gameplayManager.ChangePhase(_nextPhase);
	}

	private void CheckCurrentPlayerIsKO() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		for(int i=0; i<_listKOCharacter.Count; i++) {
			if(_listKOCharacter[i].playerId == currentPlayer.playerId) {
				_nextPhase = GameplayManagerBehaviour.GameplayPhase.CheckRollDicePair;
				return;
			}
		}
	}

	public List<CharacterBehaviour> GetListKOCharacter() { return _listKOCharacter; }
	public void SetNextPhase(GameplayManagerBehaviour.GameplayPhase phase) { _nextPhase = phase; }
}
