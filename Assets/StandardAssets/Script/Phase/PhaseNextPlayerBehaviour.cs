﻿using UnityEngine;

public class PhaseNextPlayerBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;
	PhaseUseCardBehaviour _useCard;

	int turnCount = 0;
	[SerializeField] int nextTurnPoint = 10;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
		_useCard = Resources.FindObjectsOfTypeAll<PhaseUseCardBehaviour>()[0];

		turnCount = 1;
	}

	void OnEnable() {
		DecreasePlayerState();
		_useCard.canUseCard = true;

		if(_characterManager.GetCurrentPlayer() + 1 == GameplayManagerBehaviour.MAX_PLAYER) {
			if(nextTurnPoint > 0) {
				for(int i = 0; i < GameplayManagerBehaviour.MAX_PLAYER; i++) {
					int currentPoint = _characterManager.GetCharacter(i).currentPoint;
					_characterManager.GetCharacter(i).currentPoint = Mathf.Min(currentPoint + nextTurnPoint, GameplayManagerBehaviour.MAX_POINT);
					_characterManager.GetCharacter(i).SetAnimation(CharacterBehaviour.AnimationType.TakePoint);
				}
			}

			_characterManager.NextPlayerIdTurn();
			_uiGameplayScreen.ShowText("Turn-" + (++turnCount));
			Invoke("CheckStatePhase", _uiGameplayScreen.GetShowTextDelay());
		}
		else {
			_characterManager.NextPlayerIdTurn();
			CheckStatePhase();
		}
	}
	
	private void DecreasePlayerState() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		currentPlayer.DecreaseStateTurnRemain();
	}

	private void CheckStatePhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckStates);
	}

	public int GetTurnCount() { return turnCount; }
}
