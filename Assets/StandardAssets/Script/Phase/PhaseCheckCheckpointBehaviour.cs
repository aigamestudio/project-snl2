﻿using UnityEngine;

public class PhaseCheckCheckpointBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();
	}

	void OnEnable() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		MapTileBehaviour currentTile = _map.GetMapTile(currentPlayer.position);

		if(currentTile.GetTileType() == MapTileBehaviour.TileType.Checkpoint) {
			currentPlayer.currentHP = Mathf.Min(currentPlayer.currentHP + 1, currentPlayer.GetMaxHP());
			currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.Heal);
			Invoke("CheckRollDicePairPhase", GameplayManagerBehaviour.STANDARD_TIME);
		} else {
			CheckRollDicePairPhase();
		}
	}
	
	private void CheckRollDicePairPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckRollDicePair);
	}
}
