﻿using UnityEngine;

public class PhaseCheckBattleBattleBehaviour : MonoBehaviour {
	public enum BattlePhase { Idle, SetupQuestion, ChooseAnswer, ProcessAnswer, CheckExit }
	public enum BattleType { Addition, Substraction, Multiplication }
	BattlePhase battlePhase = BattlePhase.Idle;
	BattleType battleType = BattleType.Addition;

	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	BattleGeneratorBehaviour _battleGenerator;

	[HideInInspector] public CharacterBehaviour _challenger;
	[HideInInspector] public CharacterBehaviour _challenged;

	[SerializeField] int battleTotalNumber = 3;

	int[] num;
	int[] choiceNum = new int[GameplayManagerBehaviour.BATTLE_TOTAL_CHOICE];
	int trueChoice = 0;
	[HideInInspector] public int challengerChoice = -1;
	[HideInInspector] public int challengedChoice = -1;

	bool isBossBattle = false;
	int battleCount = 0;

	void Awake() {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_battleGenerator = GetComponent<BattleGeneratorBehaviour>();
	}

	void OnEnable() {
		num = new int[battleTotalNumber];
		battlePhase = BattlePhase.Idle; 
		battleCount = 0;
	}

	void Update() {
		switch(battlePhase) {
			case BattlePhase.SetupQuestion: HandleSetupQuestionPhase(); break;
			case BattlePhase.ChooseAnswer: HandleChooseAnswerPhase(); break;
			case BattlePhase.ProcessAnswer: HandleProcessAnswerPhase(); break;
			case BattlePhase.CheckExit: HandleCheckExit(); break;
		}
	}

	private void HandleSetupQuestionPhase() {
		challengerChoice = -1;
		challengedChoice = -1;
		trueChoice = Random.Range(0, GameplayManagerBehaviour.BATTLE_TOTAL_CHOICE);

		battleType = _battleGenerator.GenerateBattleType();
		_battleGenerator.GenerateBattleQuestion(battleType, num, choiceNum, trueChoice);

		battlePhase = BattlePhase.ChooseAnswer;
	}

	private void HandleChooseAnswerPhase() {
		if(challengerChoice != -1 || challengedChoice != -1) {
			battlePhase = BattlePhase.ProcessAnswer;
		}
	}

	private void HandleProcessAnswerPhase() {
		bool isChallengerAttack = false;
		if(challengerChoice != -1) {
			if(challengerChoice == trueChoice) isChallengerAttack = true;
			else isChallengerAttack = false;
		}
		else if(challengedChoice != -1) {
			if(challengedChoice == trueChoice) isChallengerAttack = false;
			else isChallengerAttack = true;
		}

		_challenger.currentHP -= (!isChallengerAttack) ? 1 : 0;
		_challenged.currentHP -= (isChallengerAttack) ? 1 : 0;

		battleCount++;
		battlePhase = BattlePhase.CheckExit;
	}

	private void HandleCheckExit() {
		if(_challenger.currentHP == 0 || _challenged.currentHP == 0) {
			if(isBossBattle) {
				if(_challenged.currentHP == 0) Invoke("WinPhase", GameplayManagerBehaviour.STANDARD_TIME);
				else Invoke("CheckKOPhase", GameplayManagerBehaviour.STANDARD_TIME);
			}
			else {
				Invoke("CheckKOPhase", GameplayManagerBehaviour.STANDARD_TIME);
			}
		}
		else {
			if(battleCount == GameplayManagerBehaviour.BATTLE_MAX_COUNT) Invoke("CheckKOPhase", GameplayManagerBehaviour.STANDARD_TIME);
			else Invoke("ResetBattle", GameplayManagerBehaviour.STANDARD_TIME);
		}

		battlePhase = BattlePhase.Idle;
	}

	private void CheckKOPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckKO);
	}

	private void WinPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.Win);
	}

	public BattlePhase GetBattlePhase() { return battlePhase; }
	public BattleType GetBattleType() { return battleType; }
	public int[] GetNum() { return num; }
	public int[] GetChoiceNum() { return choiceNum; }
	public int GetTrueChoice() { return trueChoice; }
	public void ResetBattle() { battlePhase = BattlePhase.SetupQuestion; }
	public void SetIsBossBattle(bool cond) { isBossBattle = cond; }
	public bool GetIsBossBattle() { return isBossBattle; }
    public int GetBattleCount() { return battleCount; }
}