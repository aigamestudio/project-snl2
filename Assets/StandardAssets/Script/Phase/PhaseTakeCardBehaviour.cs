﻿using UnityEngine;

public class PhaseTakeCardBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;
	CardDeckBehaviour _cardDeck;

	PhaseDiscardCardBehaviour _discardCard;

	[SerializeField] bool enableTakeCard = true;
	[SerializeField] string takeCardStr = "Mendapatkan Kartu!";

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
		_cardDeck = GameObject.FindObjectOfType<CardDeckBehaviour>();

		_discardCard = Resources.FindObjectsOfTypeAll<PhaseDiscardCardBehaviour>()[0];
	}

	void OnEnable() {
		if(!enableTakeCard) { NextPlayerPhase(); return; }

		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		CardBehaviour newCard = _cardDeck.GetAvailableDeck().GetChild(0).GetComponent<CardBehaviour>();
		bool isPointCard = newCard.GetComponent<CardPointBehaviour>();

		_uiGameplayScreen.ShowCard(newCard, currentPlayer, PanelBehaviour.userCanInput, false, currentPlayer.GetCharacterName() + "\n" + takeCardStr);
		currentPlayer.SetAnimation(CharacterBehaviour.AnimationType.TakeCard);

		if(isPointCard) {
			newCard.PutCardToDeck(_cardDeck);
			currentPlayer.currentPoint = Mathf.Min(currentPlayer.currentPoint + newCard.GetComponent<CardPointBehaviour>().point, GameplayManagerBehaviour.MAX_POINT);
			Invoke("NextPlayerPhase", _uiGameplayScreen.GetShowCardDelay());
		}
		else {
			if(currentPlayer.GetCards().childCount < GameplayManagerBehaviour.MAX_CARD_SLOT) {
				newCard.AddCardToCharacter(currentPlayer);
				Invoke("NextPlayerPhase", _uiGameplayScreen.GetShowCardDelay());
			}
			else {
				_discardCard._newCard = newCard;
				Invoke("DiscardCardPhase", _uiGameplayScreen.GetShowCardDelay());
			}
		}
	}

	private void NextPlayerPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.NextPlayer);
	}

	private void DiscardCardPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.DiscardCard);
	}
}
