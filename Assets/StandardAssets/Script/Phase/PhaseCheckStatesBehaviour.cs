﻿using UnityEngine;

public class PhaseCheckStatesBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;

	PhaseRollDiceBehaviour _rollDice;

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();

		_rollDice = Resources.FindObjectsOfTypeAll<PhaseRollDiceBehaviour>()[0];
	}

	void OnEnable() {
		CharacterBehaviour currentCharacter = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		StateSkipBehaviour skipState = currentCharacter.GetState<StateSkipBehaviour>();
		StateRollDiceBehaviour rollDiceMultiplierState = currentCharacter.GetState<StateRollDiceBehaviour>();

		int rollDiceMultiplier = (rollDiceMultiplierState) ? rollDiceMultiplierState.multiplier : 1;
		_rollDice.SetMultiplier(rollDiceMultiplier);

		if(skipState) {
			if(skipState.isOverluckState) currentCharacter.SetAnimation(CharacterBehaviour.AnimationType.Overluck);
			else currentCharacter.SetAnimation(CharacterBehaviour.AnimationType.Skipped);
		}

		string nextPhase = (!skipState) ? "UseCardPhase" : "SkipPlayer";
		Invoke(nextPhase, GameplayManagerBehaviour.STANDARD_TIME);
	}

	private void UseCardPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.UseCard);
	}

	private void SkipPlayer() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.NextPlayer);
	}
}
