﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseWinBehaviour : MonoBehaviour {
	AudioManagerBehaviour _audioManager;
	CharacterManagerBehaviour _characterManager;
	MapBehaviour _map;
	PhaseRollDiceBehaviour _rollDice;
    PhaseNextPlayerBehaviour _nextPlayer;

	CharacterBehaviour[] characterOrder;

	void Awake() {
		_audioManager = GameObject.FindObjectOfType<AudioManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();
		_rollDice = Resources.FindObjectsOfTypeAll<PhaseRollDiceBehaviour>()[0];
        _nextPlayer = Resources.FindObjectsOfTypeAll<PhaseNextPlayerBehaviour>()[0];

        characterOrder = new CharacterBehaviour[GameplayManagerBehaviour.MAX_PLAYER];
	}

	void OnEnable() {
		_audioManager.GetComponent<AudioSource>().mute = true;

		AssignCharacterOrder();
		HandleCharacterAnimation();
		HandleResultScreenVariable();

        Achievement.UpdateWinStats(ResultScreenBehaviour.playerOrder == 0, _nextPlayer.GetTurnCount());
	}

	private void AssignCharacterOrder() {
		CharacterBehaviour currentPlayer = _characterManager.GetCharacter(_characterManager.GetCurrentPlayer());
		characterOrder[0] = currentPlayer;
		int order = 1;
		for(int i=_map.GetMapTileCount()-1; i>=0; i--) {
			MapTileBehaviour tile = _map.GetMapTile(i);
			List<CharacterBehaviour> listCharacterInTile = _characterManager.GetListCharactersInTile(tile, currentPlayer);
			listCharacterInTile.Sort((x, y) => -1 * x.currentPoint.CompareTo(y.currentPoint));
			for(int j=0; j<listCharacterInTile.Count; j++) {
				characterOrder[order] = listCharacterInTile[j];
				order++;
			}
		}
	}

	private void HandleCharacterAnimation() {
		_map.GetBossCharacter().SetAnimation(CharacterBehaviour.AnimationType.Lose);
		_map.GetBossCharacter().GetComponent<AudioSource>().mute = true;
		for(int i=0; i<characterOrder.Length; i++) {
			characterOrder[i].GetComponent<AudioSource>().mute = true;
			if(i==0) characterOrder[i].SetAnimation(CharacterBehaviour.AnimationType.Win);
			else characterOrder[i].SetAnimation(CharacterBehaviour.AnimationType.Lose);
		}
	}

	private void HandleResultScreenVariable() {
		ResultScreenBehaviour.mapRewardCoin = _map.GetMapRewardCoin();
		ResultScreenBehaviour.gamePerfectCount = _rollDice.GetPlayersPerfectCount()[GameplayManagerBehaviour.playerIdTurn];

		for (int i=0; i<characterOrder.Length; i++) {
			if(GameplayManagerBehaviour.playerIdTurn == characterOrder[i].playerId) {
				ResultScreenBehaviour.playerOrder = i;
				break;
			}
		}
	}

	public CharacterBehaviour[] GetCharacterOrder() { return characterOrder; }
}
