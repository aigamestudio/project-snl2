﻿using UnityEngine;
using System.Collections.Generic;

public class PhaseCheckTeleportBehaviour : MonoBehaviour {
	GameplayManagerBehaviour _gameplayManager;
	CharacterManagerBehaviour _characterManager;
	UIGameplayScreenBehaviour _uiGameplayScreen;
	MapBehaviour _map;

	PhaseCheckTeleportMoveBehaviour _checkTeleportMove;

	List<CharacterBehaviour> target = new List<CharacterBehaviour>();

	[SerializeField] string ladderText = "Yeay!";
	[SerializeField] string snakeText = "Oh No!";
	[SerializeField] string regroupText = "Regroup!";
	[SerializeField] string trapText = "It's a Trap!";

	bool[] hasMoved = new bool[GameplayManagerBehaviour.MAX_PLAYER];

	void Awake () {
		_gameplayManager = GameObject.FindObjectOfType<GameplayManagerBehaviour>();
		_characterManager = GameObject.FindObjectOfType<CharacterManagerBehaviour>();
		_uiGameplayScreen = GameObject.FindObjectOfType<UIGameplayScreenBehaviour>();
		_map = GameObject.FindObjectOfType<MapBehaviour>();

		_checkTeleportMove = Resources.FindObjectsOfTypeAll<PhaseCheckTeleportMoveBehaviour>()[0];
	}

	void OnEnable() {
		for(int i=0; i<GameplayManagerBehaviour.MAX_PLAYER; i++) {
			CharacterBehaviour player = _characterManager.GetCharacter(i);
			MapTileBehaviour tile = _map.GetMapTile(player.position);

			if(tile.GetTileType() == MapTileBehaviour.TileType.Ladder) {
				target.Add(player);
				_checkTeleportMove.listTarget.Add(player);
				_checkTeleportMove.SetTargetTile(tile.GetConnectedMapTile());
				_uiGameplayScreen.ShowNotification(ladderText, player.GetCharacterName() + " " + tile.GetTileDesc());
				Invoke("LadderAnimation", _uiGameplayScreen.GetNotificationDelay());
				Invoke("CheckTeleportMovePhase", _uiGameplayScreen.GetNotificationDelay());
				return;
			} else if(tile.GetTileType() == MapTileBehaviour.TileType.Snake) {
				target.Add(player);
				_checkTeleportMove.listTarget.Add(player);
				_checkTeleportMove.SetTargetTile(tile.GetConnectedMapTile());
				_uiGameplayScreen.ShowNotification(snakeText, player.GetCharacterName() + " " + tile.GetTileDesc());
				Invoke("SnakeAnimation", _uiGameplayScreen.GetNotificationDelay());
				Invoke("CheckTeleportMovePhase", _uiGameplayScreen.GetNotificationDelay());
				return;
			} else if(tile.GetTileType() == MapTileBehaviour.TileType.Regroup && hasMoved[i]) {
				for(int j = 0; j < GameplayManagerBehaviour.MAX_PLAYER; j++) {
					hasMoved[j] = false;
					if(j != i) {
						target.Add(_characterManager.GetCharacter(j));
						_checkTeleportMove.listTarget.Add(_characterManager.GetCharacter(j));
					}
				}

				_checkTeleportMove.SetTargetTile(tile);
				_uiGameplayScreen.ShowNotification(regroupText, player.GetCharacterName() + " " + tile.GetTileDesc());
				Invoke("LadderAnimation", _uiGameplayScreen.GetNotificationDelay());
				Invoke("CheckTeleportMovePhase", _uiGameplayScreen.GetNotificationDelay());
				return;
			} else if(tile.GetTileType() == MapTileBehaviour.TileType.Trap && hasMoved[i]) {
				hasMoved[i] = false;
				player.currentHP = Mathf.Max(0, player.currentHP - tile.GetTrapDamage());
				player.SetAnimation(CharacterBehaviour.AnimationType.Hit);
				_uiGameplayScreen.ShowNotification(trapText, player.GetCharacterName() + " " + tile.GetTileDesc());
				Invoke("CheckKOPhase", _uiGameplayScreen.GetNotificationDelay());
				return;
			}
		}

		CheckKOPhase();
	}

	private void LadderAnimation() {
		for(int i=0; i<target.Count; i++) {
			target[i].SetAnimation(CharacterBehaviour.AnimationType.Ladder);
		}
		target.Clear();
	}

	private void SnakeAnimation() {
		for(int i = 0; i < target.Count; i++) {
			target[i].SetAnimation(CharacterBehaviour.AnimationType.Snake);
		}
		target.Clear();
	}

	private void CheckTeleportMovePhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckTeleport_Move);
	}

	private void CheckKOPhase() {
		_gameplayManager.ChangePhase(GameplayManagerBehaviour.GameplayPhase.CheckKO);
	}

	public void SetHasMoved(int playerId, bool cond) { hasMoved[playerId] = cond; }
}
