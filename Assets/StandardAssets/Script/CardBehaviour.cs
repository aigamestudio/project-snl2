﻿using UnityEngine;

public class CardBehaviour : MonoBehaviour {
	public enum TargetType { None, Self, Target, AllExceptSelf, All, ClosestPlayer, TargetWithSelf }

	[SerializeField] [Range(0, GameplayManagerBehaviour.AVAILABLE_CARD_TYPE)] int cardType = 0;
	[SerializeField] string cardNormalMoveName = "";
	[SerializeField] string cardSpecialMoveName = "";
	[SerializeField] [TextArea(1,4)] string cardDesc;
	[SerializeField] TargetType _cardTargetType = TargetType.Self;
	[SerializeField] int cardReqPoint = 0;
	[SerializeField] [TextArea(1, 4)] string[] cardSpecialDesc = new string[GameplayManagerBehaviour.MAX_CHARACTER_LEVEL];
	[SerializeField] TargetType _cardSpecialTargetType = TargetType.None;
	[SerializeField] CharacterBehaviour _exclusiveCharacter;
	[SerializeField] int[] cardSpecialReqPoint = new int[GameplayManagerBehaviour.MAX_CHARACTER_LEVEL];

	[HideInInspector] public bool cardActive = false;
	[HideInInspector] public bool cardUseSpecial = false;

	public void PutCardToDeck(CardDeckBehaviour cardDeck) {
		transform.SetParent(cardDeck.GetGraveDeck());
	}

	public void AddCardToCharacter(CharacterBehaviour character) {
		transform.SetParent(character.GetCards());
	}

	public bool CheckSpecialRequirement(CharacterBehaviour character) {
		if(!GetHasSpecial()) return false;
		bool isExclusiveCharacter = IsExclusiveCharacter(character);
		bool isPassedReqPoint = character.currentPoint >= cardSpecialReqPoint[character.GetCurrentLevel()];
		return isExclusiveCharacter && isPassedReqPoint;
	}

	public bool IsExclusiveCharacter(CharacterBehaviour character) {
		return _exclusiveCharacter == null || character.GetCharacterType() == _exclusiveCharacter.GetCharacterType();
	}

	public int GetCardType() { return cardType; }
	public string GetCardNormalMoveName() { return cardNormalMoveName; }
	public string GetCardSpecialMoveName() { return cardSpecialMoveName; }
	public string GetCardName() { return cardNormalMoveName + ((cardSpecialMoveName != "") ? " / " + cardSpecialMoveName : ""); }
	public string GetCardDesc() { return cardDesc; }
	public string GetCardSpecialDesc() { return cardSpecialDesc[0]; }
	public string GetCardSpecialDesc(int level) { return cardSpecialDesc[level]; }
	public int GetCardReqPoint() { return cardReqPoint; }
	public int GetCardSpecialReqPoint(int level) { return cardSpecialReqPoint[level]; }
	public TargetType GetCardTargetType() { return _cardTargetType; }
	public TargetType GetCardSpecialTargetType() { return _cardSpecialTargetType; }
	public CharacterBehaviour GetExclusiveCharacter() { return _exclusiveCharacter; }
	public bool GetHasSpecial() { return _cardSpecialTargetType != TargetType.None; }
}
