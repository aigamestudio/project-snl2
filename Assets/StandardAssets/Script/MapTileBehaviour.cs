﻿using UnityEngine;

public class MapTileBehaviour : MonoBehaviour {
	public enum TileType { Normal, Checkpoint, Ladder, Snake, Regroup, Trap, Finish }
	[SerializeField] TileType tileType = TileType.Normal;

	[HideInInspector] public int tileId = 0;
	[SerializeField] MapTileBehaviour _connectedMapTile;
	[SerializeField][TextArea(1,3)] string tileDesc = "";
	[SerializeField] int trapDamage = 5;

	GameObject[] _characterSlot;

	void Awake () {
		_characterSlot = new GameObject[GameplayManagerBehaviour.MAX_PLAYER];
		for(int i=0; i<_characterSlot.Length; i++) {
			_characterSlot[i] = transform.Find("Slot " + (i + 1)).gameObject;
		}
	}

	public GameObject GetCharacterSlot(int playerId) { return _characterSlot[playerId]; }
	public TileType GetTileType() { return tileType; }
	public MapTileBehaviour GetConnectedMapTile() { return _connectedMapTile; }
	public string GetTileDesc() { return tileDesc; }
	public int GetTrapDamage() { return trapDamage; }
}
