﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenBehaviour : MonoBehaviour {
	[SerializeField] float loadingTime = 2.0f;

	void OnEnable() {
        CloudOnce.Cloud.SignIn(true);

		ProfileData.LoadMapData();
		ProfileData.LoadCharacterData();
		ProfileData.LoadSettingsData();
		ProfileData.LoadProfileData();
        ProfileData.LoadAchievementData();

		Invoke("ChangeScene", loadingTime);
	}

	public void ChangeScene() {
		SceneManager.LoadScene("menuscreen");
	}
}